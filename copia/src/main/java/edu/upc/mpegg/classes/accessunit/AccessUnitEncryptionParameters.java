package edu.upc.mpegg.classes.accessunit;

import com.sun.xml.bind.v2.runtime.unmarshaller.Base64Data;
import edu.upc.mpegg.enums.MPEGG_CIPHERS;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.crypto.Cipher;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessUnitEncryptionParametersType", namespace = "TFG:MPEG-G:AUEncryptedParameters", propOrder = {
        "wrappedKey",
        "cipher",
        "aublockIV",
        "auinIV",
        "aublockTAG",
        "auinTAG",
        "auinNounce",
        "aublockNounce"
})
public class AccessUnitEncryptionParameters {

    Element rootElement;
    private String aublockIV;
    private String auinIV;
    private String aublockTAG;
    private String auinTAG;
    private String cipher;

    public AccessUnitEncryptionParameters(Document patherDocument){
        cipher = MPEGG_CIPHERS.AES_128_CTR.value();
        aublockIV = Base64.getMimeEncoder().encodeToString("dsadasd".getBytes());
        auinIV = Base64.getMimeEncoder().encodeToString("dsadasd".getBytes());
        aublockTAG = Base64.getMimeEncoder().encodeToString("dsadasd".getBytes());
        auinTAG = Base64.getMimeEncoder().encodeToString("dsadasd".getBytes());
        rootElement = patherDocument.createElement("AccessUnitEncryptionParameters");
    }

    public Element writeAccessUnitEncryptionParameters() throws ParserConfigurationException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            rootElement.appendChild(getCipherNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getAuBlockIvNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getAuInIvNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getAuBlockTagNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getAuInTagNode(rootElement.getOwnerDocument()));


           return rootElement;

        } catch (Exception e) {
            throw e;
        }
    }

    private Node getCipherNode(Document doc) {
        Element XMLcipher = doc.createElement("cipher");
        XMLcipher.setTextContent(cipher);
        return XMLcipher;
    }

    private Node getAuInIvNode(Document doc) {
        Element XMLauinIV = doc.createElement("auinIV");
        XMLauinIV.setTextContent(auinIV);
        return XMLauinIV;
    }
    private Node getAuBlockTagNode(Document doc) {
        Element XMLaublockTAG = doc.createElement("auBlockTAG");
        XMLaublockTAG.setTextContent(aublockTAG);
        return XMLaublockTAG;
    }
    private Node getAuInTagNode(Document doc) {
        Element XMLauinTag = doc.createElement("auinTAG");
        XMLauinTag.setTextContent(auinTAG);
        return XMLauinTag;
    }
    private Node getAuBlockIvNode(Document doc) {
        Element XMLaublockIV = doc.createElement("aublockIV");
        XMLaublockIV.setTextContent(aublockIV);
        return XMLaublockIV;
    }

    public void setAuInIv(byte[] auinIv) {
        this.auinIV = Base64.getMimeEncoder().encodeToString(auinIv);
    }
    public void setAuBlockTag(byte[] auBlockTag) {
        this.aublockTAG = Base64.getMimeEncoder().encodeToString(auBlockTag);
    }
    public void setAuInTag(byte[] auinTAG) {
        this.auinTAG = Base64.getMimeEncoder().encodeToString(auinTAG);
    }
    public void setAuBlockIv(byte[] aublockIV) {
        this.aublockIV = Base64.getMimeEncoder().encodeToString(aublockIV);
    }


}
