package edu.upc.mpegg.classes.signature;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Signature {

    public void signFile(Document doc,String URI)
            throws InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            KeyStoreException, UnrecoverableEntryException,
            IOException, CertificateException,
            TransformerException,
            ParserConfigurationException,
            SAXException, MarshalException,
            XMLSignatureException {



        // Create a DOM XMLSignatureFactory that will be used to
        // generate the enveloped signature.
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

        Reference ref = getReference(fac, URI);
        // Create the SignedInfo.
        SignedInfo si = getSignInfo(fac, ref);
        KeyStore.PrivateKeyEntry keyEntry = getPrivateKey();
        X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
        // Instantiate the document to be signed.


        KeyInfo ki = createKeyInfo(fac, cert);
        signFile(keyEntry, doc, fac, ki, si);
       // writeOutput(outputFile, doc);

    }

    // Create a Reference to the enveloped document (in this case,
    // you are signing the whole document, so a URI of "" signifies
    // that, and also specify the SHA1 digest algorithm and
    // the ENVELOPED Transform.
    private Reference getReference(XMLSignatureFactory fac, String URI) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
      return fac.newReference
              (URI, fac.newDigestMethod(DigestMethod.SHA256, null),
                      Collections.singletonList
                              (fac.newTransform
                                      (Transform.ENVELOPED, (TransformParameterSpec) null)),
                      null, null);
    }

    private void writeOutput(String OutputFIle, Document doc) throws FileNotFoundException, TransformerException {
        // Output the resulting document.
        OutputStream os = new FileOutputStream(OutputFIle);
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer trans = tf.newTransformer();

        trans.transform(new DOMSource(doc), new StreamResult(os));
    }

    private SignedInfo getSignInfo(XMLSignatureFactory fac,
                                   Reference ref)
            throws InvalidAlgorithmParameterException,
            NoSuchAlgorithmException {
        return fac.newSignedInfo
                (fac.newCanonicalizationMethod
                                (CanonicalizationMethod.INCLUSIVE,
                                        (C14NMethodParameterSpec) null),
                        fac.newSignatureMethod(SignatureMethod.DSA_SHA256, null),
                        Collections.singletonList(ref));
    }

    private KeyStore.PrivateKeyEntry getPrivateKey() throws KeyStoreException, IOException, UnrecoverableEntryException, NoSuchAlgorithmException, CertificateException {
        // Load the KeyStore and get the signing key and certificate.
        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new FileInputStream("clientkeystore"), "erictfm".toCharArray());
        return (KeyStore.PrivateKeyEntry) ks.getEntry
                        ("client", new KeyStore.PasswordProtection("erictfm".toCharArray()));
    }

    // Create the KeyInfo containing the X509Data.
    private KeyInfo createKeyInfo(XMLSignatureFactory fac, X509Certificate cert){
        KeyInfoFactory kif = fac.getKeyInfoFactory();
        List x509Content = new ArrayList();
        x509Content.add(cert.getSubjectX500Principal().getName());
        x509Content.add(cert);
        X509Data xd = kif.newX509Data(x509Content);
        return kif.newKeyInfo(Collections.singletonList(xd));
    }

    private void signFile(KeyStore.PrivateKeyEntry keyEntry,
                          Document doc,
                          XMLSignatureFactory fac,
                          KeyInfo ki,
                          SignedInfo si
                          )
            throws MarshalException, XMLSignatureException {
        // Create a DOMSignContext and specify the RSA PrivateKey and
        // location of the resulting XMLSignature's parent element.
        DOMSignContext dsc = new DOMSignContext
                (keyEntry.getPrivateKey(), doc.getDocumentElement());

        // Create the XMLSignature, but don't sign it yet.
        XMLSignature signature = fac.newXMLSignature(si, ki);
        // Marshal, generate, and sign the enveloped signature.
        // signature.
        signature.sign(dsc);
    }

}
