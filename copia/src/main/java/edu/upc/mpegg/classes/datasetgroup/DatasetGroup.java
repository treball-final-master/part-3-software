package edu.upc.mpegg.classes.datasetgroup;


import org.w3c.dom.Document;
import org.w3c.dom.Element;

import edu.upc.mpegg.enums.MPEGG_CIPHERS;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import edu.upc.mpegg.classes.signature.Signature;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class DatasetGroup {

    Document datasetGroup;
    Element datasetGroupRoot;
    DatasetGroupEncryptionParameters encryptionParameters;

    public DatasetGroup() throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        datasetGroup = dBuilder.newDocument();
        //add elements to Document
        datasetGroupRoot =
                datasetGroup.createElementNS("mpg-data-gr-prot:DatasetGroupProtectionType", "DatasetGroupProtection");
        //append root element to document

    }

    private byte[] encryptData(byte[] data) throws NoSuchPaddingException, NoSuchAlgorithmException {
//        Cipher cipher =  Cipher.getInstance("AES/CTR/NoPadding");
//        byte[] temp = new byte[128 / 8];
//        System.arraycopy(nonce, 0, iv, 0, nonce.length);
//        IvParameterSpec iv = new IvParameterSpec();
//
//        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
//        byte[] plaintext = cipher.doFinal(ciphertext);
        return new byte[5];
    }

    private byte[] decyrptData(byte[] data){
//        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
//        byte[] plaintext = cipher.doFinal(ciphertext);
        return new byte[5];
    }

    public void encryptAndSign(byte[] data)
            throws TransformerException,
            ParserConfigurationException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            KeyStoreException, UnrecoverableEntryException,
            IOException, CertificateException,
            TransformerException,
            ParserConfigurationException,
            SAXException, MarshalException,
            XMLSignatureException, NoSuchPaddingException {


        encryptData(data);


        encryptionParameters =
                new DatasetGroupEncryptionParameters(datasetGroup, MPEGG_CIPHERS.AES_128_CTR, "TAG", "13", "eric", 5, "3213");
        Element XMLencryptionParameters = encryptionParameters.writeEncryptionParameters();
        datasetGroupRoot.appendChild(XMLencryptionParameters);
        datasetGroup.appendChild(datasetGroupRoot);

        Signature sig = new Signature();
        sig.signFile(datasetGroup, "");

        //for output to file, console
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //for pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(datasetGroup);

        //write to console or file
        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(new File("output/dgpr.xml"));

        //write data
        transformer.transform(source, console);
        transformer.transform(source, file);
    }
}
