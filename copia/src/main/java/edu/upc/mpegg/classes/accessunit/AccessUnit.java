package edu.upc.mpegg.classes.accessunit;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import edu.upc.mpegg.classes.signature.Signature;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class AccessUnit {

    Document accessUnit;
    Element accessUnitRoot;
    AccessUnitEncryptionParameters encryptionParameters;

    public AccessUnit() throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        accessUnit = dBuilder.newDocument();
        //add elements to Document
        accessUnitRoot =
                accessUnit.createElementNS("mpg-access-prot:AccessUnitProtectionType", "AccessUnitProtection");
        //append root element to document
        encryptionParameters = new AccessUnitEncryptionParameters(accessUnit);
    }

    public void encryptAndSign() throws TransformerException, ParserConfigurationException,
             InvalidAlgorithmParameterException,
    NoSuchAlgorithmException,
    KeyStoreException, UnrecoverableEntryException,
    IOException, CertificateException,
    TransformerException,
    ParserConfigurationException,
    SAXException, MarshalException,
            XMLSignatureException {
        Element XMLencryptionParameters = encryptionParameters.writeAccessUnitEncryptionParameters();
        accessUnitRoot.appendChild(XMLencryptionParameters);
        accessUnit.appendChild(accessUnitRoot);

        Signature sig = new Signature();
        sig.signFile(accessUnit, "");

        //for output to file, console
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //for pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(accessUnit);

        //write to console or file
        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(new File("output/aupr.xml"));

        //write data
        transformer.transform(source, console);
        transformer.transform(source, file);
        System.out.println("DONE");
    }
}
