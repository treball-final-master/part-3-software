package edu.upc.mpegg.classes.dataset;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import edu.upc.mpegg.enums.MPEGG_CIPHERS;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.Base64;

public class DatasetEncryptionParameters {

    Element rootElement;
    private String configurationID;
    private int encryptedLocations;
    private String keyName;
    private String tag;
    private String iv;
    private MPEGG_CIPHERS cipher;

    public DatasetEncryptionParameters(Document patherDocument,
                                            MPEGG_CIPHERS cipher,
                                            String tag,
                                            String iv,
                                            String keyName,
                                            int encryptedLocations,
                                            String configurationID){
        this.cipher = cipher;
        this.tag = Base64.getMimeEncoder().encodeToString(tag.getBytes());
        this.iv = Base64.getMimeEncoder().encodeToString(iv.getBytes());
        this.keyName = keyName;
        this.encryptedLocations = encryptedLocations;
        this.configurationID = configurationID;
        rootElement = patherDocument.createElement("EncryptionParameters");
    }

    public Element writeEncryptionParameters() throws ParserConfigurationException {

        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        try {
            rootElement.setAttribute("encryptedLocations", String.valueOf(encryptedLocations));
            rootElement.setAttribute("configurationID",configurationID);

            rootElement.appendChild(getCipherNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getIvNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getTagNode(rootElement.getOwnerDocument()));
            rootElement.appendChild(getKeyNameNode(rootElement.getOwnerDocument()));

            return rootElement;

        } catch (Exception e) {
            throw e;
        }
    }

    private Node getCipherNode(Document doc) {
        Element XMLcipher = doc.createElement("cipher");
        XMLcipher.setTextContent(cipher.value());
        return XMLcipher;
    }

    private Node getIvNode(Document doc) {
        Element XMLauinIV = doc.createElement("auinIV");
        XMLauinIV.setTextContent(iv);
        return XMLauinIV;
    }
    private Node getTagNode(Document doc) {
        Element XMLauinTag = doc.createElement("auinTAG");
        XMLauinTag.setTextContent(tag);
        return XMLauinTag;
    }
    private Node getKeyNameNode(Document doc) {
        Element XMLaublockIV = doc.createElement("aublockIV");
        XMLaublockIV.setTextContent(keyName);
        return XMLaublockIV;
    }

}
