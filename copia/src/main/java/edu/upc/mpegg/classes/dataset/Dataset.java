package edu.upc.mpegg.classes.dataset;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import edu.upc.mpegg.enums.MPEGG_CIPHERS;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

import edu.upc.mpegg.classes.signature.Signature;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Dataset {

    Document dataset;
    Element datasetRoot;
    DatasetEncryptionParameters encryptionParameters;

    public Dataset() throws ParserConfigurationException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        dBuilder = dbFactory.newDocumentBuilder();
        dataset = dBuilder.newDocument();
        //add elements to Document
        datasetRoot =
                dataset.createElementNS("mpg-access-prot:DatasetProtectionType", "DatasetProtection");
        //append root element to document
        encryptionParameters = new DatasetEncryptionParameters(dataset, MPEGG_CIPHERS.AES_192_GCM, "TAG", "IV", "KeyName", 3);

    }

    public void encryptAndSign(byte[] data)
            throws TransformerException, ParserConfigurationException,
            InvalidAlgorithmParameterException,
            NoSuchAlgorithmException,
            KeyStoreException, UnrecoverableEntryException,
            IOException, CertificateException,
            TransformerException,
            ParserConfigurationException,
            SAXException, MarshalException,
            XMLSignatureException {
        Element XMLencryptionParameters = encryptionParameters.writeEncryptionParameters();
        datasetRoot.appendChild(XMLencryptionParameters);
        dataset.appendChild(datasetRoot);

        Signature sig = new Signature();
        sig.signFile(dataset, "");

        //for output to file, console
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        //for pretty print
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        DOMSource source = new DOMSource(datasetRoot);

        //write to console or file
        StreamResult console = new StreamResult(System.out);
        StreamResult file = new StreamResult(new File("output/dtpr.xml"));

        //write data
        transformer.transform(source, console);
        transformer.transform(source, file);
        System.out.println("DONE");
    }
}
