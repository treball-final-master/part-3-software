package edu.upc.mpegg.classes.interfaces;

public interface MPEGGElement {
    byte[] getKLV();
}
