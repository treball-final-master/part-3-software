package edu.upc.mpegg;

import edu.upc.mpegg.classes.accessunit.AccessUnit;
import edu.upc.mpegg.classes.dataset.Dataset;
import edu.upc.mpegg.classes.datasetgroup.DatasetGroup;
import edu.upc.mpegg.classes.signature.Signature;
import org.xml.sax.SAXException;

import javax.crypto.NoSuchPaddingException;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableEntryException;
import java.security.cert.CertificateException;

public class Main {

    public static void main(String[] args)
            throws InvalidAlgorithmParameterException, NoSuchAlgorithmException,
            UnrecoverableEntryException, CertificateException, KeyStoreException,
            IOException, TransformerException, ParserConfigurationException,
            SAXException, MarshalException, XMLSignatureException, NoSuchPaddingException {
        //Signature signature = new Signature();
        Dataset au1 = new Dataset();
        au1.encryptAndSign("encrypt!".getBytes(StandardCharsets.UTF_8));
        //signature.signFile("purcharse_order.xml", "aupr_1.xml");
    }
}
