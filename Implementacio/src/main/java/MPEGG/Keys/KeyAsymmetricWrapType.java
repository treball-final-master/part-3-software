package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import javax.crypto.spec.OAEPParameterSpec;
import javax.crypto.spec.PSource;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.security.Key;
import java.security.PublicKey;
import java.security.spec.MGF1ParameterSpec;
import java.util.Arrays;
import java.util.Objects;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.BytesUtilities.deserialitzar;
import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;


/**
 * <p>Clase Java para KeyAsymmetricWrapType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="KeyAsymmetricWrapType">
 *   &lt;complexContent>
 *     &lt;extension base="{TFG:MPEG-G:KeyTrasport}KeyTransportType">
 *       &lt;sequence>
 *         &lt;element name="hashFunction">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="sha224"/>
 *               &lt;enumeration value="sha256"/>
 *               &lt;enumeration value="sha384"/>
 *               &lt;enumeration value="sha512"/>
 *               &lt;enumeration value="blake2b"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="maskGenerationHashFunction">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="sha224"/>
 *               &lt;enumeration value="sha256"/>
 *               &lt;enumeration value="sha384"/>
 *               &lt;enumeration value="sha512"/>
 *               &lt;enumeration value="blake2b"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="publicKeyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="wrappedKey" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyAsymmetricWrapType", namespace = "TFG:MPEG-G:KeyTrasport", propOrder = {
        "hashFunction",
        "maskGenerationHashFunction",
        "publicKeyName",
        "wrappedKey"
})
public class KeyAsymmetricWrapType
        extends KeyTransportType {

    /**
     * Hash utilitzat com a parametre.
     */
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String hashFunction;
    /**
     * Mask hash utilitzat com a parametre.
     */
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String maskGenerationHashFunction;
    /**
     * Nom de la clau utilitzada per wrap/unwrap.
     */
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String publicKeyName;
    /**
     * Clau wrapejada.
     */
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected byte[] wrappedKey;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia buida.
     */
    public KeyAsymmetricWrapType() {
        super(null);
    }

    /**
     * Nova instancia amb nom "nom i els valors que es passen com a parametre per fer un wrapAsymmetric a "wp".
     *
     * @param nom    Nom de la clau.
     * @param h      Hash de l'objecte.
     * @param marskh Mascara de hash de l'objecte.
     * @param k      KeyTransport utilitzar per wrapejar.
     * @param wp     Clau a wrapejar.
     * @throws Exception Error al wrapejar la clau.
     */
    public KeyAsymmetricWrapType(String nom, String h, String marskh, KeyTransportType k, Key wp) throws Exception {
        super(nom);
        WrapKey(k, wp, h, marskh);
    }

    //////////
    // SETS //
    //////////

    /**
     * Encripta la key "unwrapped" amb el keyTransport "clau" i els hash que es passen i guarda els valors a l'objecte.
     *
     * @param clau      Clau per wrappejar.
     * @param unwrapped Clau a wrappejar.
     * @throws Exception Error al wrapejar la clau -> Error al cipher / Error al obtenir la clau del KeyTransport.
     */
    protected void WrapKey(KeyTransportType clau, Key unwrapped, String hashfu, String maskha) throws Exception {
        PublicKey enc = (PublicKey) clau.getKey("RSA", Cipher.PUBLIC_KEY);
        if (enc == null) throw new Exception("No es pot obtenir la clau per encriptar");
        this.hashFunction = hashfu;
        this.maskGenerationHashFunction = maskha;
        this.publicKeyName = clau.getName();
        String transformation = String.format("RSA/ECB/OAEPWith%sAndMGF1Padding", hashfu);
        Cipher c = Cipher.getInstance(transformation);
        OAEPParameterSpec par = new OAEPParameterSpec(hashfu, "MGF1", new MGF1ParameterSpec(maskha), PSource.PSpecified.DEFAULT);
        c.init(Cipher.ENCRYPT_MODE, enc, par);
        this.wrappedKey = c.doFinal(serialitzar(unwrapped));
    }

    /**
     * Define el valor de la propiedad protectedKeyName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setpublicKeyName(String value) {
        this.publicKeyName = value;
    }

    //////////
    // GETS //
    //////////
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        Key clau = llistaClaus.getKey(this.publicKeyName).getKey("RSA", Cipher.PRIVATE_KEY);
        if (clau != null) {
            String transformation = String.format("RSA/ECB/OAEPWith%sAndMGF1Padding", this.hashFunction);
            Cipher c = Cipher.getInstance(transformation);

            OAEPParameterSpec par = new OAEPParameterSpec(this.hashFunction, "MGF1", new MGF1ParameterSpec(this.maskGenerationHashFunction), PSource.PSpecified.DEFAULT);
            c.init(Cipher.DECRYPT_MODE, clau, par);
            byte[] aux = c.doFinal(this.wrappedKey);
            return (Key) deserialitzar(aux);
        }
        return null;
    }

    /**
     * Obtiene el valor de la propiedad hashFunction.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getHashFunction() {
        return hashFunction;
    }

    /**
     * Define el valor de la propiedad hashFunction.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setHashFunction(String value) {
        this.hashFunction = value;
    }

    /**
     * Obtiene el valor de la propiedad maskGenerationHashFunction.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getMaskGenerationHashFunction() {
        return maskGenerationHashFunction;
    }

    /**
     * Define el valor de la propiedad maskGenerationHashFunction.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setMaskGenerationHashFunction(String value) {
        this.maskGenerationHashFunction = value;
    }

    /**
     * Obtiene el valor de la propiedad protectedKeyName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getpublicKeyName() {
        return publicKeyName;
    }

    /**
     * Obtiene el valor de la propiedad wrappedKey.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getWrappedKey() {
        return wrappedKey;
    }

    /**
     * Define el valor de la propiedad wrappedKey.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setWrappedKey(byte[] value) {
        this.wrappedKey = value;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.hashFunction, this.maskGenerationHashFunction, this.publicKeyName, Arrays.hashCode(this.wrappedKey), this.keyName);
    }

}
