package main.java.MPEGG.Keys;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class keysList implements Serializable {
    /**
     * HashMap amb els KeyTransport emmagatzamades.
     */
    protected Map<String, KeyTransportType> list = new HashMap<>();

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Crea una nova llista buida.
     */
    public keysList() {
    }


    ///////////////////
    // MODIFICADORES //
    ///////////////////

    /**
     * Afegeix un KeyTransport a la llista de l'objecte.
     *
     * @param k KeyTransport vàlid a afegir.
     */
    public void addKey(KeyTransportType k) {
        String nom = k.getName();
        list.put(nom, k);
    }


    //////////
    // GETS //
    //////////

    /**
     * Obte el KeyTransport amb el nom que es passa.
     *
     * @param Name Nom del KeyTrasport a obtenir.
     * @return KeyTransport amb nom igual a "name" o null en cas de no existir..
     */
    protected KeyTransportType getKey(String Name) {
        KeyTransportType kt = list.get(Name);
        if (kt == null) System.out.print("La llista ha retornat un null perque no hi ha la clau");
        return kt;
    }
}
