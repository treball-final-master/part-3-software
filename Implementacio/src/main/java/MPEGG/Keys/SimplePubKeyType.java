package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Objects;

/**
 * Objecte que guarda la clau publica d'un KeyPair de keyAgreement o RSA.
 */
public class SimplePubKeyType extends KeyTransportType {
    /**
     * Clau a emmagatzemar al KeyTransport. En el cas de RSA serà la publica. I en el de EC una sera la publica i l'altre la privada indiferentment.
     */
    PublicKey Clau;
    PrivateKey privada;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia amb una clau AES de 128bits a .
     */
    protected SimplePubKeyType() {
    }

    /**
     * Nova instancia amb nom i key.
     *
     * @param nom   Nom de la clau.
     * @param claus Claus publica i privada o per DH o per RSA.
     */
    public SimplePubKeyType(String nom, KeyPair claus) {
        super(nom);
        this.Clau = claus.getPublic();
        this.privada = claus.getPrivate();
    }

    //////////
    // SETS //
    //////////

    /**
     * Defineix una nova clau per l'objecte.
     *
     * @param claus Nou keyPair d'on agafar la clau publica.
     */
    protected void setClau(KeyPair claus) {
        this.Clau = claus.getPublic();
    }

    /**
     * Posa clau privada igual a null perque no s'envii.
     */
    protected void perEnviar() {
        this.privada = null;
    }

    //////////
    // GETS //
    //////////
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        if (wrappedKeyType == Cipher.PUBLIC_KEY) {
            if (this.Clau == null) throw new Exception("No hi ha la clau publica definida");
            return this.Clau;
        } else if (wrappedKeyType == Cipher.PRIVATE_KEY) {
            if (this.privada == null) throw new Exception("No hi ha la clau privada definida");
            return this.privada;
        } else throw new Exception("El tipus de clau ha de ser publica o privada");
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(this.privada.getEncoded()), Arrays.hashCode(this.Clau.getEncoded()), this.keyName);
    }
}
