package main.java.MPEGG.Keys;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.security.Key;


/**
 * <p>Clase Java para KeyTransportType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="KeyTransportType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="keyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyTransportType", namespace = "TFG:MPEG-G:KeyTrasport", propOrder = {
        "keyName"
})
@XmlSeeAlso({
        KeyAsymmetricWrapType.class,
        KeySymmetricWrapType.class,
        KeyDerivationType.class,
        KeyEllipticCurveType.class
})
public abstract class KeyTransportType implements Serializable {

    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String keyName;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia de KeyTransport buida.
     */
    public KeyTransportType() {

    }

    /**
     * Nova instancia de KeyTransport amb el nom que es passa.
     *
     * @param nom Nom de la key del KeyTransport.
     */
    protected KeyTransportType(String nom) {
        this.keyName = nom;
    }

    //////////
    // SETS //
    //////////

    /**
     * Obtiene el valor de la propiedad keyName.
     *
     * @return possible object is
     * {@link String }
     */
    public String getKeyName() {
        return keyName;
    }

    //////////
    // GETS //
    //////////

    /**
     * Define el valor de la propiedad keyName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setKeyName(String value) {
        this.keyName = value;
    }

    /**
     * Obte la clau que transporta el KeyTransport.
     *
     * @param wrappedKeyAlgorithm Algoritme de la clau guardada. Vàlids: AES, RSA, ChaCha20 i X25519.
     * @param wrappedKeyType      Tipus de clau. Cipher.[Tipus].
     * @return Key del KeyTransport i null si no es pot desencriptar.
     * @throws Exception Error al obtenir la clau -> Error al iniciar cipher (algoritme, parametres, clau,etc) / Clau no definifa.
     */
    public abstract Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception;

    /**
     * Obte el nom de la clau.
     *
     * @return Nom del KeyTransport.
     */
    protected String getName() {
        return this.keyName;
    }
}
