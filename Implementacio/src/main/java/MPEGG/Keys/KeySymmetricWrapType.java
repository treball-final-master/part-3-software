package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.security.Key;
import java.util.Arrays;
import java.util.Objects;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;


/**
 * <p>Clase Java para KeySymmetricWrapType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="KeySymmetricWrapType">
 *   &lt;complexContent>
 *     &lt;extension base="{TFG:MPEG-G:KeyTrasport}KeyTransportType">
 *       &lt;sequence>
 *         &lt;element name="kek" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="wrappedKey" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeySymmetricWrapType", namespace = "TFG:MPEG-G:KeyTrasport", propOrder = {
        "kek",
        "wrappedKey"
})
public class KeySymmetricWrapType
        extends KeyTransportType {

    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String kek;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected byte[] wrappedKey;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia buida.
     */
    public KeySymmetricWrapType() {
        super(null);
    }

    /**
     * Nova instancia de la clau "wp" amb wrap simetric fet per "k" i amb nom "nom".
     *
     * @param nom Nom de la clau.
     * @param k   KeyTransport que s'utilitzara per fer el Wrap.
     * @param wp  Clau a Wrappejar.
     * @throws Exception Error al wrapejar la clau.
     */
    public KeySymmetricWrapType(String nom, KeyTransportType k, Key wp) throws Exception {
        super(nom);
        WrapKey(k, wp);
    }

    //////////
    // GETS //
    //////////
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        Key clau = llistaClaus.getKey(this.kek).getKey("AES", Cipher.SECRET_KEY);
        if (clau != null) {
            Cipher c = Cipher.getInstance("AESWrap");
            c.init(Cipher.UNWRAP_MODE, clau);
            return c.unwrap(this.wrappedKey, wrappedKeyAlgorithm, wrappedKeyType);
        }
        return null;
    }

    /**
     * Obtiene el valor de la propiedad kek.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getKek() {
        return kek;
    }

    /**
     * Define el valor de la propiedad kek.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setKek(String value) {
        this.kek = value;
    }

    //////////
    // SETS //
    //////////

    /**
     * Obtiene el valor de la propiedad wrappedKey.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getWrappedKey() {
        return wrappedKey;
    }

    /**
     * Define el valor de la propiedad wrappedKey.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setWrappedKey(byte[] value) {
        this.wrappedKey = value;
    }

    /**
     * Encripta la key "unwrapped" amb el keyTransport "clau i guarda els valors.
     *
     * @param clau      Clau per wrappejar.
     * @param unwrapped Clau a wrappejar.
     * @throws Exception Error al wrapejar la clau -> Error al cipher / Error al obtenir la clau del KeyTransport.
     */
    protected void WrapKey(KeyTransportType clau, Key unwrapped) throws Exception {
        Key enc = clau.getKey("AES", Cipher.SECRET_KEY);
        if (enc == null) throw new Exception("No es pot obtenir la clau per encriptar");
        this.kek = clau.getName();
        Cipher c = Cipher.getInstance("AESWrap");
        c.init(Cipher.WRAP_MODE, enc);
        this.wrappedKey = c.wrap(unwrapped);
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.kek, Arrays.hashCode(this.wrappedKey), this.keyName);
    }

}
