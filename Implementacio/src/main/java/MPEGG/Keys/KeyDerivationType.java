package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigInteger;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.Objects;
import java.util.Random;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;


/**
 * <p>Clase Java para KeyDerivationType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="KeyDerivationType">
 *   &lt;complexContent>
 *     &lt;extension base="{TFG:MPEG-G:KeyTrasport}KeyTransportType">
 *       &lt;sequence>
 *         &lt;element name="PRF">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="hmac-sha224"/>
 *               &lt;enumeration value="hmac-sha256"/>
 *               &lt;enumeration value="hmac-sha384"/>
 *               &lt;enumeration value="hmac-sha512"/>
 *               &lt;enumeration value="blake2b"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="passwordName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="salt" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="iterations" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="length" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyDerivationType", namespace = "TFG:MPEG-G:KeyTrasport", propOrder = {
        "prf",
        "passwordName",
        "salt",
        "iterations",
        "length"
})
public class KeyDerivationType
        extends KeyTransportType {

    @XmlElement(name = "PRF", namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String prf;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String passwordName;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected byte[] salt;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected int iterations;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected int length;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova key derivation buida.
     */
    public KeyDerivationType() {
        super(null);
    }

    /**
     * Nova key derivation amb valors aleatoris i hmca = sha-256.
     *
     * @param nom      Nom del keytransport.
     * @param password Nom de la master key.
     */
    public KeyDerivationType(String nom, String password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        super(nom);
        this.passwordName = password;
        this.salt = new byte[200];
        Random random = new Random();
        random.nextBytes(this.salt);
        this.iterations = 10;
        this.length = 256;
        this.prf = "SHA256";
    }

    //////////
    // GETS //
    //////////
    @Override
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        String transformation = String.format("PBKDF2WithHmac%s", this.prf);
        SecretKeyFactory kf = SecretKeyFactory.getInstance(transformation);
        char[] pass = Base64.getEncoder().encodeToString(llistaClaus.getKey(this.passwordName).getKey("AES", Cipher.SECRET_KEY).getEncoded()).toCharArray();
        KeySpec specs = new PBEKeySpec(pass, this.salt, this.iterations, this.length);
        return kf.generateSecret(specs);
    }

    /**
     * Obtiene el valor de la propiedad salt.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getSalt() {
        return salt;
    }

    /**
     * Define el valor de la propiedad salt.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setSalt(byte[] value) {
        this.salt = value;
    }

    /**
     * Obtiene el valor de la propiedad passwordName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getPasswordName() {
        return passwordName;
    }

    /**
     * Define el valor de la propiedad passwordName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setPasswordName(String value) {
        this.passwordName = value;
    }

    /**
     * Obtiene el valor de la propiedad prf.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getPRF() {
        return prf;
    }

    //////////
    // SETS //
    //////////

    /**
     * Define el valor de la propiedad prf.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setPRF(String value) {
        this.prf = value;
    }

    /**
     * Obtiene el valor de la propiedad iterations.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    protected int getIterations() {
        return iterations;
    }

    /**
     * Define el valor de la propiedad iterations.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    protected void setIterations(int value) {
        this.iterations = value;
    }

    /**
     * Obtiene el valor de la propiedad length.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    protected int getLength() {
        return length;
    }

    /**
     * Define el valor de la propiedad length.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    protected void setLength(int value) {
        this.length = value;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.iterations, this.length, Arrays.hashCode(this.salt), this.passwordName, this.prf, this.keyName);
    }

}
