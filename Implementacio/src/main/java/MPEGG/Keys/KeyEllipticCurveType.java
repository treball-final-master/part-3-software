package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Objects;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;


/**
 * <p>Clase Java para KeyEllipticCurveType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="KeyEllipticCurveType">
 *   &lt;complexContent>
 *     &lt;extension base="{TFG:MPEG-G:KeyTrasport}KeyTransportType">
 *       &lt;sequence>
 *         &lt;element name="writerKeyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="readerKeyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="curveName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="X22519"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="hashName">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="blake2b"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyEllipticCurveType", namespace = "TFG:MPEG-G:KeyTrasport", propOrder = {
        "writerKeyName",
        "readerKeyName",
        "curveName",
        "hashName"
})
public class KeyEllipticCurveType
        extends KeyTransportType {

    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected final String curveName = "X25519";
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String writerKeyName;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String readerKeyName;
    @XmlElement(namespace = "TFG:MPEG-G:KeyTrasport", required = true)
    protected String hashName;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia buida.
     */
    public KeyEllipticCurveType() {
        super(null);
    }

    /**
     * Nova instancia apartir de dos KeyTransports de claus publiques per fer el KeyAgrement i obtenir la clau compartida.
     *
     * @param nom  Nom de la clau.
     * @param Key1 Clau publica 1.
     * @param Key2 Clau publica 2.
     */
    public KeyEllipticCurveType(String nom, String Key1, String Key2) {
        super(nom);
        this.writerKeyName = Key1;
        this.readerKeyName = Key2;
    }

    //////////
    // GETS //
    //////////

    /**
     * Obtiene el valor de la propiedad writerKeyName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getWriterKeyName() {
        return writerKeyName;
    }

    /**
     * Define el valor de la propiedad writerKeyName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setWriterKeyName(String value) {
        this.writerKeyName = value;
    }

    /**
     * Obtiene el valor de la propiedad readerKeyName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getReaderKeyName() {
        return readerKeyName;
    }

    /**
     * Define el valor de la propiedad readerKeyName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setReaderKeyName(String value) {
        this.readerKeyName = value;
    }

    /**
     * Obtiene el valor de la propiedad hashName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getHashName() {
        return hashName;
    }

    //////////
    // SETS //
    //////////

    /**
     * Define el valor de la propiedad hashName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setHashName(String value) {
        this.hashName = value;
    }

    /**
     * Obtiene el valor de la propiedad curveName.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getCurveName() {
        return curveName;
    }

    @Override
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        PublicKey clau2 = (PublicKey) llistaClaus.getKey(this.writerKeyName).getKey("X25519", Cipher.PUBLIC_KEY);
        PrivateKey clau1 = (PrivateKey) llistaClaus.getKey(this.readerKeyName).getKey("X25519", Cipher.PRIVATE_KEY);
        if (clau1 != null && clau2 != null) {
            KeyAgreement ka = KeyAgreement.getInstance(this.curveName);
            ka.init(clau1);
            ka.doPhase(clau2, true);
            byte[] encoded = ka.generateSecret();
            if (wrappedKeyType != Cipher.SECRET_KEY) throw new Exception("EC només pot obtenir SecretKeys");
            return new SecretKeySpec(encoded, wrappedKeyAlgorithm);
        }
        return null;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.curveName, this.hashName, this.readerKeyName, this.writerKeyName, this.keyName);
    }

}
