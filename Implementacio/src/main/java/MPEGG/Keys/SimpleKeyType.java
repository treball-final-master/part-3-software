package main.java.MPEGG.Keys;

import javax.crypto.Cipher;
import java.security.Key;
import java.util.Arrays;
import java.util.Objects;

public class SimpleKeyType extends KeyTransportType {
    /**
     * Clau que emmagatzema el KeyTransport.
     */
    Key clau;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia buida.
     */
    protected SimpleKeyType() {
    }

    /**
     * Nova instancia amb nom i key.
     *
     * @param nom  Nom de la clau.
     * @param Clau Key del KeyTransport.
     */
    public SimpleKeyType(String nom, Key Clau) {
        super(nom);
        this.clau = Clau;
    }

    //////////
    // SETS //
    //////////

    /**
     * Defineix una nova clau per l'objecte.
     *
     * @param clau Nova clau.
     */
    protected void setClau(Key clau) {
        this.clau = clau;
    }

    //////////
    // GETS //
    //////////
    public Key getKey(String wrappedKeyAlgorithm, int wrappedKeyType) throws Exception {
        if (clau == null) throw new Exception("No hi ha clau definida");
        if (wrappedKeyType != Cipher.SECRET_KEY) throw new Exception("Nomes s'emmagatzemen secrets keys");
        return this.clau;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(this.clau.getEncoded()), this.keyName);
    }
}
