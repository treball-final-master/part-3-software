package main.java.MPEGG.Types;

import main.java.MPEGG.Keys.*;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static main.java.MPEGG.Utilitats.KeyGens.nouChacha;


/**
 * <p>Clase Java para EncryptionParametersType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="EncryptionParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cipher" type="{TFG:MPEG-G:KeyTrasport}CipherURIType" minOccurs="0"/>
 *         &lt;element name="keyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IV" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="TAG" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Nounce" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="encryptedLocations" use="required" type="{http://www.w3.org/2001/XMLSchema}anyURI" />
 *       &lt;attribute name="configurationID" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EncryptionParametersType", namespace = "TFG:MPEG-G:EncryptedParameters", propOrder = {
        "cipher",
        "keyName",
        "iv",
        "tag",
        "nounce"
})
public class EncryptionParametersType implements Serializable {

    @XmlElement(namespace = "TFG:MPEG-G:EncryptedParameters")
    // Cipher a utilitzar.
    @XmlSchemaType(name = "string")
    protected CipherURIType cipher;
    // Nom de la clau a utilitzar amb el cipher.
    @XmlElement(namespace = "TFG:MPEG-G:EncryptedParameters", required = true)
    protected String keyName;
    @XmlElement(name = "IV", namespace = "TFG:MPEG-G:EncryptedParameters")
    protected byte[] iv; // IV sempre present utilitzat per tots els AES i Chacha20.
    @XmlElement(name = "TAG", namespace = "TFG:MPEG-G:EncryptedParameters")
    protected byte[] tag; // Nomes present si s'aplica el mode DSC en AES.
    @XmlElement(name = "Nounce", namespace = "TFG:MPEG-G:EncryptedParameters")
    protected byte[] nounce; // Nomes present en el cas de chacha20.
    // URI de la localitzacio de les dades.
    @XmlAttribute(name = "encryptedLocations", namespace = "TFG:MPEG-G:EncryptedParameters", required = true)
    @XmlSchemaType(name = "anyURI")
    protected URI encryptedLocations;
    // No s'utilitzara en aquesta implementació. Només estaria present en AU.
    @XmlAttribute(name = "configurationID", namespace = "TFG:MPEG-G:EncryptedParameters")
    protected BigInteger configurationID;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instància buida.
     */
    public EncryptionParametersType() {

    }

    /**
     * Instancia amb la localitzacio especificada.
     *
     * @param id Localitzacio de l'EP.
     */
    public EncryptionParametersType(URI id) {
        this.encryptedLocations = id;
    }

    //////////
    // SETS //
    //////////

    /**
     * Actualitza l'EP amb valors randoms generats correctament. NOMES UTILITZAR QUAN LA LOCALITZACIO ESTIGUI DESENCRIPTADA.
     * Alguns parametres estan predefinits per fer mes facil l'implementacio. Mida i parametres per defecte: Chacha 256, AES 256, hash sha256 i derivation parametres definits.
     *
     * @param cipher   Cipher a utilitzar.
     * @param wrapper  KeyTransport que protegeix la clau que es retorna.
     * @param id       Id del box a encryptar.
     * @param wrapmode Mode per wrapejar la clau amb el wrapper.
     * @param Eliptic2 Null si wrapmode != EC, sino la 2a clau a utilitzar.
     * @return KeyTransport a afegir a KeyTransportAES protegit pel wrapper.
     * @throws Exception Error al wrapejar la clau.
     */
    public KeyTransportType setNewForEPValues(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode, KeyTransportType Eliptic2) throws Exception {
        this.cipher = cipher;
        byte[] IV = new byte[128 / 8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        this.iv = IV;
        if (cipher == CipherURIType.AES_256_GCM || cipher == CipherURIType.AES_128_GCM || cipher == CipherURIType.AES_192_GCM) {
            byte[] ta = new byte[128 / 8];
            random.nextBytes(ta);
            this.tag = ta;
        }
        this.keyName = random.toString();
        this.encryptedLocations = id;
        KeyTransportType kt = null;
        if (cipher.equals(CipherURIType.CHACHA_20_POLY_1305)) {
            switch (wrapmode) {
                case EC:
                    kt = new KeyEllipticCurveType(this.keyName, wrapper.getKeyName(), Eliptic2.getKeyName());
                    break;
                case Simetric:
                    kt = new KeySymmetricWrapType(this.keyName, wrapper, nouChacha(256));
                    break;
                case Asimetric:
                    kt = new KeyAsymmetricWrapType(this.keyName, "SHA-256", "SHA-256", wrapper, nouChacha(256));
                    break;
                case Derivation:
                    kt = new KeyDerivationType(this.keyName, wrapper.getKeyName());
                    break;
            }
            byte[] no = new byte[12];
            random.nextBytes(no);
            this.nounce = no;
        } else {
            switch (wrapmode) {
                case EC:
                    kt = new KeyEllipticCurveType(this.keyName, wrapper.getKeyName(), Eliptic2.getKeyName());
                    break;
                case Simetric:
                    kt = new KeySymmetricWrapType(this.keyName, wrapper, nouAES(256));
                    break;
                case Asimetric:
                    kt = new KeyAsymmetricWrapType(this.keyName, "SHA-256", "SHA-256", wrapper, nouAES(256));
                    break;
                case Derivation:
                    kt = new KeyDerivationType(this.keyName, wrapper.getKeyName());
                    break;
            }
        }
        return kt;
    }

    /**
     * Actualitza l'EP amb un IV random diferent.
     */
    protected void actualitzarIV() {
        byte[] IV = new byte[128 / 8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        this.iv = IV;
    }

    /**
     * Actualitza l'EP amb un Nounce random diferent.
     */
    protected void actualitzarNounce() {
        byte[] IV = new byte[128 / 8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(IV);
        this.nounce = IV;
    }

    /**
     * Obtiene el valor de la propiedad cipher.
     *
     * @return possible object is
     * {@link CipherURIType }
     */
    public CipherURIType getCipher() {
        return cipher;
    }

    /**
     * Define el valor de la propiedad cipher.
     *
     * @param value allowed object is
     *              {@link CipherURIType }
     */
    protected void setCipher(CipherURIType value) {
        this.cipher = value;
    }

    /**
     * Obtiene el valor de la propiedad keyName.
     *
     * @return possible object is
     * {@link String }
     */
    public String getKeyName() {
        return keyName;
    }

    /**
     * Define el valor de la propiedad keyName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setKeyName(String value) {
        this.keyName = value;
    }

    /**
     * Obtiene el valor de la propiedad iv.
     *
     * @return possible object is
     * byte[]
     */
    public byte[] getIV() {
        return iv;
    }

    /**
     * Define el valor de la propiedad iv.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setIV(byte[] value) {
        this.iv = value;
    }

    /**
     * Obtiene el valor de la propiedad tag.
     *
     * @return possible object is
     * byte[]
     */
    public byte[] getTAG() {
        return tag;
    }

    //////////
    // GETS //
    //////////

    /**
     * Define el valor de la propiedad tag.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setTAG(byte[] value) {
        this.tag = value;
    }

    /**
     * Obtiene el valor de la propiedad nounce.
     *
     * @return possible object is
     * byte[]
     */
    public byte[] getNounce() {
        return nounce;
    }

    /**
     * Define el valor de la propiedad nounce.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setNounce(byte[] value) {
        this.nounce = value;
    }

    /**
     * Obtiene el valor de la propiedad encryptedLocations.
     *
     * @return possible object is
     * {@link String }
     */
    public URI getEncryptedLocations() {
        return encryptedLocations;
    }

    /**
     * Define el valor de la propiedad encryptedLocations.
     *
     * @param value allowed object is
     *              {@link String }
     */
    protected void setEncryptedLocations(URI value) {
        this.encryptedLocations = value;
    }

    /**
     * Obtiene el valor de la propiedad configurationID.
     *
     * @return possible object is
     * {@link BigInteger }
     */
    protected BigInteger getConfigurationID() {
        return configurationID;
    }

    /**
     * Define el valor de la propiedad configurationID.
     *
     * @param value allowed object is
     *              {@link BigInteger }
     */
    protected void setConfigurationID(BigInteger value) {
        this.configurationID = value;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.cipher.value(), Arrays.hashCode(this.iv), this.keyName, Arrays.hashCode(this.nounce), Arrays.hashCode(this.tag));
    }

}
