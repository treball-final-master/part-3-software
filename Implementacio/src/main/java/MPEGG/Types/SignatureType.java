package main.java.MPEGG.Types;

import main.java.MPEGG.Utilitats.URI;

import javax.xml.bind.annotation.XmlType;
import javax.xml.crypto.dsig.SignedInfo;
import java.io.Serializable;
import java.util.Objects;


/**
 * Classe Java per representar firma dels objectes dels fitxers MPEG-G. Nomes per objectes que tenen el hashCode correcte, com es el cas de: DatasetProtection, DatasetMetadata, AUProtection, Reference, etc.
 *
 * @author Marti Fernandez
 */
@XmlType(name="Signature",
        namespace = "https://www.w3.org/TR/2002/REC-xmldsig-core-20020212/xmldsig-core-schema.xsd#")
public class SignatureType implements Serializable {
    /**
     * Valor de la firma.
     */
    protected int signatureValue;
    /**
     * URI de la firma.
     */
    protected URI location;

    /**
     * Obte la signatura de l'objecte que es passa com a parametre.
     *
     * @param ob Obtecte del que es fa la firma.
     */
    public SignatureType(Object ob, URI location) {
        this.signatureValue = ob.hashCode();
        this.location = location;
    }

    /**
     * Comprova la signatura de l'objecte actual amb l'obecte que es passa com a parametre.
     *
     * @param ob Objecte a comprovar.
     * @return True si l'objecte es equivalent a la firma. False si no ho és.
     */
    public boolean comprovarSign(Object ob) {
        return (this.signatureValue == ob.hashCode());
    }

    /**
     * Obté la  localització.
     *
     * @return Localització de la que pertany la signatura.
     */
    public URI getLocation() {
        return location;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.signatureValue);
    }
}
