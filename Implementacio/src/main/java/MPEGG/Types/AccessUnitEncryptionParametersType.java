package main.java.MPEGG.Types;


import main.java.MPEGG.enume.CipherURIType;

import javax.crypto.SecretKey;
import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.math.BigInteger;
import java.security.Key;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Objects;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static main.java.MPEGG.Utilitats.KeyGens.nouChacha;


/**
 * <p>Clase Java para AccessUnitEncryptionParametersType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="AccessUnitEncryptionParametersType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="wrappedKey" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
 *                 &lt;attribute name="configurationID" type="{http://www.w3.org/2001/XMLSchema}integer" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="cipher" type="{TFG:MPEG-G:KeyTrasport}CipherURIType"/>
 *         &lt;element name="aublockIV" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="auinIV" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="aublockTAG" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="auinTAG" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="auinNounce" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="aublockNounce" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccessUnitEncryptionParametersType", namespace = "TFG:MPEG-G:AUEncryptedParameters", propOrder = {
        "wrappedKey",
        "cipher",
        "aublockIV",
        "auinIV",
        "aublockTAG",
        "auinTAG",
        "auinNounce",
        "aublockNounce"
})
public class AccessUnitEncryptionParametersType implements Serializable {

    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected AccessUnitEncryptionParametersType.WrappedKey wrappedKey;
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters", required = true)
    @XmlSchemaType(name = "string")
    protected CipherURIType cipher; // Cipher a utilitzar.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters", required = true)
    protected byte[] aublockIV; // IV sempre present utilitzat per tots els AES i Chacha20.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected byte[] auinIV;  // Present utilitzat per tots els AES i Chacha20 per information block.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected byte[] aublockTAG;  // Nomes present si s'aplica el mode DSC en AES.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected byte[] auinTAG; // Nomes present si s'aplica el mode DSC en AES i es un information block.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected byte[] auinNounce; // Nomes present en el cas de chacha20 i si es informationBlock.
    @XmlElement(namespace = "TFG:MPEG-G:AUEncryptedParameters")
    protected byte[] aublockNounce;  // Nomes present en el cas de chacha20.

    //////////
    // GETS //
    //////////

    /**
     * Gets the value of the wrappedKey property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the wrappedKey property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getWrappedKey().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccessUnitEncryptionParametersType.WrappedKey }
     */
    public AccessUnitEncryptionParametersType.WrappedKey getWrappedKey() {
        if (wrappedKey == null) {
            wrappedKey = new WrappedKey();
        }
        return this.wrappedKey;
    }

    /**
     * Obtiene el valor de la propiedad cipher.
     *
     * @return possible object is
     * {@link CipherURIType }
     */
    public CipherURIType getCipher() {
        return cipher;
    }

    /**
     * Define el valor de la propiedad cipher.
     *
     * @param value allowed object is
     *              {@link CipherURIType }
     */
    protected void setCipher(CipherURIType value) {
        this.cipher = value;
    }

    /**
     * Obtiene el valor de la propiedad aublockIV.
     *
     * @return possible object is
     * byte[]
     */
    public byte[] getAublockIV() {
        return aublockIV;
    }

    /**
     * Define el valor de la propiedad aublockIV.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAublockIV(byte[] value) {
        this.aublockIV = value;
    }

    /**
     * Obtiene el valor de la propiedad auinIV.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getAuinIV() {
        return auinIV;
    }

    /**
     * Define el valor de la propiedad auinIV.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAuinIV(byte[] value) {
        this.auinIV = value;
    }

    /**
     * Obtiene el valor de la propiedad aublockTAG.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getAublockTAG() {
        return aublockTAG;
    }


    //////////
    // SETS //
    //////////

    /**
     * Define el valor de la propiedad aublockTAG.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAublockTAG(byte[] value) {
        this.aublockTAG = value;
    }

    /**
     * Obtiene el valor de la propiedad auinTAG.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getAuinTAG() {
        return auinTAG;
    }

    /**
     * Define el valor de la propiedad auinTAG.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAuinTAG(byte[] value) {
        this.auinTAG = value;
    }

    /**
     * Obtiene el valor de la propiedad auinNounce.
     *
     * @return possible object is
     * byte[]
     */
    protected byte[] getAuinNounce() {
        return auinNounce;
    }

    /**
     * Define el valor de la propiedad auinNounce.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAuinNounce(byte[] value) {
        this.auinNounce = value;
    }

    /**
     * Obtiene el valor de la propiedad aublockNounce.
     *
     * @return possible object is
     * byte[]
     */
    public byte[] getAublockNounce() {
        return aublockNounce;
    }

    /**
     * Define el valor de la propiedad aublockNounce.
     *
     * @param value allowed object is
     *              byte[]
     */
    protected void setAublockNounce(byte[] value) {
        this.aublockNounce = value;
    }

    /**
     * Defineix uns nous parametres per encriptar el Block que apunta l'AU. Només modificar-ho quan estigui desencriptat, sino es poden perdre dades.
     *
     * @param cipher  Cipher a utilitzar.
     * @param wrapper Wrapper de la clau, ha de ser la mateixa que per desencriptar l'AU.
     * @throws Exception ??
     */
    public void setNewForAUValues(CipherURIType cipher, Key wrapper) throws Exception {
        this.cipher = cipher;
        byte[] iv = new byte[128 / 8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        this.aublockIV = iv;
        if (cipher.equals(CipherURIType.CHACHA_20_POLY_1305)) {
            random.nextBytes(iv);
            this.aublockNounce = iv;
        }
        SecretKey clau;
        if (cipher.equals(CipherURIType.AES_128_CTR) || cipher.equals(CipherURIType.AES_128_GCM)) {
            clau = nouAES(128);
        } else if (cipher.equals(CipherURIType.AES_192_CTR) || cipher.equals(CipherURIType.AES_192_GCM)) {
            clau = nouAES(192);
        } else if (cipher.equals(CipherURIType.AES_256_CTR) || cipher.equals(CipherURIType.AES_256_GCM)) {
            clau = nouAES(256);
        } else {
            clau = nouChacha(256);
        }
        this.wrappedKey = new WrappedKey();
        this.wrappedKey.setValue(clau);
    }

    protected void newIV() {
        byte[] iv = new byte[128 / 8];
        SecureRandom random = new SecureRandom();
        random.nextBytes(iv);
        this.aublockIV = iv;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return signature dels obtectes d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return Objects.hash(Arrays.hashCode(this.aublockIV), Arrays.hashCode(this.aublockNounce), Arrays.hashCode(this.aublockTAG), this.cipher.value(), this.wrappedKey);
    }


    /**
     * <p>Clase Java para anonymous complex type.
     *
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
     *       &lt;attribute name="configurationID" type="{http://www.w3.org/2001/XMLSchema}integer" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "value"
    })
    public static class WrappedKey implements Serializable {

        @XmlValue
        protected Key value;
        @XmlAttribute(name = "configurationID", namespace = "TFG:MPEG-G:AUEncryptedParameters")
        protected BigInteger configurationID; // No s'utilitzara en aquesta implementació.


        //////////
        // SETS //
        //////////

        /**
         * Obtiene el valor de la propiedad configurationID.
         *
         * @return possible object is
         * {@link BigInteger }
         */
        protected BigInteger getConfigurationID() {
            return configurationID;
        }

        /**
         * Define el valor de la propiedad configurationID.
         *
         * @param value allowed object is
         *              {@link BigInteger }
         */
        protected void setConfigurationID(BigInteger value) {
            this.configurationID = value;
        }


        //////////
        // GETS //
        //////////

        /**
         * Obtiene el valor de la propiedad value.
         *
         * @return possible object is
         * byte[]
         */
        public Key getValue() {
            return value;
        }

        /**
         * Define el valor de la propiedad value.
         *
         * @param value allowed object is
         *              byte[]
         */
        protected void setValue(Key value) {
            this.value = value;
        }

        ///////////////
        // HASH CODE //
        ///////////////

        /**
         * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
         *
         * @return signature dels obtectes d'aquesta classe.
         */
        @Override
        public int hashCode() {
            return Arrays.hashCode(value.getEncoded());
        }

    }

}
