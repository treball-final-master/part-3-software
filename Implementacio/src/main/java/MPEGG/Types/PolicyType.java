package main.java.MPEGG.Types;


import java.io.Serializable;


/**
 * Classe que representa la politica d'acces del fitxer MPEG-G.
 * No especificat en aquesta implementació.
 *
 * @author Marti Fernandez
 */
public class PolicyType implements Serializable {
}
