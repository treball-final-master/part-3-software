package main.java.MPEGG.Data;

import main.java.MPEGG.Boxes.AccessUnitProtectionType;
import main.java.MPEGG.Boxes.DatasetMetadataType;
import main.java.MPEGG.Boxes.DatasetProtectionType;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Types.EncryptionParametersType;
import main.java.MPEGG.Utilitats.BytesUtilities;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.Utilitats.Pair;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.DadesType;
import main.java.MPEGG.enume.WrapType;
import MPEGG.Data.AccesUnit;

import java.io.File;
import java.security.Key;
import java.util.ArrayList;

/**
 * Classe Java per un Dataset d'un fitxer MPEG-G.
 *
 * @author Marti Fernandez
 */
public class Dataset {


    private String folderPath;

    /**
     * Conté els bytes de la box de protecció si està encriptada. Sino == null.
     */
    protected byte[] encryptedBoxP;
    /**
     * Conté els bytes de la box de metadata si està encriptada. Sino == null.
     */
    protected byte[] encryptedBoxMd;
    /**
     * Box de protecció de la classe Dataset si esta desencriptada. Sino == null.
     */
    protected DatasetProtectionType DTProtection;
    /**
     * Box de metadades de la classe Dataset si esta desencriptada. Sino == null.
     */
    protected DatasetMetadataType DTMetadata;
    /**
     * ArrayList amb els accesunits pertanyents al Dataset.
     */
    protected ArrayList<AccesUnit> accesunits;
    /**
     * Llista amb els ReferenceType de Uris encriptades.
     */
    protected ArrayList<ReferenceType> encryptedLocations;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Crea un nou objecte Dataset amb tots els parametres buits.
     */
    protected Dataset() {
        this.encryptedBoxP = null;
        this.encryptedBoxMd = null;
        newDTProtection();
        newDTMetadata();
        this.accesunits = new ArrayList<>();
    }

    /**
     * Crea un nou objecte Dataset apartir dels bytes que es passen, sempre que corresponguin a una classe AccesUnit.
     *
     * @param b Bytes a parsejar a la classe.
     */
    protected Dataset(byte[] b) throws Exception {
        this.encryptedBoxP = null;
        this.encryptedBoxMd = null;
        this.DTProtection = null;
        this.DTMetadata = null;
        this.accesunits = new ArrayList<>();
        parse(b);
    }

    protected Dataset(String folderPath) throws Exception {
        this.folderPath = folderPath;
        parseDatasetComponents();
        parseAccessUnits(folderPath);
    }

    private void parseDatasetComponents() {
        // TODO: PARSE DATASET COMPONENTS
    }

    private void parseAccessUnits(String folderPath) throws Exception{
        accesunits = new ArrayList<>();
        File directoryPath = new File(folderPath);
        String[] directoris = directoryPath.list();
        if (directoris != null) {
            for (String s : directoris) {
                File dgCandidate = new File(folderPath + "/" + s);
                if(dgCandidate.isDirectory()){
                    if(s.contains("au_")){
                        accesunits.add(new AccesUnit(dgCandidate.toString()));
                    }
                }
            }
        }
    };

    //////////
    // SETS //
    //////////

    /**
     * Parseja els bytes que es passen a l'objecte actual.
     *
     * @param aux Byte Array a parsejar.
     * @throws Exception Error al parsejar dataset.
     */
    protected void parse(byte[] aux) throws Exception {
        while (aux != null) {
            Pair<KLV, byte[]> a = BytesUtilities.getFirstKLV(aux);
            aux = a.getValue();
            switch (a.getKey().getK()) {
                case "dtpr":
                    try {
                        this.DTProtection = (DatasetProtectionType) BytesUtilities.deserialitzar(a.getKey().getV());
                        this.encryptedBoxP = null;
                    } catch (Exception e) {
                        // You are converting an invalid stream
                        this.encryptedBoxP = a.getKey().getV();
                        this.DTProtection = null;
                    }
                    break;
                case "dtmd":
                    try {
                        this.DTMetadata = (DatasetMetadataType) BytesUtilities.deserialitzar(a.getKey().getV());
                        this.encryptedBoxMd = null;
                    } catch (Exception e) {
                        // You are converting an invalid stream
                        this.encryptedBoxMd = a.getKey().getV();
                        this.DTMetadata = null;
                    }
                    break;
                case "auva":
                    addAU(a.getKey().getV());
                    break;
                case "encr":
                    encryptedLocations = (ArrayList<ReferenceType>) BytesUtilities.deserialitzar(a.getKey().getV());
                    break;
                default:
                    throw new Exception("Fitxer amb format incorrecte al parsejar Dataset");
            }
        }
    }

    /**
     * Defineix l'id que hi ha a la metadata de l'objecte.
     *
     * @param id Id a definir.
     * @throws Exception Error al accedir a DTMetadata.
     */
    protected void setMeta(Integer id) throws Exception {
        if (DTMetadata == null) throw new Exception("Falta DTMetadata");
        this.DTMetadata.setId(String.valueOf(id));
    }

    /**
     * Defineix un DatasetProtectionType buit a DTProtection.
     */
    protected void newDTProtection() {
        this.DTProtection = new DatasetProtectionType();
    }

    /**
     * Defineix un DatasetMetadataType buit a DTMetadata.
     */
    protected void newDTMetadata() {
        this.DTMetadata = new DatasetMetadataType();
    }

    /**
     * Posa els bytes de la box de metadata encriptat i null a DTMetadata.
     *
     * @param v Bytes de encriptats de la classe DatasetMetadataType.
     */
    protected void setDTMetadataEncrypted(byte[] v) {
        DTMetadata = null;
        encryptedBoxMd = v;
    }

    /**
     * Posa els bytes de la box de protection encriptat i null a DTProtection.
     *
     * @param v Bytes de encriptats de la classe DatasetProtectionType.
     */
    protected void setDTProtectionEncrypted(byte[] v) {
        DTProtection = null;
        encryptedBoxP = v;
    }

    /**
     * Posa a DTMetadata el parametre que es pasa i null a encryptedBoxMD.
     *
     * @param value DatasetMetadataType que sera DTMetadata de l'objecte.
     */
    protected void setDTMetadata(DatasetMetadataType value) {
        DTMetadata = value;
        encryptedBoxMd = null;
    }

    /**
     * Posa a AUProtection el parametre que es pasa i null a encryptedBoxP.
     *
     * @param value DatasetProtectionType que sera DTProtection de la classe.
     */
    protected void setDTProtection(DatasetProtectionType value) {
        this.DTProtection = value;
        this.encryptedBoxP = null;
    }

    /**
     * Defineix un nou AccesUnit.
     *
     * @return ID de l'accesunit.
     */
    protected AccesUnit addAU() {
        AccesUnit aux = new AccesUnit();
        this.accesunits.add(aux);
        return aux;
    }


    /**
     * Defineix un nou AccesUnit amb els bytes que es passen.
     *
     * @param dades Dades que es per
     * @throws Exception Error al crear AccesUnit -> Format no correcte / IOException de ObjectInput.
     */
    protected void addAU(byte[] dades) throws Exception {
        AccesUnit aux = new AccesUnit(dades);
        this.accesunits.add(aux);
    }

    /**
     * Crea un nou AccesUnit i Block amb les dades que es pasen i retorna l'id.
     *
     * @param uri   URI que apunta al Dataset.
     * @param dades Dades que contindra el block.
     */
    public URI newBlock(URI uri, byte[] dades) throws Exception {
        if (uri.getRef() == null) throw new Exception("S'ha d'haver definit un Reference");
        if (antioverlapping(uri.getRef()))
            throw new Exception("Estas intentant afegir un block amb intersecció amb un existent");
        AccesUnit aux;
        aux = this.addAU();
        aux.newBlock(dades, uri.getRef());
        URI ret = new URI(uri);
        ret.setTipus(DadesType.Dades);
        return ret;
    }


    ////////////
    // CRYPTO //
    ////////////

    /**
     * Encrypta l'AUProtection i Metadata de l'accesunit amb id del parametre que es pasa.
     *
     * @param uri URI de l'accesunit que s'encriptaran les boxes. Metadata o Protection segons l'URI.
     * @throws Exception Error al encriptar AU -> DTProtection inexistent o encriptat / URI incorrecte / Error al iniciar el cipher per desencriptar.
     */
    protected void EncryptAU(URI uri) throws Exception {
        if (DTProtection == null) throw new Exception("Error de permisos");
        AccesUnit a = getAU(uri.getRef());
        if (uri.getTipus().equals(DadesType.AUProtection)) {
            AccessUnitProtectionType aupr = a.getAUProtection();
            byte[] res = this.DTProtection.encryptAU(uri, aupr);
            a.setAUProtectionEncrypted(res);
        } else if (uri.getTipus().equals(DadesType.AUMetadata)) {
            throw new Exception("El reference no es pot encriptar, es on es troba la posicio");
        } else throw new Exception("URI incorrecte per aquest nivell");
    }

    /**
     * Encripta el block amb id igual a "id".
     *
     * @param uri URI que apunta a el block a encriptar.
     * @throws Exception Error encriptar Block -> AUProtection inexistent o encryptat / Error al iniciar el cipher per encriptar.
     */
    protected void EncryptBlock(URI uri) throws Exception {
        ReferenceType r = getAU(uri.getRef()).EncryptBlock();
        this.encryptedLocations.add(r);
    }

    protected void DecryptBlock(URI uri) throws Exception {
        getAU(uri.getRef()).DecryptBlock();
        this.encryptedLocations.remove(uri.getRef());
    }

    /**
     * Encripta totes les dades encriptables dels nivells inferiors (block i accesunit boxes).
     *
     * @throws Exception Error al encriptar -> AUProtection inexistent o encryptat / DTProtection inexistent o encriptat / Error al iniciar el cipher per encriptar.
     */
    protected void encryptAll() throws Exception {
        for (AccesUnit auu : this.accesunits) {
            auu.EncryptBlock();
            URI b = new URI();
            b.apuntarAccesUnitPr(0, 0, auu.getReference());
            EncryptAU(b);
        }
    }

    /**
     * Desencripta totes les dades encriptables dels nivells inferiors (block i accesunit boxes).
     *
     * @throws Exception Error al desencriptar -> Box PR o MD d'AccesUnit no existeix / AUProtection inexistent / Error al iniciar el cipher per desencriptar.
     */
    protected void searchAndDecryptEncryptedLocations() throws Exception {
        for (AccesUnit a : this.accesunits) {
            if (a.isEncryptedBoxP()) {
                URI b = new URI();
                b.apuntarAccesUnitPr(0, 0, a.getReference());
                DecryptAU(b);
            }
            a.DecryptBlock();
        }
    }

    /**
     * Desencripta les boxes de l'acces unit que s'especifica.
     *
     * @param uri URI de l'accesunit box a desencriptar.
     * @throws Exception Error al desencriptar -> Error al iniciar el cipher per desencriptar / URI incorrecte (no es cap Box).
     */
    protected void DecryptAU(URI uri) throws Exception {
        AccesUnit a = getAU(uri.getRef());
        if (uri.getTipus().equals(DadesType.AUProtection)) {
            byte[] aupr = a.getAUProtectionEncry();
            AccessUnitProtectionType res = (AccessUnitProtectionType) this.DTProtection.decryptAU(uri, aupr);
            a.setAUProtection(res);
        } else if (uri.getTipus().equals(DadesType.AUMetadata)) {
            throw new Exception("Reference mai esta encriptat");
        } else throw new Exception("URI incorrecte");
    }


    public void setNewAUParams(ReferenceType AU_Id, CipherURIType cipher, Key wrapper) throws Exception {
        this.getAU(AU_Id).getAUProtection().getAccessUnitEncryptionParameters().setNewForAUValues(cipher, wrapper);
    }

    protected void setNewDTEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode, KeyTransportType Eliptic2) throws Exception {
        this.DTProtection.setNewEP(cipher, wrapper, id, wrapmode, Eliptic2);
    }

    public void setNewDTEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode) throws Exception {
        this.DTProtection.setNewEP(cipher, wrapper, id, wrapmode);
    }

    /**
     * Modifica l'EncryptionParametersType que tingui el mateix id que EncryptionParametersType que es passa.
     *
     * @param dgpt EncryptionParametersType que substituira l'anterior amb mateix id.
     */
    protected void changeAUEP(EncryptionParametersType dgpt) {
        this.DTProtection.changeEP(dgpt);
    }

    /**
     * Elimina l'EncryptionParametersType de DTProtection que tingui l'id que es pasa com a parametre.
     *
     * @param id URI del EncryptionParametersType a eliminar. Nomes tenint en compte el tipus i au id.
     */
    protected void deleteAUEP(URI id) {
        this.DTProtection.deleteEP(id);
    }

    ///////////////
    // CONSULTES //
    ///////////////

    /**
     * Obte si la box de proteccio esta encriptada.
     *
     * @return True = encriptat / False = no encriptat.
     * @throws Exception No hi ha cap boc de protecció.
     */
    protected boolean isEncryptedBoxP() throws Exception {
        if (DTProtection == null && encryptedBoxP == null) throw new Exception("Error box");
        return (DTProtection == null);
    }

    /**
     * Obte si la box de metadata esta encriptada.
     *
     * @return True = encriptat / False = no encriptat.
     * @throws Exception No hi ha cap boc de metadata.
     */
    protected boolean isEncryptedBoxMd() throws Exception {
        if (DTMetadata == null && encryptedBoxMd == null) throw new Exception("Error box");
        return (DTMetadata == null);
    }

    /**
     * Comprova si al afegir un block hi hauria overlapping.
     *
     * @param r Reference type a comprovar amb els existents.
     * @return True si hi ha intersecció amb un existent, false en cas contrari.
     */
    protected boolean antioverlapping(ReferenceType r) {
        for (AccesUnit a : accesunits) {
            if (a.intersects(r)) return true;
        }
        return false;
    }

    //////////
    // GETS //
    //////////

    /**
     * Obte els bytes de l'objecte amb les dades actuals de la classe.
     *
     * @return Byte Array de protection + metadata + accesunits.
     * @throws Exception Falta alguna box.
     */
    protected byte[] actualitzarBytes() throws Exception {
        byte[] pr;
        byte[] md;
        if (encryptedBoxP == null && DTProtection != null) {
            pr = this.DTProtection.getKLV().toBytes();
        } else if (encryptedBoxP != null && DTProtection == null) {
            pr = new KLV("dtpr", encryptedBoxP).toBytes();
        } else throw new Exception("Error amb boxes");
        if (encryptedBoxMd == null && DTMetadata != null) {
            md = this.DTMetadata.getKLV().toBytes();
        } else if (encryptedBoxMd != null && DTMetadata == null) {
            md = new KLV("dtmd", encryptedBoxMd).toBytes();
        } else throw new Exception("Error amb boxes");
        byte[] aux = BytesUtilities.addArrays(pr, md);
        BytesUtilities.addArrays(aux, new KLV("encr", BytesUtilities.serialitzar(encryptedLocations)).toBytes());
        return BytesUtilities.addArrays(aux, getAUBytes());
    }

    /**
     * Obte els bytes concadenats dels objectes accesunit amb el seu KLV.
     *
     * @return Byte Array amb els valors.
     * @throws Exception Algun AccesUnit no esta complert i no pot obtenir el KLV.
     */
    protected byte[] getAUBytes() throws Exception {
        byte[] ret = new byte[0];
        for (AccesUnit accesunit : this.accesunits) {
            ret = BytesUtilities.addArrays(ret, accesunit.getKLV().toBytes());
        }
        return ret;
    }

    /**
     * Obte el KLV de l'objecte actual.
     *
     * @return KLV de l'objecte
     * @throws Exception Error al obtenir els bytes. Falta alguna box.
     */
    protected KLV getKLV() throws Exception {
        return new KLV("dtva", actualitzarBytes());
    }

    /**
     * Obte els ids dels acces units.
     *
     * @return ArrayList amb els ids dels accesunits de l'objecte.
     */
    public ArrayList<ReferenceType> getAURefs() {
        ArrayList<ReferenceType> ret = new ArrayList<>();
        for (AccesUnit au: accesunits) {
            ret.add(au.getReference());
        }
        return ret;
    }

    /**
     * Obte l'acces unit amb l'id que es pasa com a parametre.
     *
     * @param id ID de l'accesunit que es vol aconseguir.
     * @return AccesUnit amb id "id".
     * @throws Exception Id no exitent.
     */
    protected AccesUnit getAU(ReferenceType id) throws Exception {
        AccesUnit ret = null;
        for(AccesUnit au: accesunits) {
            if(au.reference.equals(id)) ret = au;
        }
        if (ret == null) throw new Exception("Id erroni");
        return ret;
    }

    /**
     * Obte els bytes de DTProtection encriptat.
     *
     * @return DatasetProtectionType de l'obtecte encriptat. Null si esta desencriptat o no hi es.
     */
    protected byte[] getDTProtEncry() {
        return encryptedBoxP;
    }

    /**
     * Obte els bytes de DTMetadata encriptat.
     *
     * @return DatasetMetadataType de l'obtecte encriptat. Null si esta desencriptat o no hi es.
     */
    protected byte[] getDTMetEncry() {
        return encryptedBoxMd;
    }

    public byte[] readBlock(URI uri) throws Exception {
        if (getAU(uri.getRef()).intersectsList(this.encryptedLocations)) {
            this.DecryptBlock(uri);
        }
        return getAU(uri.getRef()).readBlock();
    }

}
