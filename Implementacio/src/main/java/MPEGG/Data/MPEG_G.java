package main.java.MPEGG.Data;

import MPEGG.enume.BoxKey;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.BytesUtilities;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.Utilitats.Pair;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.Data.DatasetGroup;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.crypto.Data;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Classe Java per un fitxer MPEG-G.
 *
 * @author Marti Fernandez
 */

public class MPEG_G {

    public String majorBrand;
    public String minorVersion;
    public ArrayList<DatasetGroup> datasetGroups;
    public int numberOfDg;
    /**
     * Objecte amb les claus pel fitxer.
     */
    public static keysList llistaClaus;
    /**
     * ArrayList amb els DatasetGroup presents al fitxer.
     */
    protected ArrayList<DatasetGroup> dg;

    ///////////////////
    // CONSTRUCTORES //
    //////////////////

    /**
     * Crea un esquema del fitxer buit.
     */
    public MPEG_G() {
        this.dg = new ArrayList<>();
        llistaClaus = new keysList();
    }

    public MPEG_G(String folderPath) throws Exception{
        parseMpeggFolder(folderPath);
    }


    //////////
    // SETS //
    //////////

    /**
     * Parseja els bytes que es passen a l'objecte actual.
     *
     * @param aux Byte Array a parsejar.
     * @throws Exception Error al parsejar. Format no correcte.
     */
    protected void parse(byte[] aux) throws Exception {
        while (aux != null) {
            Pair<KLV, byte[]> a = BytesUtilities.getFirstKLV(aux);
            aux = a.getValue();
            if (a.getKey().getK().equals("llcl")) {
                ByteArrayInputStream in = new ByteArrayInputStream(a.getKey().getV());
                ObjectInputStream is = new ObjectInputStream(in);
                llistaClaus = (keysList) is.readObject();
            } else if (a.getKey().getK().equals("dgva")) {
                this.dg.add(new DatasetGroup(a.getKey().getV()));
            } else {
                try {
                    throw new Exception("Fitxer incorrecte");
                } catch (Exception e) {
                    throw new Exception("Format del fitxer incorrecte");
                }
            }
        }
    }

    /**
     * Defineix un nou DatasetGroup.
     *
     * @return ID del DatasetGroup.
     * @throws Exception Falta DGMetadata.
     */
    public Integer addDatasetGroup() throws Exception {
        DatasetGroup aux = new DatasetGroup();
        this.dg.add(aux);
        Integer id = this.dg.indexOf(aux);
        aux.setMeta(id);
        return id;
    }

    /**
     * Afegeix un Block al fitxer creant els DG, DT, AU necessaris si no estan creats.
     *
     * @param uri Un URI amb dades fins a un Dataset indiferent el tipus de dades. Els ids del DG i DT si són -1 en crea un de nou.
     * @return URI del block.
     * @throws Exception DTMetadata o DGMetadata no exitent o esta encriptat. O DG id no trobat.
     */
    public URI newBlock(URI uri, byte[] dades) throws Exception {
        DatasetGroup aux;
        if (uri.getDg_id() == -1) {
            uri.setDg_id(addDatasetGroup());
        } else if (this.dg.size() <= uri.getDg_id()) throw new Exception("Error id DG");
        aux = this.dg.get(uri.getDg_id());
        return aux.newBlock(uri, dades);
    }


    ////////////
    // BYTES //
    ///////////

    /**
     * Llegeix els bytes del fitxer i retorna els bytes.
     *
     * @param nomFitxer Nom del fitxer a llegir.
     * @throws Exception Fitxer no correcte.
     */
    protected byte[] readFileBytes(String nomFitxer) throws Exception {
        String fit = String.format("./%s.mpg", nomFitxer);
        File fitxer = new File(fit);
        byte[] aux = null;
        try {
            aux = Files.readAllBytes(fitxer.toPath());
        } catch (IOException e) {
            System.out.println("Fitxer no trobat");
        }
        System.out.println(aux.length);
        String auxS = "MPEG File Part 3 / 2021 TFM Implementation\n";
        byte[] auxB = auxS.getBytes();
        int longitud = auxB.length;
        assert aux != null;
        if (!Arrays.equals(auxB, Arrays.copyOfRange(aux, 0, longitud))) {
            throw new Exception("Fitxer no MPEG-G");
        }
        return Arrays.copyOfRange(aux, longitud, aux.length);
    }

    /**
     * Return the byte array of a file
     *
     * @param fileName  The path to the file is going to be read
     * @return Byte Array of the file
     */

    protected byte[] parseFileBytes(String fileName) {
        File fitxer = new File(fileName);
        byte[] byteArray = null;
        try {
            byteArray = Files.readAllBytes(fitxer.toPath());
        } catch (IOException e) {
            System.out.println("Fitxer no trobat");
        }
        return byteArray;
    }

    /**
     * Escriu els bytes pasats com a parametre a als bytes del fitxer.
     *
     * @param nomFitxer Nom del fitxer a escriure.
     * @param dades     Dades a posar al fitxer.
     * @throws Exception Path incorrecte.
     */
    protected void writeFileBytes(String nomFitxer, byte[] dades) throws Exception {
        String fitxer = String.format("./%s.mpg", nomFitxer);
        Path path = Paths.get(fitxer);
        try {
            Files.write(path, dades);
        } catch (IOException e) {
            throw new Exception("Path incorrecte");
        }
    }


    //////////
    // GETS //
    //////////

    /**
     * Obte els ids dels DatasetGroup.
     *
     * @return ArrayList amb els ids dels datasetgroup de l'objecte.
     */
    public ArrayList<Integer> getDGIds() {
        ArrayList<Integer> ids = new ArrayList<>();
        for (int i = 0; i < this.dg.size(); ++i) {
            ids.add(i);
        }
        return ids;
    }

    /**
     * Obte el DatasetGroup amb l'id que es pasa com a parametre.
     *
     * @param iddg ID del DatasetGroup que es vol aconseguir.
     * @return DatasetGroup amb id "id".
     * @throws Exception Id no exitent.
     */
    public DatasetGroup getDatasetGroup(Integer iddg) throws Exception {
        if (iddg > this.dg.size() || iddg < 0) throw new Exception("Id erroni");
        return this.dg.get(iddg);
    }

    /**
     * Obte els bytes de l'objecte amb les dades actuals de la classe.
     *
     * @return Byte Array de protection + metadata + datasetgroup.
     * @throws Exception Falta alguna box.
     */
    protected byte[] getBytes() throws Exception {
        byte[] aux = BytesUtilities.serialitzar(llistaClaus);
        System.out.println(llistaClaus);
        KLV claus = new KLV("llcl", aux);
        for (DatasetGroup datasetGroup : dg) {
            aux = BytesUtilities.addArrays(claus.toBytes(), datasetGroup.getKLV().toBytes());
        }
        return aux;
    }


    ////////////
    // CRYPTO //
    ////////////

    /**
     * Encripta el block que apunta l'URI que es pasa com a parametre.
     *
     * @param uri URI que apunta al block.
     * @throws Exception AUProtection inexistent o encryptat / Error al iniciar el cipher per encriptar.
     */
    protected void encryptBlock(URI uri) throws Exception {
        this.dg.get(uri.getDg_id()).EncryptBlock(uri);
    }

    /**
     * Encrypta l'AUProtection i Metadata de l'accesunit amb id dels parametres que es pasen.
     *
     * @param uri URI de l'AU a encriptar. Metadata si es vol fer les metadates o Protection si es vol encriptar el protection.
     * @throws Exception DTProtection inexistent o encriptat / URI incorrecte / Error al iniciar el cipher per desencriptar.
     */
    protected void encryptAU(URI uri) throws Exception {
        this.dg.get(uri.getDg_id()).EncryptAU(uri);
    }

    /**
     * Encrypta el DTProtection del Dataset amb id del parametre que es pasa.
     *
     * @param uri URI del dataset que s'encriptara amb Metadata si es vol fer les metadates o Protection si es vol encriptar el protection.
     * @throws Exception DGProtection inexistent o encriptat / O URI incorrecte / Error al iniciar el cipher amb EP.
     */
    protected void encryptDT(URI uri) throws Exception {
        this.dg.get(uri.getDg_id()).EncryptDT(uri);
    }

    /**
     * Encripta totes les dades encriptables dels nivells inferiors (block, accesunit boxes, dataset boxes i datasetgroup boxes).
     *
     * @throws Exception Algun Box inexistent o encriptat / Error al iniciar el cipher per encriptar.
     */
    public void encryptAll() throws Exception {
        for (DatasetGroup dgg : this.dg) {
            dgg.encryptAll();
        }
    }

    /**
     * Desencripta totes les boxes i dades del fitxer.
     *
     * @throws Exception Error al desencriptar -> Algun Box inexistent o encriptat / Error al iniciar el cipher per encriptar.
     */
    public void decryptAll() throws Exception {
        for (DatasetGroup dgg : this.dg) {
            dgg.searchAndDecryptEncryptedLocations();
        }
    }


    //////////
    // FILE //
    //////////

    /**
     * Llegeix el fitxer i el parseja a les classes.
     *
     * @param nomFitxer Nom del fitxer a llegir.
     * @throws Exception Format fitxer incorrecte.
     */
    public void readFile(String nomFitxer) throws Exception {
        byte[] dat;
        dat = readFileBytes(nomFitxer);
        this.parse(dat);
    }


    /**
     * Parse the MPEG-G file with the folder structure defined on the meet's
     *
     * @param folderPath path to the root of the folder
     *
     */
    public void parseMpeggFolder(String folderPath) throws Exception{
        KLV headerKLV = KLV.parseFile(folderPath, BoxKey.FileHeader);
        parseHeaderKLV(headerKLV);
        parseDatasetGroups(folderPath);
    }

    private void parseDatasetGroups(String folderPath) throws Exception {
        numberOfDg = 0;
        datasetGroups = new ArrayList<>();
        File directoryPath = new File(folderPath);
        String[] directoris = directoryPath.list();
        if (directoris != null) {
            for (String s : directoris) {
                File dgCandidate = new File(folderPath + "/" + s);
                if(dgCandidate.isDirectory()){
                    if(s.contains("dg_")){
                        datasetGroups.add(new DatasetGroup(dgCandidate.toString()));
                        numberOfDg += 1;
                    }
                }
            }
        }
    }

    private void parseHeaderKLV(KLV headerKLV){
        byte[] aux = headerKLV.getV();
        majorBrand = new String(aux, 0, 6);
        minorVersion = new String(aux, 6, 4);
        if(headerKLV.getLength() > 10){
            // TODO: IMPLEMENT compatible_brand PARSER
        }

    }




    /**
     * Escriu el fitxer format per l'objecte actual.
     *
     * @param nomFitxer Nom del fitxer a escriure.
     * @throws Exception Falta alguna box / Path incorrecte.
     */
    public void writeFile(String nomFitxer) throws Exception {
        String id = "MPEG File Part 3 / 2021 TFM Implementation\n";
        byte[] escriure = BytesUtilities.addArrays(id.getBytes(), getBytes());
        writeFileBytes(nomFitxer, escriure);
    }

    @Override
    public String toString(){
        String mpegg = "MPEG_G: \n\tMajor Brand: " + majorBrand + " \n\tMinor Version: " + minorVersion + "\n";
        StringBuilder DatasetGroups = new StringBuilder("DatasetGroups:" + "\n\tNumber of DatasetGroups: " + numberOfDg + "\n");
        for (DatasetGroup datasetGroup: datasetGroups) {
            DatasetGroups.append(datasetGroup.toString());
        }
        return mpegg + DatasetGroups;
    }

}
