package MPEGG.Data;

import MPEGG.enume.ACCESS_UNIT_TYPE;
import MPEGG.enume.BoxKey;
import main.java.MPEGG.Boxes.AccessUnitProtectionType;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Types.AccessUnitEncryptionParametersType;
import main.java.MPEGG.Utilitats.BytesUtilities;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.Utilitats.Pair;
import main.java.MPEGG.Data.Block;
import main.java.MPEGG.Utilitats.URI;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;

/**
 * Classe Java per un AccesUnit d'un fitxer MPEG-G.
 *
 * @author Marti Fernandez
 */

@XmlRootElement
public class AccesUnit {

    private String folderPath;
    private int id;
    private int numBlocks;
    private int parameterSetId;
    private ACCESS_UNIT_TYPE type;
    private Integer readsCount;

    private Short mmThreshold;
    private Short mmCount;

    /**
     * Conté els bytes de la box de protecció si està encriptada. Sino == null.
     */
    protected byte[] encryptedBoxP;
    /**
     * Box de protecció de la classe AccesUnit si esta desencriptada. Sino == null.
     */
    protected AccessUnitProtectionType AUProtection;
    /**
     * Box de posicio dels block de la classe AccesUnit.
     */
    protected ReferenceType reference;
    /**
     * Bloc al que apunta l'acces unit.
     */
    protected Block bloc;

    protected ArrayList<Block> blocks;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Crea un nou objecte AccesUnit amb tots els parametres buits.
     */
    protected AccesUnit() {
        this.encryptedBoxP = null;
        newProtection();
        newMeta();
        this.bloc = null;
    }

    /**
     * Crea un nou objecte AccesUnit apartir dels bytes que es passen, sempre que corresponguin a una classe AccesUnit.
     *
     * @param b Bytes a parsejar a la classe.
     */
    protected AccesUnit(byte[] b) throws Exception {
        this.encryptedBoxP = null;
        this.AUProtection = null;
        this.reference = null;
        this.bloc = null;
        parse(b);
    }

    protected AccesUnit(String folderPath) throws Exception{
        this.folderPath = folderPath;
        parseAccessUnitHeader(folderPath);
        parseProtection(folderPath);
        parseBlocks(folderPath);
    }

    private void parseAccessUnitHeader(String folderPath) throws Exception {
        KLV headerKLV = KLV.parseFile(folderPath, BoxKey.AccessUnitHeader);
        parseHeaderKLV(headerKLV);
    }

    private void parseProtection(String folderPath) throws Exception {
        KLV headerKLV = KLV.parseFile(folderPath, BoxKey.AccessUnitProtecction);
        parseProtectionKLV(headerKLV);
    }

    private void parseProtectionKLV(KLV headerKLV) {

    }

    private void parseHeaderKLV(KLV headerKLV){
        byte[] aux = headerKLV.getV();
        int bufferIndex = 0;
        id = BytesUtilities.parseuint32(aux,bufferIndex);
        bufferIndex += 4;
        numBlocks = BytesUtilities.parserShort(aux,bufferIndex);
        bufferIndex += 2;
        type = parseAUType(aux, bufferIndex);
    }

    private ACCESS_UNIT_TYPE parseAUType(byte[] aux, int index) {
        // TODO: PARSE ACCESS_UNIT_TYPE
        return ACCESS_UNIT_TYPE.HM_TYPE_AU;
    }

    private void parseBlocks(String folderPath) throws Exception{
        blocks = new ArrayList<>();
        File directoryPath = new File(folderPath);
        String[] directoris = directoryPath.list();
        if (directoris != null) {
            for (String s : directoris) {
                File blockCandidate = new File(folderPath + "/" + s);
                if(blockCandidate.isDirectory()){
                    if(s.contains("block_")){
                        blocks.add(new Block(blockCandidate.toString()));
                    }
                }
            }
        }
    }


    //////////
    // SETS //
    //////////
    /**
     * Parseja els bytes que es passen a l'objecte actual.
     *
     * @param aux Byte Array a parsejar. Els blocs es parsejen com encriptats.
     * @throws Exception Error de format al parsejar.
     */
    protected void parse(byte[] aux) throws Exception {
        boolean pr = false;
        boolean md = false;
        while (aux != null && (!pr || !md)) {
            Pair<KLV, byte[]> a = BytesUtilities.getFirstKLV(aux);
            aux = a.getValue();
            switch (a.getKey().getK()) {
                case "aupr":
                    pr = true;
                    try {
                        this.AUProtection = (AccessUnitProtectionType) BytesUtilities.deserialitzar(a.getKey().getV());
                        this.encryptedBoxP = null;
                    } catch (Exception e) {
                        // You are converting an invalid stream
                        this.encryptedBoxP = a.getKey().getV();
                        this.AUProtection = null;
                    }
                    break;
                case "rfmd":
                    md = true;
                    this.reference = (ReferenceType) BytesUtilities.deserialitzar(a.getKey().getV());
                    break;
                default:
                    throw new Exception("Error de format al parsejar AU");
            }
        }
        this.bloc = (Block) BytesUtilities.deserialitzar(aux);
    }

    /**
     * Crea un nou box buit de metadata per l'objecte.
     */
    protected void newMeta() {
        this.reference = new ReferenceType();
    }

    /**
     * Crea un nou box buit de protection per l'objecte.
     */
    protected void newProtection() {
        this.AUProtection = new AccessUnitProtectionType();
    }

    /**
     * Posa els bytes de la box de protection encriptat i null a AUProtection.
     *
     * @param v Bytes de encriptats de la classe AccessUnitProtectionType.
     */
    protected void setAUProtectionEncrypted(byte[] v) {
        this.AUProtection = null;
        this.encryptedBoxP = v;
    }

    /**
     * Posa a bloc un block amb els bytes que es pasen.
     *
     * @param b Bytes que contindra el block.
     * @param r Reference de l'accesunit. S'ha de comprovar previament que no hi ha overlaping.
     */
    protected void newBlock(byte[] b, ReferenceType r) throws Exception {
        if(r == null) throw new Exception("Reference no pot ser null");
        this.bloc = new Block(b);
        this.reference.getPosition().setStart(r.getPosition().getStart());
        this.reference.getPosition().setEnd(r.getPosition().getEnd());
        this.reference.setChromosomeName(r.getChromosomeName());
    }

    /**
     * Encrypta el block al que apunta amb les dades de AUProtection.
     *
     * @return Retorna el Reference del block encriptat.
     * @throws Exception AUPR Inexistent o encriptat.
     */
    protected ReferenceType EncryptBlock() throws Exception {
        if (AUProtection == null) {
            throw new Exception("AUProtection inexistent o encriptat");
        }
        byte[] block = this.bloc.getBytes();
        byte[] res = this.AUProtection.encryptBlock(block);
        bloc.setBytes(res);
        return this.reference;
    }

    /**
     * Defineix aupt com a AccesUnitEncryptionParameters de AUProtection.
     *
     * @param aupt AccessUnitEncryptionParametersType que sera part de AUProtection.
     */
    protected void setBlockEP(AccessUnitEncryptionParametersType aupt) {
        if (AUProtection == null) newProtection();
        this.AUProtection.setEP(aupt);
    }


    ////////////
    // CRYPTO //
    ////////////
    /**
     * Desencripta el block que apunta AccesUnit.
     *
     * @throws Exception AUPR Inexistent o encriptat.
     */
    protected void DecryptBlock() throws Exception {
        if (AUProtection == null) throw new Exception("AUProtection inexistent o encriptat");
        byte[] block = this.bloc.getBytes();
        byte[] res = this.AUProtection.decryptBlock(block);
        bloc.setBytes(res);
    }

    /**
     * Obte si la box de proteccio esta encriptada.
     *
     * @return True = encriptat / False = no encriptat.
     * @throws Exception AUPR Inexistent.
     */
    protected boolean isEncryptedBoxP() throws Exception {
        if (AUProtection == null && encryptedBoxP == null) throw new Exception("Box Pr inexistent");
        return (AUProtection == null);
    }

    ///////////////
    // CONSULTES //
    ///////////////
    /**
     * Comprova si el Reference que es passa intersecta amb el que hi ha a aquests AU.
     *
     * @param r Reference Type a comprovar.
     * @return True = intersecta / False = no intersecta.
     */
    protected boolean intersects(ReferenceType r) {
        if (!r.getChromosomeName().equals(this.reference.getChromosomeName())) return false;
        if ((r.getPosition().getStart() >= this.reference.getPosition().getStart()) && (r.getPosition().getStart() <= this.reference.getPosition().getEnd())) {
            return true;
        }
        return ((r.getPosition().getEnd() >= this.reference.getPosition().getStart()) && (r.getPosition().getEnd() <= this.reference.getPosition().getEnd()));
    }

    /**
     * Comprova si ell Reference de l'AU intersecta amb algun dels que hi ha a un ArrayList.
     *
     * @param ref ArrayList a comprovar.
     * @return True = algun intersecta / Falsa = cap intersecta.
     */
    protected boolean intersectsList(ArrayList<ReferenceType> ref) {
        if (ref == null) return false;
        for (ReferenceType r : ref) {
            if (intersects(r)) return true;
        }
        return false;
    }

    //////////
    // GETS //
    //////////
    /**
     * Obte els bytes de l'objecte amb les dades actuals de la classe.
     *
     * @return Byte Array de protection + metadata + box.
     * @throws Exception Falta alguna box.
     */
    protected byte[] getBytes() throws Exception {
        byte[] pr;
        byte[] md;
        if (encryptedBoxP == null && AUProtection != null) {
            pr = this.AUProtection.getKLV().toBytes();
        } else if (encryptedBoxP != null && AUProtection == null) {
            pr = new KLV("aupr", encryptedBoxP).toBytes();
        } else throw new Exception("No hi ha protection box a l'AU");
        if (reference != null) {
            md = this.reference.getKLV().toBytes();
        } else throw new Exception("No hi ha Reference a l'AU");
        byte[] aux = BytesUtilities.addArrays(pr, md);
        return BytesUtilities.addArrays(aux, BytesUtilities.serialitzar(this.getBlock()));
    }

    /**
     * Obte el KLV de l'objecte actual.
     *
     * @return KLV de l'objecte.
     */
    protected KLV getKLV() throws Exception {
        return new KLV("auva", getBytes());
    }

    /**
     * Obte l'objecte Block al que apunta l'acces unit.
     *
     * @return Block apuntat.
     */
    protected Block getBlock() {
        return this.bloc;
    }

    /**
     * Obte les dades del block que apunta l'acces unit.
     *
     * @return Bytes del block que apunta l'accesunit.
     */
    protected byte[] readBlock() {
        return this.bloc.getBytes();
    }

    /**
     * Obte l'AUProtection de l'Objecte.
     *
     * @return AUProtection de l'obtecte. Null si esta encriptat o no hi es.
     */
    protected AccessUnitProtectionType getAUProtection() {
        return this.AUProtection;
    }

    /**
     * Posa a AUProtection el parametre que es pasa i null a encryptedBoxP.
     *
     * @param value AccessUnitProtectionType que sera AUProtection de la classe.
     */
    protected void setAUProtection(AccessUnitProtectionType value) {
        this.AUProtection = value;
        this.encryptedBoxP = null;
    }

    /**
     * Obte el Reference de l'Objecte.
     *
     * @return Reference de l'obtecte.
     */
    protected ReferenceType getReference() {
        return this.reference;
    }

    /**
     * Posa a reference el parametre que es pasa.
     *
     * @param value ReferenceType que sera reference de la classe.
     */
    protected void setReference(ReferenceType value) {
        ReferenceType ref = new ReferenceType();
        ref.copiRef(value);
        this.reference = ref;
    }

    /**
     * Obte els bytes de l'AUProtection encriptat.
     *
     * @return AUProtection de l'obtecte encriptat. Null si esta desencriptat o no hi es.
     */
    protected byte[] getAUProtectionEncry() {
        return encryptedBoxP;
    }
}
