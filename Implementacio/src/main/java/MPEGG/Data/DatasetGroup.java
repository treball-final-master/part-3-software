package main.java.MPEGG.Data;

import MPEGG.enume.BoxKey;
import main.java.MPEGG.Boxes.DatasetGroupMetadataType;
import main.java.MPEGG.Boxes.DatasetGroupProtectionType;
import main.java.MPEGG.Boxes.DatasetMetadataType;
import main.java.MPEGG.Boxes.DatasetProtectionType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Types.EncryptionParametersType;
import main.java.MPEGG.Utilitats.BytesUtilities;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.Utilitats.Pair;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.DadesType;
import main.java.MPEGG.enume.WrapType;
import main.java.MPEGG.Data.Dataset;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.crypto.Data;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Classe Java per un DatasetGroup d'un fitxer MPEG-G.
 *
 * @author Marti Fernandez
 * @author Èric Monné Mesalles
 */
public class DatasetGroup {

    private String folderPath;
    private int dataset_group_ID;
    private int version_number;
    private ArrayList<Short> datasetId;
    private int numberOfDatasets;

//    private Pair<Integer, Dataset> datasets;
    /**
     * Box de protection del DatasetGroup.
     */
    protected DatasetGroupProtectionType DGProtection;
    /**
     * Box de Metadata del DatasetGroup.
     */
    protected DatasetGroupMetadataType DGMetadata;

//    @Override
//    public String toString() {
//
//            return DGMetadata.toString() + DGProtection.toString();
//    }

    /**
     * ArrayList amb els Datasets pertanyents al DatasetGroup.
     */
    protected ArrayList<Dataset> datasets;

    ///////////////////
    // CONSTRUCTORES //
    //////////////////

    /**
     * Nova instància de DatasetGroup amb valors buits.
     */
    protected DatasetGroup() {
        newDGMetadata();
        newDGProtection();
        this.datasets = new ArrayList<>();
    }

    /**
     * Crea nova instancia de DatasetGroup amb dades i les parseja.
     *
     * @param data Bytes valids a parsejar a la classe.
     * @throws Exception Error al parsejar els Bytes. Format no correcte.
     */
    protected DatasetGroup(byte[] data) throws Exception {
        this.DGProtection = null;
        this.DGMetadata = null;
        this.datasets = new ArrayList<>();
        parse(data);
    }

    protected DatasetGroup(String folderPath) throws Exception {
        this.folderPath = folderPath;
        parseDatasetGroupComponents();
        parseDatasets(folderPath);
    }

    private void parseDatasets(String folderPath) throws Exception {
        datasets = new ArrayList<>();
        File directoryPath = new File(folderPath);
        String[] directoris = directoryPath.list();
        if (directoris != null) {
            for (String s : directoris) {
                File dgCandidate = new File(folderPath + "/" + s);
                if(dgCandidate.isDirectory()){
                    if(s.contains("dt_")){
                        datasets.add(new Dataset(dgCandidate.toString()));
                    }
                }
            }
        }
    }

    private void parseDatasetGroupComponents() throws Exception {
        KLV headerKLV = KLV.parseFile(folderPath, BoxKey.DataGroupHeader);
        parseHeaderKLV(headerKLV);
        KLV metadataKLV = KLV.parseFile(folderPath, BoxKey.DataGroupMetada);
        parseMetadataKLV(metadataKLV);
        KLV protectionKLV = KLV.parseFile(folderPath, BoxKey.DataGroupProtection);
        parseprotectionKLV(protectionKLV);
    }
    private void parseprotectionKLV(KLV headerKLV){
        // TODO: PARSE PROTECTION KLV FROM DATASETGROUP
    }
    private void parseMetadataKLV(KLV headerKLV){
        // TODO: PARSE METADATA KLV FROM DATASETGROUP
    }

    private void parseHeaderKLV(KLV headerKLV){
        byte[] aux = headerKLV.getV();
        int bufferIndex = 0;
        dataset_group_ID = BytesUtilities.parseuint8(aux[bufferIndex]);
        bufferIndex += 1;
        version_number = BytesUtilities.parseuint8(aux[bufferIndex]);
        bufferIndex += 2;
        datasetId = new ArrayList<>();
        for ( int x = bufferIndex ; x < headerKLV.getLength() ; x += 2){
            datasetId.add(BytesUtilities.parserShort(aux, x));
        }
        numberOfDatasets = datasetId.size();
    }

    @Override
    public String toString(){
        String DatasetGroup = "Dataset Group: \n\tID: " + dataset_group_ID + "\n\tVersion Number:" + version_number + "\n";
        return DatasetGroup;
    }
    ///////////////
    // AUXILIARS //
    ///////////////

    /**
     * Parseja els bytes que es passen a l'objecte actual.
     *
     * @param aux Byte Array a parsejar.
     * @throws Exception Error al parsejar. Format no correcte.
     */
    protected void parse(byte[] aux) throws Exception {
        while (aux != null) {
            Pair<KLV, byte[]> a = BytesUtilities.getFirstKLV(aux);
            aux = a.getValue();
            switch (a.getKey().getK()) {
                case "dgpr":
                    try {
                        this.DGProtection = (DatasetGroupProtectionType) BytesUtilities.deserialitzar(a.getKey().getV());
                    } catch (IOException | ClassNotFoundException e) {
                        // Error in de-serialization or you are converting an invalid stream
                        e.printStackTrace();
                    }
                    break;
                case "dgmd":
                    try {
                        this.DGMetadata = (DatasetGroupMetadataType) BytesUtilities.deserialitzar(a.getKey().getV());
                    } catch (IOException | ClassNotFoundException e) {
                        // Error in de-serialization or you are converting an invalid stream
                        e.printStackTrace();
                    }
                    break;
                case "dtva":
                    this.addDataset(a.getKey().getV());
                    break;
                default:
                    throw new Exception("Fitxer incorrecte");
            }
        }
    }

    /**
     * Defineix un DatasetGroupProtectionType buit a DGProtection.
     */
    protected void newDGProtection() {
        this.DGProtection = new DatasetGroupProtectionType();
    }

    /**
     * Defineix un DatasetGroupMetadataType buit a DGMetadata.
     */
    protected void newDGMetadata() {
        this.DGMetadata = new DatasetGroupMetadataType();
    }

    /**
     * Defineix l'id que hi ha a la metadata de l'objecte.
     *
     * @param id Id a definir.
     * @throws Exception Falta DGMetadata.
     */
    protected void setMeta(Integer id) throws Exception {
        if (DGMetadata == null) throw new Exception("Falta DGMetadata");
        DGMetadata.setId(String.valueOf(id));
    }


    /**
     * Defineix un nou Dataset.
     *
     * @return ID del Dataset.
     * @throws Exception DTMetadata no exitent o esta encriptat.
     */
    public Integer addDataset() throws Exception {
        Dataset aux = new Dataset();
        this.datasets.add(aux);
        int id = this.datasets.indexOf(aux);
        aux.setMeta(id);
        return id;
    }

    /**
     * Defineix un nou Dataset amb els bytes parsejats.
     *
     * @param dades Dades a parsejar al dataset.
     * @throws Exception DTMetadata no exitent o esta encriptat / Error al parsejar el dataset.
     */
    protected void addDataset(byte[] dades) throws Exception {
        Dataset aux = new Dataset(dades);
        this.datasets.add(aux);
    }

    /**
     * Crea un nou AccesUnit i Block amb les dades que es pasen i retorna l'id.
     *
     * @param id    Id del Dataset. Si es -1 crea un nou Dataset.
     * @param dades Dades que contindra el block.
     * @return URI que apunta a l'accesunit/block des del nivell actual.
     * @throws Exception DTMetadata no exitent o esta encriptat /DT id no trobat.
     */
    public URI newBlock(URI id, byte[] dades) throws Exception {
        if (id.getRef() == null) throw new Exception("S'ha d'haver definit un Reference");
        Dataset aux;
        if (id.getDt_id() == -1) {
            id.setDt_id(this.addDataset());
        } else if (this.datasets.size() <= id.getDt_id()) throw new Exception("Error id DT");
        aux = this.datasets.get(id.getDt_id());
        return aux.newBlock(id, dades);
    }


    /////////////
    // CRYPTO //
    /////////////

    /**
     * Encrypta el DTProtection del Dataset amb id del parametre que es pasa.
     *
     * @param uri URI del dataset que s'encriptara amb Metadata si es vol fer les metadates o Protection si es vol encriptar el protection.
     * @throws Exception DGProtection inexistent o encriptat / O URI incorrecte / Error al iniciar el cipher amb EP.
     */
    protected void EncryptDT(URI uri) throws Exception {
        if (DGProtection == null) throw new Exception("Error de permisos");
        Dataset d = this.datasets.get(uri.getDt_id());
        if (uri.getTipus().equals(DadesType.DTProtection)) {
            DatasetProtectionType dtpr = d.DTProtection;
            byte[] res = this.DGProtection.encryptDT(uri, dtpr);
            d.setDTProtectionEncrypted(res);
        } else if (uri.getTipus().equals(DadesType.DTMetadata)) {
            DatasetMetadataType dtmd = d.DTMetadata;
            byte[] res = this.DGProtection.encryptDT(uri, dtmd);
            d.setDTMetadataEncrypted(res);
        } else throw new Exception("URI incorrecte");
    }

    /**
     * Encrypta l'AUProtection i Metadata de l'accesunit amb id dels parametres que es pasen.
     *
     * @param uri URI de les boxes a encriptar.
     * @throws Exception DTProtection inexistent o encriptat / URI incorrecte / Error al iniciar el cipher per desencriptar.
     */
    protected void EncryptAU(URI uri) throws Exception {
        this.datasets.get(uri.getDt_id()).EncryptAU(uri);
    }

    /**
     * Encripta el block que apunta l'URI que es pasa com a parametre.
     *
     * @param uri URI que apunta al block.
     * @throws Exception AUProtection inexistent o encryptat / Error al iniciar el cipher per encriptar.
     */
    protected void EncryptBlock(URI uri) throws Exception {
        this.datasets.get(uri.getDt_id()).EncryptBlock(uri);
    }

    /**
     * Encripta totes les dades encriptables dels nivells inferiors (block, accesunit boxes i dataset boxes).
     *
     * @throws Exception AUProtection inexistent o encryptat / DTProtection inexistent o encriptat / DGProtection inexistent / Error al iniciar el cipher per encriptar.
     */
    protected void encryptAll() throws Exception {
        for (Dataset dtt : this.datasets) {
            dtt.encryptAll();
            URI a = new URI();
            a.apuntarDatasetMd(0, this.datasets.indexOf(dtt));
            URI b = new URI();
            b.apuntarDatasetPr(0, this.datasets.indexOf(dtt));
            EncryptDT(a);
            EncryptDT(b);
        }
    }

    /**
     * Defineix l'EncryptionParametersType que es pasa a DGProtection.
     *
     * @param dgpt EncryptionParametersType a definir.
     * @throws Exception Ja existeix un EP amb aquest URI.
     */
    protected void setDTEP(EncryptionParametersType dgpt) throws Exception {
        this.DGProtection.setEP(dgpt);
    }

    /**
     * Desencripta totes les dades encriptables dels nivells inferiors (block, accesunit boxes i datasetboxes).
     *
     * @throws Exception Error al desencriptar -> Box PR o MD d'AccesUnit o Dataset no existeix / AUProtection o DTProtection inexistent / Error al iniciar el cipher per desencriptar.
     */
    protected void searchAndDecryptEncryptedLocations() throws Exception {
        for (Dataset d : this.datasets) {
            if (d.isEncryptedBoxP()) {
                URI b = new URI();
                b.apuntarDatasetPr(0, this.datasets.indexOf(d));
                DecryptDT(b);

            }
            if (d.isEncryptedBoxMd()) {
                URI a = new URI();
                a.apuntarDatasetMd(0, this.datasets.indexOf(d));
                DecryptDT(a);
            }
            d.searchAndDecryptEncryptedLocations();
        }
    }

    public void setNewDGEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode, KeyTransportType Eliptic2) throws Exception {
        this.DGProtection.setNewEP(cipher, wrapper, id, wrapmode, Eliptic2);
    }

    public void setNewDGEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode) throws Exception {
        this.DGProtection.setNewEP(cipher, wrapper, id, wrapmode);
    }

    /**
     * Modifica l'EncryptionParametersType que tingui el mateix id que EncryptionParametersType que es passa.
     *
     * @param dgpt EncryptionParametersType que substituira l'anterior amb mateix id.
     */
    protected void changeDTEP(EncryptionParametersType dgpt) {
        this.DGProtection.changeEP(dgpt);
    }

    /**
     * Elimina l'EncryptionParametersType de DGProtection que tingui l'id que es pasa com a parametre.
     *
     * @param id EncriptedLocation del EncryptionParametersType a eliminar. Nomes es te en compte el tipus i dt_id
     */
    protected void deleteAUEP(URI id) {
        this.DGProtection.deleteEP(id);
    }

    /**
     * Desencripta les boxes del dataser que s'especifica.
     *
     * @param uri URI del Dataset a desencriptar les boxes. Amb Metadata o Protection.
     * @throws Exception Error al desencriptar -> Error al iniciar el cipher per desencriptar / URI incorrecte (no es cap Box).
     */
    protected void DecryptDT(URI uri) throws Exception {
        Dataset d = this.datasets.get(uri.getDt_id());
        if (uri.getTipus().equals(DadesType.DTProtection)) {
            byte[] dtpr = d.getDTProtEncry();
            DatasetProtectionType res = (DatasetProtectionType) this.DGProtection.decryptDT(uri, dtpr);
            d.setDTProtection(res);
        } else if (uri.getTipus().equals(DadesType.DTMetadata)) {
            byte[] dtmd = d.getDTMetEncry();
            DatasetMetadataType res = (DatasetMetadataType) this.DGProtection.decryptDT(uri, dtmd);
            d.setDTMetadata(res);
        } else throw new Exception("URI incorrecte");
    }

    //////////
    // GETS //
    //////////

    /**
     * Obte els bytes de l'objecte amb les dades actuals de la classe.
     *
     * @return Byte Array de protection + metadata + datasets.
     * @throws Exception Falta alguna box.
     */
    protected byte[] getBytes() throws Exception {
        byte[] aux = BytesUtilities.addArrays(this.DGProtection.getKLV().toBytes(), this.DGMetadata.getKLV().toBytes());
        return BytesUtilities.addArrays(aux, getDatasetsBytes());
    }

    /**
     * Obte els bytes concadenats dels objectes dataset amb el seu KLV.
     *
     * @return Byte Array amb els valors.
     * @throws Exception Algun Dataset o Accesunit no esta complert.
     */
    protected byte[] getDatasetsBytes() throws Exception {
        byte[] ret = new byte[0];
        for (Dataset dataset : this.datasets) {
            ret = BytesUtilities.addArrays(ret, dataset.getKLV().toBytes());
        }
        return ret;
    }

    /**
     * Obte el KLV de l'objecte actual.
     *
     * @return KLV de l'objecte
     * @throws Exception Falta alguna box.
     */
    protected KLV getKLV() throws Exception {
        return new KLV("dgva", getBytes());
    }

    /**
     * Obte els ids dels Dataset.
     *
     * @return ArrayList amb els ids dels dataset de l'objecte.
     */
    public ArrayList<Integer> getDTIds() {
        int fin = this.datasets.size();
        ArrayList<Integer> ret = new ArrayList<>();
        for (int i = 0; i < fin; ++i) {
            ret.add(i);
        }
        return ret;
    }

    /**
     * Obte el Dataset amb l'id que es pasa com a parametre.
     *
     * @param id ID del Dataset que es vol aconseguir.
     * @return Dataset amb id "id".
     * @throws Exception Id no exitent.
     */
    public Dataset getDataset(Integer id) throws Exception {
        if (id > this.datasets.size() || id < 0) throw new Exception("Id erroni");
        return this.datasets.get(id);
    }
}
