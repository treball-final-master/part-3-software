package main.java.MPEGG.Data;

import MPEGG.enume.BoxKey;
import MPEGG.interfaces.MPEG_Component;

import java.io.File;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;

import main.java.MPEGG.Utilitats.KLV;
import org.apache.http.util.ByteArrayBuffer;

/**
 * Classe Java per un grup de blocks d'un fitxer MPEG-G.
 *
 * @author Marti Fernandez
 */
public class Block implements Serializable, MPEG_Component {
    /**
     * Dades del block.
     */
    protected byte[] dades;

    private byte firstReserved;
    private byte[] descriptorId;
    private byte[] secondReserved;
    private int payloadSize;

    public String folderPath;
    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Nova instancia de Block amb valors buits.
     */
    public Block() {
        this.dades = null;
    }

    /**
     * Crea un nou block amb els bytes especificats i sense encriptar.
     *
     * @param b Bytes a tenir el block.
     */
    protected Block(byte[] b) {
        this.dades = b;
    }


    public Block(String folderPath) throws Exception{
        this.folderPath = folderPath;
        parseBlockHeader(folderPath);
        parseBlockData(folderPath + '/' + BoxKey.BlockPayload);
    }

    private void parseBlockHeader(String folderPath) throws Exception {
//        System.out.println(folderPath);
//        KLV headerKLV = KLV.parseFile(folderPath, BoxKey.BlockHeader);
//        parseHeaderKLV(headerKLV);
    }

    private void parseHeaderKLV(KLV headerKLV){
        byte[] aux = headerKLV.getV();
        int bufferIndex = 0;
        firstReserved = aux[bufferIndex];
        bufferIndex += 1;
        descriptorId = Arrays.copyOfRange(aux, bufferIndex, bufferIndex + 7);
        bufferIndex += 7;
        secondReserved = Arrays.copyOfRange(aux, bufferIndex, bufferIndex + 3);
        bufferIndex += 3;
        byte[] tempL = Arrays.copyOfRange(aux, bufferIndex, aux.length);
        ByteBuffer wrapped = ByteBuffer.wrap(tempL);
        System.out.println(wrapped.getInt());
        // TODO: Parse Block payload Size
    }

    private void parseBlockData(String filePath) throws Exception{
        File blockData = new File(filePath);
        dades = Files.readAllBytes(blockData.toPath());
    }

    //////////
    // GETS //
    //////////

    /**
     * Obte les dades del block.
     *
     * @return ByteArray dels bytes del block.
     */
    public byte[] getBytes() {
        return this.dades;
    }

    //////////
    // SETS //
    //////////

    /**
     * Modifica les dades del block.
     *
     * @param d ByteArray que ha de contenir el Block.
     */
    public void setBytes(byte[] d) {
        this.dades = d;
    }

    @Override
    public void writeData() {

    }
}
