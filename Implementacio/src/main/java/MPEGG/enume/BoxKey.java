package MPEGG.enume;

public enum BoxKey {
    FileHeader {
        @Override
        public String toString() {
            return "flhd";
        }
    },
    DataGroupHeader {
        @Override
        public String toString() {
            return "dghd";
        }
    },
    DataGroupMetada {
        @Override
        public String toString() {
            return "dgmd";
        }
    },
    DataGroupProtection {
        @Override
        public String toString() {
            return "dgpr";
        }
    },
    DataSetHeader {
        @Override
        public String toString() {
            return "dthd";
        }
    },
    DataSetMetadata {
        @Override
        public String toString() {
            return "dtmd";
        }
    },
    DataSetProtection {
        @Override
        public String toString() {
            return "dtpr";
        }
    },
    AccessUnitHeader {
        @Override
        public String toString() {
            return "auhd";
        }
    },
    AccessUnitInformation {
        @Override
        public String toString() {
            return "auin";
        }
    },
    AccessUnitProtecction {
        @Override
        public String toString() {
            return "aupr";
        }
    },
    BlockHeader {
        @Override
        public String toString() {
            return "block_hd";
        }
    },
    BlockPayload {
        @Override
        public String toString() {
            return "block_payload";
        }
    }
}