package main.java.MPEGG.enume;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CipherURIType.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * <p>
 * <pre>
 * &lt;simpleType name="CipherURIType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="aes128-ctr"/>
 *     &lt;enumeration value="aes192-ctr"/>
 *     &lt;enumeration value="aes256-ctr"/>
 *     &lt;enumeration value="aes128-gcm"/>
 *     &lt;enumeration value="aes192-gcm"/>
 *     &lt;enumeration value="aes256-gcm"/>
 *     &lt;enumeration value="chacha20-poly1305"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "CipherURIType", namespace = "TFG:MPEG-G:KeyTrasport")
@XmlEnum
public enum CipherURIType {

    @XmlEnumValue("aes128-ctr")
    AES_128_CTR("AES/CTR/NoPadding"),
    @XmlEnumValue("aes192-ctr")
    AES_192_CTR("AES/CTR/NoPadding"),
    @XmlEnumValue("aes256-ctr")
    AES_256_CTR("AES/CTR/NoPadding"),
    @XmlEnumValue("aes128-gcm")
    AES_128_GCM("AES/GCM/NoPadding"),
    @XmlEnumValue("aes192-gcm")
    AES_192_GCM("AES/GCM/NoPadding"),
    @XmlEnumValue("aes256-gcm")
    AES_256_GCM("AES/GCM/NoPadding");
    private final String value;

    /**
     * Nova instancia de l'enumeració amb el valor que es passa.
     *
     * @param v Valor de l'enumeració.
     */
    CipherURIType(String v) {
        value = v;
    }

    /**
     * Retorna un CipherURIType a partir d'un valor.
     *
     * @param v Valor a convertir.
     * @return Cipher URI del valor que s'ha passat.
     */
    protected static CipherURIType fromValue(String v) {
        for (CipherURIType c : CipherURIType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

    /**
     * Retorna el valor en format String.
     *
     * @return String del valor del CipherURIType.
     */
    public String value() {
        return value;
    }

}
