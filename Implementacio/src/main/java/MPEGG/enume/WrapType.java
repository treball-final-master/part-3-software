package main.java.MPEGG.enume;

public enum WrapType {
    Simetric,
    Asimetric,
    EC,
    Derivation
}
