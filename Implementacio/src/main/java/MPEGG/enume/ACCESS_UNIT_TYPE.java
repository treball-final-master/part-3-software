package MPEGG.enume;

public enum ACCESS_UNIT_TYPE {
    P_TYPE_AU,
    N_TYPE_AU,
    M_TYPE_AU,
    I_TYPE_AU,
    HM_TYPE_AU,
    U_TYPE_AU
}

