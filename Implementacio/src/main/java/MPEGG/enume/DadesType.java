package main.java.MPEGG.enume;

public enum DadesType {
    DTMetadata,
    DTProtection,
    DGMetadata,
    DGProtection,
    AUMetadata,
    AUProtection,
    Dades
}
