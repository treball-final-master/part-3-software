package main.java.MPEGG.Boxes;

import main.java.MPEGG.Utilitats.KLV;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;

import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;


/**
 * <p>Clase Java para DatasetGroupMetadataType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DatasetGroupMetadataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatasetGroupMetadataType", namespace = "TFG:MPEG-G:DatasetGroupMetadata")
@XmlRootElement(name = "DatasetGroupMetadata")
public class DatasetGroupMetadataType implements Serializable {

    @XmlAttribute(name = "id", namespace = "TFG:MPEG-G:DatasetGroupMetadata")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;


    @XmlElement(name = "Title", namespace = "TFG:MPEG-G:DatasetGroupMetadata")
    protected String title;

    @XmlElement(name = "Description", namespace = "TFG:MPEG-G:DatasetGroupMetadata")
    protected String description;
    //////////
    // GETS //
    //////////

    /**
     * Obté el KLV de l'objecte amb els Bytes d'aquest.
     *
     * @return KLV de l'objecte.
     */
    public KLV getKLV() throws IOException {
        return new KLV("dgmd", serialitzar(this));
    }

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }


    /**
     * Obtiene el valor de la propiedad Title.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getTitle() {
        return title;
    }

    /**
     * Define el valor de la propiedad Title.
     *
     * @param title allowed object is
     * {@link String }
     */
    public void setTitle(String title){
        this.title = title;
    }

    /**
     * Obtiene el valor de la propiedad Description.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad Description.
     *
     * @param description allowed object is
     * {@link String }
     */
    public void setDescription(String description){
        this.description = description;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return Signature de l'bjecte d'aquesta classe.
     */
    @Override
    public int hashCode() {
        return id.hashCode();
    }


    @Override
    public String toString(){
        try {
            JAXBContext context = JAXBContext.newInstance(DatasetGroupMetadataType.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            StringWriter sw = new StringWriter();
            marshaller.marshal(this,sw);
            String result = sw.toString();
            return result;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return "";
    }
}
