package main.java.MPEGG.Boxes;

import main.java.MPEGG.Utilitats.KLV;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.IOException;
import java.io.Serializable;

import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;

/**
 * <p>Clase Java para DatasetMetadataType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DatasetMetadataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="id" type="{http://www.w3.org/2001/XMLSchema}ID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatasetMetadataType", namespace = "TFG:MPEG-G:DatasetMetadata")
public class DatasetMetadataType implements Serializable {

    @XmlAttribute(name = "id", namespace = "TFG:MPEG-G:DatasetMetadata")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlID
    @XmlSchemaType(name = "ID")
    protected String id;

    /**
     * Obté el KLV de l'objecte amb els Bytes d'aquest.
     *
     * @return KLV de l'objecte.
     */
    public KLV getKLV() throws IOException {
        return new KLV("dtmd", serialitzar(this));
    }

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is
     * {@link String }
     */
    protected String getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setId(String value) {
        this.id = value;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return Signature de l'bjecte d'aquesta classe.
     */
    @Override
    public int hashCode() {
        if (id == null) return 0;
        return id.hashCode();
    }

}
