package main.java.MPEGG.Boxes;

import main.java.MPEGG.Utilitats.KLV;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.IOException;
import java.io.Serializable;

import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;


/**
 * <p>Clase Java para ReferenceType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="ReferenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="chromosome_name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="position" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReferenceType", namespace = "TFG:MPEG-G:referenceMetadata", propOrder = {
        "chromosomeName",
        "position"
})
public class ReferenceType implements Serializable {

    @XmlElement(name = "chromosome_name", namespace = "TFG:MPEG-G:referenceMetadata", required = true)
    protected String chromosomeName;
    @XmlElement(namespace = "TFG:MPEG-G:referenceMetadata")
    protected ReferenceType.Position position;

    public void copiRef(ReferenceType r) {
        this.setChromosomeName(r.getChromosomeName());
        ReferenceType.Position p = new ReferenceType.Position();
        p.setEnd(r.getPosition().getEnd());
        p.setStart(r.getPosition().getStart());
        this.setPosition(p);
    }

    /**
     * Obté el KLV de l'objecte amb els Bytes d'aquest.
     *
     * @return KLV de l'objecte.
     */
    public KLV getKLV() throws IOException {
        return new KLV("rfmd", serialitzar(this));
    }

    /**
     * Obtiene el valor de la propiedad chromosomeName.
     *
     * @return possible object is
     * {@link String }
     */
    public String getChromosomeName() {
        return chromosomeName;
    }

    /**
     * Define el valor de la propiedad chromosomeName.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setChromosomeName(String value) {
        this.chromosomeName = value;
    }

    /**
     * Obtiene el valor de la propiedad position.
     *
     * @return possible object is
     * {@link ReferenceType.Position }
     */
    public ReferenceType.Position getPosition() {
        if (position == null) {
            position = new Position();
        }
        return position;
    }

    /**
     * Define el valor de la propiedad position.
     *
     * @param value allowed object is
     *              {@link ReferenceType.Position }
     */
    public void setPosition(ReferenceType.Position value) {
        this.position = value;
    }

    @Override
    public int hashCode() {
        int result;
        if (chromosomeName == null) result = 0;
        else result = chromosomeName.hashCode();
        result += getPosition().getStart();
        result += getPosition().getEnd();
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        // If the object is compared with itself then return true
        if (obj == this) {
            return true;
        }

        /* Check if o is an instance of Complex or not
          "null instanceof [type]" also returns false */
        if (!(obj instanceof ReferenceType)) {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        ReferenceType c = (ReferenceType) obj;

        // Compare the data members and return accordingly
        if(!(this.chromosomeName.equals((c.chromosomeName)))) return false;
        if(this.position.start != c.position.start) return false;
        return this.position.end == c.position.end;
    }

    /**
     * <p>Clase Java para anonymous complex type.
     *
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="start" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="end" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "start",
            "end"
    })
    public static class Position implements Serializable {

        @XmlElement(namespace = "TFG:MPEG-G:referenceMetadata")
        protected long start;
        @XmlElement(namespace = "TFG:MPEG-G:referenceMetadata")
        protected long end;

        /**
         * Obtiene el valor de la propiedad start.
         */
        public long getStart() {
            return start;
        }

        /**
         * Define el valor de la propiedad start.
         */
        public void setStart(long value) {
            this.start = value;
        }

        /**
         * Obtiene el valor de la propiedad end.
         */
        public long getEnd() {
            return end;
        }

        /**
         * Define el valor de la propiedad end.
         */
        public void setEnd(long value) {
            this.end = value;
        }

    }

}
