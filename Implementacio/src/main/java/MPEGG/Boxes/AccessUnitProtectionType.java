package main.java.MPEGG.Boxes;

import main.java.MPEGG.Types.AccessUnitEncryptionParametersType;
import main.java.MPEGG.Types.SignatureType;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.enume.CipherURIType;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.ChaCha20ParameterSpec;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.*;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;


/**
 * <p>Clase Java para AccessUnitProtectionType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="AccessUnitProtectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AccessUnitEncryptionParameters" type="{TFG:MPEG-G:AUEncryptedParameters}AccessUnitEncryptionParametersType" minOccurs="0"/>
 *         &lt;element name="SignatureParameters" type="{http://www.w3.org/2000/09/xmldsig#}SignatureType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="", propOrder = {
        "accessUnitEncryptionParameters",
        "signatureParameters"
})
@XmlRootElement(namespace = "MPEGG.Data.AccesUnit")
public class AccessUnitProtectionType implements Serializable {

    @XmlElement(name = "AccessUnitEncryptionParameters", namespace = "TFG:MPEG-G:AccessUnitProtection")
    protected AccessUnitEncryptionParametersType accessUnitEncryptionParameters;
    @XmlElement(name = "SignatureParameters", namespace = "TFG:MPEG-G:AccessUnitProtection")
    protected int signatureParameters;

    //////////
    // GETS //
    //////////

    /**
     * Obté el KLV de l'objecte amb els Bytes d'aquest.
     *
     * @return KLV de l'objecte.
     */
    public KLV getKLV() throws IOException, JAXBException {
        return new KLV("aupr", serialitzar(this));
    }

    /**
     * Obtiene el valor de la propiedad accessUnitEncryptionParameters.
     *
     * @return possible object is
     * {@link AccessUnitEncryptionParametersType }
     */
    public AccessUnitEncryptionParametersType getAccessUnitEncryptionParameters() {
        if (accessUnitEncryptionParameters == null)
            accessUnitEncryptionParameters = new AccessUnitEncryptionParametersType();
        return accessUnitEncryptionParameters;
    }

    /**
     * Gets the value of the signatureParameters property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the signatureParameters property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSignatureParameters().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SignatureType }
     */
    protected int getSignatureParameters() {
        return this.signatureParameters;
    }

    //////////////////
    // MODIFICADORS //
    //////////////////

    /**
     * Defineix l'AccessUnitEncryptionParametersType de l'objecte.
     *
     * @param epta AccessUnitEncryptionParametersType que passara a ser el accessUnitEncryptionParameters de l'objecte.
     */
    public void setEP(AccessUnitEncryptionParametersType epta) {
        this.accessUnitEncryptionParameters = epta;
    }

    /**
     * Elimina l'AccessUnitEncryptionParametersType.
     */
    protected void delEP() {
        this.accessUnitEncryptionParameters = null;
    }


    ///////////
    // CRYPT //
    ///////////

    /**
     * Obte la clau de AccessUnitEncryptionParameters.
     *
     * @return Key del block.
     */
    protected Key getKey() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException {
        if (accessUnitEncryptionParameters == null) {
            accessUnitEncryptionParameters = new AccessUnitEncryptionParametersType();
        }
        return this.accessUnitEncryptionParameters.getWrappedKey().getValue();
    }

    /**
     * Obté els Bytes aplicant el cipher a les dades que es passen.
     *
     * @param dades  Dades que se'ls hi ha d'aplicar el cipher.
     * @param cipher Cipher a aplicar a les dades.
     * @return Dades despres d'haver aplicat el cipher.
     */
    protected byte[] readObject(byte[] dades, Cipher cipher) throws IOException, BadPaddingException, IllegalBlockSizeException {
        return cipher.doFinal(dades);
    }

    /**
     * Inicialitza el cipher per desencriptar.
     *
     * @return Cipher a aplicar per desencriptar.
     */
    protected Cipher initCipherDecrypt() throws InvalidAlgorithmParameterException, InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException {
        Key sks = getKey();

        Cipher cipher = Cipher.getInstance(accessUnitEncryptionParameters.getCipher().value());
        if (accessUnitEncryptionParameters.getCipher().equals(CipherURIType.CHACHA_20_POLY_1305)) {
            cipher.init(Cipher.DECRYPT_MODE, sks, new ChaCha20ParameterSpec(accessUnitEncryptionParameters.getAublockNounce(), 1));
        } else {
            if (accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_256_GCM || accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_128_GCM || accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_192_GCM) {
                cipher.init(Cipher.DECRYPT_MODE, sks, new GCMParameterSpec(128, accessUnitEncryptionParameters.getAublockIV()));
                byte[] tag = new byte[128];
                cipher.updateAAD(tag);
            } else {
                cipher.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(accessUnitEncryptionParameters.getAublockIV()));
            }
        }
        return cipher;
    }

    /**
     * Inicialitza el cipher per encriptar.
     *
     * @return Cipher a aplicar per encriptar.
     */
    protected Cipher initCipherEncrypt() throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException {
        Key sks = getKey();

        Cipher cipher = Cipher.getInstance(accessUnitEncryptionParameters.getCipher().value());
        if (accessUnitEncryptionParameters.getCipher().equals(CipherURIType.CHACHA_20_POLY_1305)) {
            cipher.init(Cipher.ENCRYPT_MODE, sks, new ChaCha20ParameterSpec(accessUnitEncryptionParameters.getAublockNounce(), 1));
        } else {
            if (accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_256_GCM || accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_128_GCM || accessUnitEncryptionParameters.getCipher() == CipherURIType.AES_192_GCM) {
                cipher.init(Cipher.ENCRYPT_MODE, sks, new GCMParameterSpec(128, accessUnitEncryptionParameters.getAublockIV()));
                byte[] tag = new byte[128];
                cipher.updateAAD(tag);
            } else {
                cipher.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(accessUnitEncryptionParameters.getAublockIV()));
            }
            ////// NOMES ES FA AMB IV JA QUE NO S'IMPLEMENTA DSC NI TAMPOC INFORMATION
        }

        return cipher;
    }

    /**
     * Encripta els bytes que es passen.
     *
     * @param block Dades del block a encriptar.
     * @return Dades del block encriptat.
     */
    public byte[] encryptBlock(byte[] block) throws InvalidAlgorithmParameterException, NoSuchAlgorithmException, InvalidKeyException, NoSuchPaddingException, IOException, BadPaddingException, IllegalBlockSizeException {
        signatureParameters = Arrays.hashCode(block);
        return readObject(block, initCipherEncrypt());
    }

    /**
     * Desencripta els bytes que es passen.
     *
     * @param block Dades del block a desencriptar.
     * @return Dades del block desencriptat.
     */
    public byte[] decryptBlock(byte[] block) throws Exception {
        byte[] aux = readObject(block, initCipherDecrypt());
        if (signatureParameters != Arrays.hashCode(aux)) throw new Exception("Signatura incorrecte");
        return aux;
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return Signature de l'bjecte d'aquesta classe.
     */
    @Override
    public int hashCode() {
        if (accessUnitEncryptionParameters == null) return 0;
        return accessUnitEncryptionParameters.hashCode();
    }
}
