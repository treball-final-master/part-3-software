package main.java.MPEGG.Boxes;

import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Types.EncryptionParametersType;
import main.java.MPEGG.Types.PolicyType;
import main.java.MPEGG.Types.SignatureType;
import main.java.MPEGG.Utilitats.KLV;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.DadesType;
import main.java.MPEGG.enume.WrapType;

import javax.crypto.Cipher;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.IOException;
import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;
import static main.java.MPEGG.Utilitats.CryptoForms.*;


/**
 * <p>Clase Java para DatasetProtectionType complex type.
 *
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="DatasetProtectionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="KeyTransportAES" type="{TFG:MPEG-G:KeyTrasport}KeyTransportType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="EncryptionParameters" type="{TFG:MPEG-G:EncryptedParameters}EncryptionParametersType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SignatureParameters" type="{http://www.w3.org/2000/09/xmldsig#}SignatureType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{urn:oasis:names:tc:xacml:1.0:policy}Policy" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatasetProtectionType", namespace = "TFG:MPEG-G:DatasetProtection", propOrder = {
        "keyTransportAES",
        "encryptionParameters",
        "signatureParameters",
        "policy"
})
public class DatasetProtectionType implements Serializable {

    @XmlElement(name = "KeyTransportAES")
    protected List<KeyTransportType> keyTransportAES;
    @XmlElement(name = "EncryptionParameters")
    protected List<EncryptionParametersType> encryptionParameters;
    @XmlElement(name = "SignatureParameters")
    protected List<SignatureType> signatureParameters;
    @XmlElement(name = "Policy", namespace = "urn:oasis:names:tc:xacml:1.0:policy")
    protected PolicyType policy;


    //////////
    // GETS //
    //////////

    /**
     * Obté el KLV de l'objecte amb els Bytes d'aquest.
     *
     * @return KLV de l'objecte.
     */
    public KLV getKLV() throws IOException {
        return new KLV("dtpr", serialitzar(this));
    }

    /**
     * Gets the value of the keyTransportAES property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyTransportAES property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyTransportAES().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyTransportType }
     */
    protected List<KeyTransportType> getKeyTransportAES() {
        if (keyTransportAES == null) {
            keyTransportAES = new ArrayList<>();
        }
        return this.keyTransportAES;
    }

    /**
     * Gets the value of the encryptionParameters property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the encryptionParameters property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEncryptionParameters().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EncryptionParametersType }
     */
    public List<EncryptionParametersType> getEncryptionParameters() {
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        return this.encryptionParameters;
    }

    /**
     * Gets the value of the signatureParameters property.
     *
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the signatureParameters property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSignatureParameters().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SignatureType }
     */
    protected List<SignatureType> getSignatureParameters() {
        if (signatureParameters == null) {
            signatureParameters = new ArrayList<>();
        }
        return this.signatureParameters;
    }

    /**
     * Obtiene el valor de la propiedad policy.
     *
     * @return possible object is
     * {@link PolicyType }
     */
    protected PolicyType getPolicy() {
        return policy;
    }

    //////////////////
    // MODIFICADORS //
    //////////////////

    /**
     * Define el valor de la propiedad policy.
     *
     * @param value allowed object is
     *              {@link PolicyType }
     */
    protected void setPolicy(PolicyType value) {
        this.policy = value;
    }

    /**
     * Afegeix un nou KeyTransport utilitzat per desencriptar el nicvell inferior i wrapejat o no amb una clau de la keylist.
     *
     * @param kt KeyTransportType amb la clau per CipherURIType.
     */
    public void addKeyAES(KeyTransportType kt) {
        if (keyTransportAES == null) {
            keyTransportAES = new ArrayList<>();
        }
        keyTransportAES.add(kt);
    }

    /**
     * Defineix un nou EP. Si no hagues un amb location igual l'eliminaria.
     *
     * @param epta Nou EP a afegir.
     * @throws Exception Ja existeix un EP amb aquest URI
     */
    public void setEP(EncryptionParametersType epta) throws Exception {
        URI idd = epta.getEncryptedLocations();
        for (EncryptionParametersType e : encryptionParameters) {
            if (e.getEncryptedLocations().getRef().equals(idd.getRef()) && e.getEncryptedLocations().getTipus().equals(idd.getTipus()))
                throw new Exception("Ja existeix un EP amb aquest URI");
        }
        this.encryptionParameters.add(epta);
    }

    /**
     * Actualitza l'EP amb valors randoms generats correctament i guarda el keytransport, que s'unwrapeja amb el wrapper, a KeyTransportAES. NOMES UTILITZAR QUAN LA LOCALITZACIO ESTIGUI DESENCRIPTADA.
     *
     * @param cipher   Cipher a utilitzar.
     * @param wrapper  KeyTransport que protegeix la clau.
     * @param id       Id del box a encryptar.
     * @param Eliptic2 Null si wrapmode != EC, sino la 2a clau a utilitzar.
     * @param wrapmode Mode de wrap de la Key.
     * @throws Exception URI incorrecte, no apunta a un DT.
     */
    public void setNewEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode, KeyTransportType Eliptic2) throws Exception {
        if (keyTransportAES == null) {
            keyTransportAES = new ArrayList<>();
        }
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        if (id.getTipus().equals(DadesType.AUProtection) || id.getTipus().equals(DadesType.AUMetadata)) {
            EncryptionParametersType ept = new EncryptionParametersType();
            if (wrapmode == WrapType.EC) {
                keyTransportAES.add(ept.setNewForEPValues(cipher, wrapper, id, wrapmode, Eliptic2));
            } else keyTransportAES.add(ept.setNewForEPValues(cipher, wrapper, id, wrapmode, null));
            encryptionParameters.add(ept);
        } else {
            throw new Exception("Uri incorrecte");
        }
    }

    /**
     * Actualitza l'EP amb valors randoms generats correctament i guarda el keytransport, que s'unwrapeja amb el wrapper, a KeyTransportAES. NOMES UTILITZAR QUAN LA LOCALITZACIO ESTIGUI DESENCRIPTADA.
     *
     * @param cipher   Cipher a utilitzar.
     * @param wrapper  KeyTransport que protegeix la clau.
     * @param id       Id del box a encryptar.
     * @param wrapmode Mode de wrap de la Key.
     * @throws Exception URI incorrecte, no apunta a un DT. O falta la segona clau del mode EC.
     */
    public void setNewEP(CipherURIType cipher, KeyTransportType wrapper, URI id, WrapType wrapmode) throws Exception {
        if (keyTransportAES == null) {
            keyTransportAES = new ArrayList<>();
        }
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        if (id.getTipus().equals(DadesType.AUProtection) || id.getTipus().equals(DadesType.AUMetadata)) {
            EncryptionParametersType ept = new EncryptionParametersType();
            if (wrapmode == WrapType.EC) {
                throw new Exception("Falta un parametre per utilitzar mode EC");
            } else keyTransportAES.add(ept.setNewForEPValues(cipher, wrapper, id, wrapmode, null));
            encryptionParameters.add(ept);
        } else {
            throw new Exception("Uri incorrecte");
        }
    }


    /**
     * Modifica l'EP amb id a la localització igual a l'EncryptionParametersType que es passa com a parametre.
     *
     * @param dgpt EncryptionParametersType que substituira l'anterior.
     */
    public void changeEP(EncryptionParametersType dgpt) {
        URI idd = dgpt.getEncryptedLocations();
        deleteEP(idd);
        this.encryptionParameters.add(dgpt);
    }

    /**
     * Elimina l'EP de l'id que es passa.
     *
     * @param id Id de la localitzacio de l'EP a eliminar.
     */
    public void deleteEP(URI id) {
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        encryptionParameters.removeIf(ep -> ep.getEncryptedLocations().getRef().equals(id.getRef()) && ep.getEncryptedLocations().getTipus().equals(id.getTipus()));
    }

    ///////////
    // CRYPT //
    ///////////

    /**
     * Obté l'EP de l'id que es passa.
     *
     * @param id ID a obtenir l'EP.
     * @return EncryptionParametersType de l'ID que s'ha passat com a parametre.
     * @throws Exception Error al obtenir l'EP -> No existeix cap EP amb la localitzacio d'id que s'ha passat.
     */
    protected EncryptionParametersType getEP(URI id) throws Exception {
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        for (EncryptionParametersType epp : encryptionParameters) {
            if (epp.getEncryptedLocations().getRef().equals(id.getRef()) && epp.getEncryptedLocations().getTipus().equals(id.getTipus())) {
                return epp;
            }
        }
        throw new Exception("No hi ha la localitzacio");
    }

    /**
     * Obté la clau amb el nom especificat.
     *
     * @param KeyName     Nom de la clau a buscar.
     * @param KeyAlgortim Algoritme de la clau que es vol obtenir. Vàlids: AES, RSA, ChaCha20 i X25519.
     * @param KeyType     Tipus de clau que es vol obtenir.
     * @return Key equivalent al nom que es passa.
     * @throws Exception Error al obtenir la clau -> Falta alguna clau per obtenir la key que es demana.
     */
    protected Key getKey(String KeyName, String KeyAlgortim, int KeyType) throws Exception {
        for (KeyTransportType kp : keyTransportAES) {
            if (kp.getKeyName().equals(KeyName)) {
                return kp.getKey(KeyAlgortim, KeyType);
            }
        }
        throw new Exception("No hi ha la clau");
    }

    /**
     * Obte l'AccessUnitProtectionType o ReferenceType, segons uri, amb format Object despres de desencriptar els bytes equivalents encriptats i amb l'id que s'especifica.
     *
     * @param id    Id de la box encriptada.
     * @param aubox Bytes del aupr o rfmd encriptat.
     * @return AccessUnitProtectionType desencriptat.
     * @throws Exception Error al iniciar el cipher -> No existeix cap EP / No hi ha la clau o no es pot aconseguir / Error al inicialitzar cipher (nom, padding o key incorrectes).
     */
    public Object decryptAU(URI id, byte[] aubox) throws Exception {
        boolean signature = false;
        Object ob;
        if (getEP(id).getCipher() == CipherURIType.CHACHA_20_POLY_1305) {
            ob = getObject(aubox, initCipher(false, getEP(id), getKey(getEP(id).getKeyName(), "ChaCha20", Cipher.SECRET_KEY)));
        } else
            ob = getObject(aubox, initCipher(false, getEP(id), getKey(getEP(id).getKeyName(), "AES", Cipher.SECRET_KEY)));
        for (SignatureType sig : signatureParameters) {
            if (sig.getLocation().getRef().equals(id.getRef()) && sig.getLocation().getTipus().equals(id.getTipus())) {
                signature = true;
                if (!sig.comprovarSign(ob)) throw new Exception("Signatura incorrecte");
            }
        }
        if (!signature) System.out.print("Atenció, box sense comprovar signatura");
        return ob;
    }

    /**
     * Obté els bytes d'encriptar la box AccessUnitProtectionType o ReferenceType, segons l'uri.
     *
     * @param id    Id de la box a encriptar.
     * @param aubox Box a encriptar.
     * @return Bytes del box encriptat.
     * @throws Exception Error al iniciar el cipher -> No existeix cap EP / No hi ha la clau o no es pot aconseguir / Error al inicialitzar cipher (nom, padding o key incorrectes).
     */
    public byte[] encryptAU(URI id, Object aubox) throws Exception {
        if (signatureParameters == null) {
            signatureParameters = new ArrayList<>();
        }
        novaSig(new SignatureType(aubox, id));
        if (getEP(id).getCipher() == CipherURIType.CHACHA_20_POLY_1305) {
            return setObject(aubox, initCipher(true, getEP(id), getKey(getEP(id).getKeyName(), "ChaCha20", Cipher.SECRET_KEY)));
        } else
            return setObject(aubox, initCipher(true, getEP(id), getKey(getEP(id).getKeyName(), "AES", Cipher.SECRET_KEY)));
    }

    /**
     * Afegeix la nova signatura eliminant l'anterior si hi era.
     *
     * @param s Signature a afegir.
     */
    protected void novaSig(SignatureType s) {
        signatureParameters.removeIf(n -> n.getLocation().getRef().equals(s.getLocation().getRef()) && n.getLocation().getTipus().equals(s.getLocation().getTipus()));
        signatureParameters.add(s);
    }

    ///////////////
    // HASH CODE //
    ///////////////

    /**
     * Modificacio del hash code perque pugui funcionar amb la signature despres de deserialitzar.
     *
     * @return Signature de l'bjecte d'aquesta classe.
     */
    @Override
    public int hashCode() {
        if (encryptionParameters == null) {
            encryptionParameters = new ArrayList<>();
        }
        if (keyTransportAES == null) {
            keyTransportAES = new ArrayList<>();
        }
        if (signatureParameters == null) {
            signatureParameters = new ArrayList<>();
        }
        int resultAES = 0;
        int resultEP = 0;
        int resultSig = 0;
        for (KeyTransportType k : keyTransportAES) {
            resultAES += k.hashCode();
        }
        for (EncryptionParametersType e : encryptionParameters) {
            resultEP += e.hashCode();
        }
        for (SignatureType t : signatureParameters) {
            resultSig += t.hashCode();
        }
        return Objects.hash(resultAES, resultEP, resultSig);
    }

}
