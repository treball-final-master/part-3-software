package main.java.MPEGG.Utilitats;

import javax.crypto.KeyAgreement;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

public class KeyGens {
    /**
     * Genera un nou parell de claus RSA.
     *
     * @return Parell de claus.
     */
    public static KeyPair nouParellRSA() throws NoSuchAlgorithmException {
        KeyPairGenerator kg;
        kg = KeyPairGenerator.getInstance("RSA");
        kg.initialize(2048);
        return kg.generateKeyPair();
    }

    /**
     * Genera un nou parell de claus per la corba X25519.
     *
     * @return Parell de claus.
     */
    public static KeyPair nouParellEC() throws NoSuchAlgorithmException {
        KeyPairGenerator kg;
        kg = KeyPairGenerator.getInstance("X25519");
        return kg.generateKeyPair();
    }

    /**
     * Genera una clau AES.
     *
     * @param keySize Tamany de la clau.
     * @return La secret key.
     */
    public static SecretKey nouAES(Integer keySize) {
        KeyGenerator kg;
        try {
            kg = KeyGenerator.getInstance("AES");
        } catch (NoSuchAlgorithmException e) {
            System.out.print("Java no correcte, no troba un algoritme que hi ha de ser");
            return null;
        }
        kg.init(keySize);
        return kg.generateKey();
    }

    /**
     * Genera una clau clau ChaCha20.
     *
     * @param keySize Tamany de la clau.
     * @return La secret key-
     */
    public static SecretKey nouChacha(Integer keySize) {
        KeyGenerator kg;
        try {
            kg = KeyGenerator.getInstance("ChaCha20");
        } catch (NoSuchAlgorithmException e) {
            System.out.print("Java no correcte, no troba un algoritme que hi ha de ser");
            return null;
        }
        kg.init(keySize);
        return kg.generateKey();
    }

    /**
     * Genera una clau secreta apartir de la publica de l'escriptor i privada del lector.
     *
     * @param read  KeyPar del lector.
     * @param write KeyPar de l'escriptor.
     * @return Secret key del KeyAgreement.
     */
    protected static SecretKey getKeyAgreeRecived(KeyPair read, KeyPair write) throws InvalidKeyException {
        KeyAgreement ka;
        try {
            ka = KeyAgreement.getInstance("X25519");
        } catch (NoSuchAlgorithmException e) {
            System.out.print("Java no correcte, no troba un algoritme que hi ha de ser");
            return null;
        }
        ka.init(write.getPublic());
        ka.doPhase(read.getPrivate(), true);
        try {
            return ka.generateSecret("AES");
        } catch (NoSuchAlgorithmException e) {
            System.out.print("Java no correcte, no troba un algoritme que hi ha de ser");
            return null;
        }
    }

    /**
     * Genera una clau secreta apartir de la privada de l'escriptor i publica del lector.
     *
     * @param read  KeyPar del lector.
     * @param write KeyPar de l'escriptor.
     * @return Secret key del KeyAgreement.
     */
    public static SecretKey getKeyAgreeSend(KeyPair read, KeyPair write) throws InvalidKeyException {
        KeyAgreement ka;
        try {
            ka = KeyAgreement.getInstance("X25519");
        } catch (NoSuchAlgorithmException e) {
            System.out.print("Java no correcte, no troba un algoritme que hi ha de ser");
            return null;
        }
        ka.init(write.getPrivate());
        ka.doPhase(read.getPublic(), true);
        byte[] encoded = ka.generateSecret();
        return new SecretKeySpec(encoded, "AES");

    }

}
