package main.java.MPEGG.Utilitats;

import MPEGG.enume.BoxKey;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Classe per implementar l'estructura de un KLV.
 *
 * @author Marti Fernandez
 */
public class KLV implements Serializable {
    /**
     * Key del value.
     */
    protected String K;
    /**
     * Lenght del value.
     */
    protected int L;

    /**
     * Length of the value
     */
    protected Long length;
    /**
     * Value del KLV.
     */
    protected byte[] V;

    ///////////////////
    // CONSTRUCTORES //
    ///////////////////

    /**
     * Crea un nou objecte de KLV.
     *
     * @param k Key del KLV.
     * @param v Value del KLV.
     */
    public KLV(String k, byte[] v) {
        this.K = k;
        this.L = v.length;
        this.V = v;
    }

    public KLV(String k, Long length, byte[] v) {
        this.K = k;
        this.length = length;
        this.V = v;
    }

    public KLV(File file) {
        byte[] aux = null;
        try {
            aux = Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            System.out.println("Fitxer no trobat");
        }
        K = new String(aux,0,4);
        byte[] tempL = Arrays.copyOfRange(aux, 4, 12);
        ByteBuffer wrapped = ByteBuffer.wrap(tempL);
        Long tempLength = wrapped.getLong();
        length = Long.reverseBytes(tempLength);
        V = Arrays.copyOfRange(aux,12, aux.length);
    }

    public static KLV parseFile(String folderPath, BoxKey boxkey) throws Exception{
        String filePath = folderPath + "/" + boxkey;
        File file = new File(filePath);
        if(checkIfExistsFile(file)) {
            return new KLV(file);
        } else {
            throw new Exception("No file header");
        }
    }

    private static Boolean checkIfExistsFile(File file){
        return file.exists() && !file.isDirectory();
    }



    ///////////////////
    // MODIFICADORES //
    ///////////////////

    /**
     * Modifica el Value de l'objecte KLV.
     *
     * @param v Nou Value del KLV.
     */
    protected void changeBytes(@NotNull byte[] v) {
        this.L = v.length;
        this.V = v;
    }

    //////////
    // GETS //
    //////////

    /**
     * Obte la key de l'objecte KLV.
     *
     * @return Key de l'objecte KLV.
     */
    public String getK() {
        return this.K;
    }

    /**
     * Obte la lenght en bytes de l'objecte KLV.
     *
     * @return Longitud en bytes del value de l'objecte KLV.
     */
    protected int getL() {
        return this.L;
    }

    /**
     * Obte la lenght en bytes de l'objecte KLV.
     *
     * @return Longitud en bytes del value de l'objecte KLV.
     */
    public Long getLength() {
        return this.length;
    }

    /**
     * Obte el value de l'objecte KLV.
     *
     * @return Value de l'objecte KLV.
     */
    public byte[] getV() {
        return this.V;
    }

    /////////
    // OUT //
    /////////

    /**
     * Obte els bytes de l'objecte KLV inclos K i L.
     *
     * @return Bytes de l'objecte KLV.
     */
    public byte[] toBytes() {
        byte[] i = BytesUtilities.addArrays(this.K.getBytes(), ByteBuffer.allocate(4).putInt(this.L).array());
        i = BytesUtilities.addArrays(i, this.V);
        return i;
    }


    public String getKey(){
        return K;
    }

}
