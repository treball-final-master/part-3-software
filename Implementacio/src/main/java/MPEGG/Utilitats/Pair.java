package main.java.MPEGG.Utilitats;

public class Pair<T, T1> {
    protected final T first;
    protected final T1 second;

    public Pair(T first, T1 second) {
        this.first = first;
        this.second = second;
    }

    public T1 getValue() {
        return second;
    }

    public T getKey() {
        return first;
    }
}
