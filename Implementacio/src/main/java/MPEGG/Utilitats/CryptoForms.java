package main.java.MPEGG.Utilitats;

import main.java.MPEGG.Types.EncryptionParametersType;
import main.java.MPEGG.enume.CipherURIType;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.IvParameterSpec;
import java.io.*;
import java.security.Key;

public class CryptoForms {
    /**
     * Obte l'objecte despres d'aplicar el cipher per desencriptar a les dades.
     *
     * @param dades  Dades encriptades a les que se li aplicara el cipher.
     * @param cipher Cipher per desencriptar.
     * @return Objecte despres d'aplicar el cipher per desencriptar.
     */
    public static Object getObject(byte[] dades, Cipher cipher) throws IOException, ClassNotFoundException, BadPaddingException, IllegalBlockSizeException {
        InputStream istream = new ByteArrayInputStream(cipher.doFinal(dades));
        ObjectInputStream inputStream = new ObjectInputStream(istream);
        return inputStream.readObject();
    }

    /**
     * Obte els bytes despres d'aplicar el cipher per encriptar a l'objecte.
     *
     * @param a      Objecte a aplicar el cipher per encriptar.
     * @param cipher Cipher a aplicar a l'objecte.
     * @return ByteArray amb les dades de l'objecte encriptat.
     */
    public static byte[] setObject(Object a, Cipher cipher) throws IOException, BadPaddingException, IllegalBlockSizeException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(a);
        return cipher.doFinal(baos.toByteArray());
    }

    /**
     * Inicialitza el cipher per encriptar o desencriptar.
     *
     * @param mode TRUE = encriptar / FALSE = Decrypt.
     * @param EP   Encrypted Parameters de l'uri
     * @param sks  Key de l'EP.
     * @return Cipher a aplicar per encriptar o desencriptar.
     * @throws Exception Error al iniciar el cipher -> No existeix cap EP / No hi ha la clau o no es pot aconseguir / Error al inicialitzar cipher (nom, padding o key incorrectes).
     */
    public static Cipher initCipher(boolean mode, EncryptionParametersType EP, Key sks) throws Exception {
        Cipher cipher = Cipher.getInstance(EP.getCipher().value());
        if (EP.getCipher().equals(CipherURIType.CHACHA_20_POLY_1305)) {
            // ChaCha20ParameterSpec params = new ChaCha20ParameterSpec(EP.getNounce(), 1);
            // EL ChaCha20ParameterSpec nomes s'utilitza si es chacha20 sol. Sino es IV
            if (mode) cipher.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(EP.getNounce()));
            else cipher.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(EP.getNounce()));
        } else {
            if (EP.getCipher() == CipherURIType.AES_256_GCM || EP.getCipher() == CipherURIType.AES_128_GCM || EP.getCipher() == CipherURIType.AES_192_GCM) {
                if (mode) cipher.init(Cipher.ENCRYPT_MODE, sks, new GCMParameterSpec(128, EP.getIV()));
                else cipher.init(Cipher.DECRYPT_MODE, sks, new GCMParameterSpec(128, EP.getIV()));
                cipher.updateAAD(EP.getTAG());
            } else {
                if (mode) cipher.init(Cipher.ENCRYPT_MODE, sks, new IvParameterSpec(EP.getIV()));
                else cipher.init(Cipher.DECRYPT_MODE, sks, new IvParameterSpec(EP.getIV()));
            }
        }
        return cipher;
    }
}
