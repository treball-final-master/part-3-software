package main.java.MPEGG.Utilitats;

import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.enume.DadesType;

import java.io.Serializable;

/**
 * Classe per implementar l'estructura dels URI.
 *
 * @author Marti Fernandez
 */
public class URI implements Serializable {
    /**
     * Id del DatasetGroup.
     */
    protected Integer dg_id;
    /**
     * Id del Dataset.
     */
    protected Integer dt_id;
    /**
     * Tipu de dades. O Metadata, Protection o Dades.
     */
    protected DadesType tipus;
    /**
     * En cas d'apuntar a dedes (accesunit i block) tambe ha de tenir ref per indicar.
     */
    protected ReferenceType ref;


    /**
     * Nou URI buit.
     */
    public URI() {
    }

    /**
     * Nou URI copia de l'altre.
     */
    public URI(URI uri) {
        this.dt_id = uri.dt_id;
        this.dg_id = uri.dg_id;
        this.tipus = uri.tipus;
        ReferenceType r = new ReferenceType();
        r.copiRef(uri.ref);
        this.ref = r;
    }


    /**
     * Defineix l'URI perque apunti a un Block amb tipus de dades "Dades".
     *
     * @param dgid Id del DatasetGroup. -1 si es fa des d'un nivell inferior.
     * @param dtid Id del Dataset. -1 si es fa des d'un nivell inferior.
     */
    public void apuntarBlock(Integer dgid, Integer dtid, ReferenceType ref) {
        this.dg_id = dgid;
        this.dt_id = dtid;
        this.tipus = DadesType.Dades;
        this.ref = ref;
    }

    /**
     * Defineix l'URI perque apunti a un AccesUnit Protection Box amb tipu de dades "Protection".
     *
     * @param dgid Id del DatasetGroup. -1 si es fa des d'un nivell inferior.
     * @param dtid Id del Dataset. -1 si es fa des d'un nivell inferior.
     */
    public void apuntarAccesUnitPr(Integer dgid, Integer dtid, ReferenceType ref) {
        this.dg_id = dgid;
        this.dt_id = dtid;
        this.tipus = DadesType.AUProtection;
        this.ref = ref;
    }

    /**
     * Defineix l'URI perque apunti a un AccesUnit Metadata Box amb tipu de dades "Metadata".
     *
     * @param dgid Id del DatasetGroup. -1 si es fa des d'un nivell inferior.
     * @param dtid Id del Dataset. -1 si es fa des d'un nivell inferior.
     */
    public void apuntarAccesUnitMd(Integer dgid, Integer dtid, ReferenceType ref) {
        this.dg_id = dgid;
        this.dt_id = dtid;
        this.tipus = DadesType.AUMetadata;
        this.ref = ref;
    }

    /**
     * Defineix l'URI perque apunti a un Dataset Protection Box amb tipu de dades "Protection".
     *
     * @param dgid Id del DatasetGroup. -1 si es fa des d'un nivell inferior.
     * @param dtid Id del Dataset.
     */
    public void apuntarDatasetPr(Integer dgid, Integer dtid) {
        this.dg_id = dgid;
        this.dt_id = dtid;
        this.tipus = DadesType.DTProtection;
        this.ref = null;
    }

    /**
     * Defineix l'URI perque apunti a un Dataset Metadata Box amb tipu de dades "Metadata".
     *
     * @param dgid Id del DatasetGroup. -1 si es fa des d'un nivell inferior.
     * @param dtid Id del Dataset.
     */
    public void apuntarDatasetMd(Integer dgid, Integer dtid) {
        this.dg_id = dgid;
        this.dt_id = dtid;
        this.tipus = DadesType.DTMetadata;
        this.ref = null;
    }

    /**
     * Defineix l'URI perque apunti a un DatasetGroup Protection Box amb tipu de dades "Protection".
     *
     * @param dgid Id del DatasetGroup.
     */
    protected void apuntarDGPr(Integer dgid) {
        this.dg_id = dgid;
        this.tipus = DadesType.DGProtection;
        this.ref = null;
    }

    /**
     * Defineix l'URI perque apunti a un DatasetGroup Metadata Box amb tipu de dades "Metadata".
     *
     * @param dgid Id del DatasetGroup.
     */
    protected void apuntarDGMd(Integer dgid) {
        this.dg_id = dgid;
        this.tipus = DadesType.DGMetadata;
        this.ref = null;
    }

    /**
     * Obté el l'id del DatasetGroup.
     *
     * @return Id del DatasetGroup.
     */
    public Integer getDg_id() {
        return dg_id;
    }

    /**
     * Defineix l'id del DatasetGroup.
     *
     * @param dg_id Id del DatasetGroup a definir.
     */
    public void setDg_id(Integer dg_id) {
        this.dg_id = dg_id;
    }

    /**
     * Obté el l'id del Dataset.
     *
     * @return Id del Dataset.
     */
    public Integer getDt_id() {
        return dt_id;
    }

    /**
     * Defineix l'id del Dataset.
     *
     * @param dt_id Id del Dataset a definir.
     */
    public void setDt_id(Integer dt_id) {
        this.dt_id = dt_id;
    }

    /**
     * Obté el tipus de dades que apunta.
     *
     * @return Tipu de dades.
     */
    public DadesType getTipus() {
        return tipus;
    }

    /**
     * Defineix el tipus de gen-info que apunta.
     *
     * @param tipus DadesType a definir.
     */
    public void setTipus(DadesType tipus) {
        this.tipus = tipus;
    }

    /**
     * Obté el ReferenceType.
     *
     * @return Reference Type de l'URI.
     */
    public ReferenceType getRef() {
        ReferenceType r = new ReferenceType();
        r.setChromosomeName(ref.getChromosomeName());
        ReferenceType.Position p = new ReferenceType.Position();
        p.setStart(ref.getPosition().getStart());
        p.setEnd(ref.getPosition().getEnd());
        r.setPosition(p);
        return r;
    }

    /**
     * Defineix el Reference de l'AU que apunta.
     *
     * @param ref ReferenceType a definir.
     */
    public void setRef(ReferenceType ref) {
        this.ref = ref;
    }
}
