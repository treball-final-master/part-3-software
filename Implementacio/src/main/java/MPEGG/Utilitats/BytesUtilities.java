package main.java.MPEGG.Utilitats;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Classe amb funcions per tractar bytes arrays i KLVs.
 *
 * @author Marti Fernandez
 */
public class BytesUtilities {

    /**
     * Obte el primer KLV i la resta de bytes d'una cadena de Bytes.
     *
     * @param b Un Byte array amb com a minim un KLV vàlid.
     * @return Un Pair amb el primer KLV al Byte Array i un Byte Array amb el sobrant.
     */
    public static Pair<KLV, byte[]> getFirstKLV(byte[] b) {
        int pos = 0;
        String auxK;
        int auxL;
        byte[] auxV;
        byte[] tempL;
        byte[] tempK;

        tempK = Arrays.copyOfRange(b, 0, 4);
        auxK = new String(tempK);
        //System.out.print("Key: " + auxK + "\n");
        tempL = Arrays.copyOfRange(b, 4, 8);
        ByteBuffer wrapped = ByteBuffer.wrap(tempL);
        auxL = wrapped.getInt();
        //System.out.print("Lenght: " + auxL + "\n");
        pos += auxL + 8;
        auxV = Arrays.copyOfRange(b, 8, pos);
        //System.out.print("Pos inici: " + 8 + "\n");
        //System.out.print("Pos final: " + pos + "\n");
        //System.out.print("Value: " + auxV + "\n");
        KLV retk = new KLV(auxK, auxV);
        byte[] retb;
        if (pos == b.length) {
            retb = null;
        } else retb = Arrays.copyOfRange(b, pos, b.length);
        return new Pair<>(retk, retb);
    }

    /**
     * Obte la concatenacio ordenada de dos byte arrays.
     *
     * @param a Byte array 1.
     * @param b Byte array 2.
     * @return Byte array de byte array 1 i byte array 2 concatenats.
     */
    public static byte[] addArrays(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    /**
     * Obte la concatenació ordenada d'un byte array i d'un byte.
     *
     * @param a Byte array.
     * @param b Byte.
     * @return Byte array de la concatenació resultant.
     */
    protected static byte[] addArrayByte(byte[] a, byte b) {
        byte[] newarr = new byte[a.length + 1];
        System.arraycopy(a, 0, newarr, 0, a.length);
        newarr[a.length] = b;
        return newarr;
    }

    /**
     * Obte la concatenació ordenada d'un byte i d'un byte  array.
     *
     * @param a Byte.
     * @param b Byte array.
     * @return Byte array de la concatenació resultant.
     */
    protected static byte[] addByteArray(byte a, byte[] b) {
        byte[] newarr = new byte[b.length + 1];
        newarr[0] = a;
        if (b.length - 1 >= 0) System.arraycopy(b, 0, newarr, 1, b.length);
        return newarr;
    }

    /**
     * Obte la concatenació ordenada de dos bytes.
     *
     * @param a Byte 1.
     * @param b Byte 2.
     * @return Byte array de la concatenació resultant.
     */
    protected static byte[] addBytes(byte a, byte b) {
        byte[] result = new byte[2];
        result[0] = a;
        result[1] = b;
        return result;
    }

    /**
     * Obte el KLV amb la key "Key" al byte array "a".
     *
     * @param a   Byte array on es buscarà el KLV.
     * @param key Key a buscar.
     * @return KLV amb key "Key".
     */
    protected static KLV searchKeyValue(byte[] a, String key) {
        while (a.length > 0) {
            Pair<KLV, byte[]> aux = getFirstKLV(a);
            a = aux.getValue();
            if (aux.getKey().getK().equals(key)) return aux.getKey();
        }
        return null;
    }

    public static byte[] serialitzar(Object object) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(baos);
        oos.writeObject(object);
        return baos.toByteArray();

    }

    public static Object deserialitzar(byte[] dades) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bais = new ByteArrayInputStream(dades);
        ObjectInputStream ois = new ObjectInputStream(bais);
        return ois.readObject();
    }

    public static int parseuint8(byte uint8) {
        return uint8 & 0xFF;
    }

    public static int parseuint32(byte[] aux, int index) {
        byte[] temp = Arrays.copyOfRange(aux, index, index + 4);
        ByteBuffer wrapped = ByteBuffer.wrap(temp);
        return wrapped.getInt();
    }

    public static short parserShort(byte[] aux, int index) {
        byte[] temp = Arrays.copyOfRange(aux, index, index + 2);
        ByteBuffer wrapped = ByteBuffer.wrap(temp);
        return wrapped.getShort();
    }
}
