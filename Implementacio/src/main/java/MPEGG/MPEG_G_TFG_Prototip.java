package main.java.MPEGG;

import MPEGG.Types.XMLSignature.Signature;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Data.Dataset;
import main.java.MPEGG.Data.DatasetGroup;
import main.java.MPEGG.Data.MPEG_G;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.SimplePubKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.DadesType;
import main.java.MPEGG.enume.WrapType;

import javax.crypto.Cipher;
import javax.sound.midi.SysexMessage;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.xpath.XPathConstants;
import java.util.ArrayList;
import java.util.Random;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.*;

/**
 * @author Marti Fernandez
 */
public class MPEG_G_TFG_Prototip {

    public static void tfmTestEnviroment() throws Exception{
        MPEG_G fitxer2 = new MPEG_G("/home/xemyst/Documents/9 - TFM/MPEG-G_sample");
        Signature a = new Signature();
        a.run();
        System.out.println();


    }

    protected static void ProvaTFG() throws Exception {
        // 1- Crea una instancia de fitxer buida.
        MPEG_G fitxer = new MPEG_G();
        tfmTestEnviroment();
        // 2- Creació de Blocks de diferents formes.
        Random rand = new Random();
        byte[] dadesaux1 = new byte[300];
        byte[] dadesaux2 = new byte[600];
        byte[] dadesaux3 = new byte[200];
        byte[] dadesaux4 = new byte[1200];
        byte[] dadesaux5 = new byte[100];
        byte[] dadesaux6 = new byte[400];
        rand.nextBytes(dadesaux1);
        rand.nextBytes(dadesaux2);
        rand.nextBytes(dadesaux3);
        rand.nextBytes(dadesaux4);
        rand.nextBytes(dadesaux5);
        rand.nextBytes(dadesaux6);
        Integer id1 = fitxer.addDatasetGroup();
        Integer id2 = fitxer.getDatasetGroup(id1).addDataset();
        URI create = new URI();
        ReferenceType.Position pos = new ReferenceType.Position();
        pos.setStart(0);
        pos.setEnd(300);
        ReferenceType ref1 = new ReferenceType();
        ref1.setChromosomeName("chr1");
        ref1.setPosition(pos);
        create.apuntarBlock(id1, -1, ref1);
        URI uri_block1 = fitxer.newBlock(create, dadesaux1);
        pos.setStart(301);
        pos.setEnd(900);
        ref1.setPosition(pos);
        create.setRef(ref1);
        URI uri_block2 = fitxer.newBlock(create, dadesaux2);
        pos.setStart(0);
        pos.setEnd(900);
        ref1.setPosition(pos);
        ref1.setChromosomeName("chr2");
        create.apuntarBlock(id1, id2, ref1);
        URI uri_block3 = fitxer.newBlock(create, dadesaux3);
        DatasetGroup dg = fitxer.getDatasetGroup(id1);
        Dataset dt = dg.getDataset(id2);
        pos.setStart(1000);
        pos.setEnd(1900);
        ref1.setPosition(pos);
        URI uri_block4 = dt.newBlock(create, dadesaux4);
        pos.setStart(2000);
        pos.setEnd(2900);
        ref1.setPosition(pos);
        URI uri_block5 = dg.newBlock(create, dadesaux5);
        pos.setStart(3000);
        pos.setEnd(3900);
        ref1.setPosition(pos);
        URI uri_block6 = fitxer.newBlock(create, dadesaux6);


        // 2b- Definim URIs per mes facilitat.
        URI aupr_1P = new URI(uri_block1);
        aupr_1P.setTipus(DadesType.AUProtection);
        URI aupr_1M = new URI(uri_block1);
        aupr_1M.setTipus(DadesType.AUMetadata);
        URI aupr_2P = new URI(uri_block2);
        aupr_2P.setTipus(DadesType.AUProtection);
        URI aupr_2M = new URI(uri_block2);
        aupr_2M.setTipus(DadesType.AUMetadata);
        URI aupr_3P = new URI(uri_block3);
        aupr_3P.setTipus(DadesType.AUProtection);
        URI aupr_3M = new URI(uri_block3);
        aupr_3M.setTipus(DadesType.AUMetadata);
        URI aupr_4P = new URI(uri_block4);
        aupr_4P.setTipus(DadesType.AUProtection);
        URI aupr_4M = new URI(uri_block4);
        aupr_4M.setTipus(DadesType.AUMetadata);
        URI aupr_5P = new URI(uri_block5);
        aupr_5P.setTipus(DadesType.AUProtection);
        URI aupr_5M = new URI(uri_block5);
        aupr_5M.setTipus(DadesType.AUMetadata);
        URI aupr_6P = new URI(uri_block6);
        aupr_6P.setTipus(DadesType.AUProtection);
        URI aupr_6M = new URI(uri_block6);
        aupr_6M.setTipus(DadesType.AUMetadata);
        URI dtpr_1P = new URI(uri_block1);
        dtpr_1P.setTipus(DadesType.DTProtection);
        URI dtpr_2P = new URI(uri_block2);
        dtpr_2P.setTipus(DadesType.DTProtection);
        URI dtpr_3P = new URI(uri_block3);
        dtpr_3P.setTipus(DadesType.DTProtection);
        URI dtpr_1M = new URI(uri_block1);
        dtpr_1M.setTipus(DadesType.DTMetadata);
        URI dtpr_2M = new URI(uri_block2);
        dtpr_2M.setTipus(DadesType.DTMetadata);
        URI dtpr_3M = new URI(uri_block3);
        dtpr_3M.setTipus(DadesType.DTMetadata);


        // Definim la llista amb claus que a la realitzat possiblement s'enviarien apart.
        llistaClaus = new keysList();
        KeyTransportType a = new SimpleKeyType("clau1", nouAES(256));
        KeyTransportType b = new SimpleKeyType("clau2", nouAES(256));
        KeyTransportType c = new SimpleKeyType("clau3", nouAES(256));
        KeyTransportType d = new SimpleKeyType("clau4", nouAES(256));
        KeyTransportType e = new SimplePubKeyType("clau5", nouParellRSA());
        KeyTransportType f = new SimplePubKeyType("clau6", nouParellRSA());
        KeyTransportType g = new SimplePubKeyType("clau7", nouParellEC());
        KeyTransportType h = new SimplePubKeyType("clau8", nouParellEC());
        llistaClaus.addKey(a);
        llistaClaus.addKey(b);
        llistaClaus.addKey(c);
        llistaClaus.addKey(d);
        llistaClaus.addKey(e);
        llistaClaus.addKey(f);
        llistaClaus.addKey(g);
        llistaClaus.addKey(h);


        // 3- Afegir els EP.
        fitxer.getDatasetGroup(uri_block1.getDg_id()).getDataset(uri_block1.getDt_id()).setNewAUParams(uri_block1.getRef(), CipherURIType.AES_256_GCM, a.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(uri_block2.getDg_id()).getDataset(uri_block2.getDt_id()).setNewAUParams(uri_block2.getRef(), CipherURIType.AES_256_GCM, b.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(uri_block3.getDg_id()).getDataset(uri_block3.getDt_id()).setNewAUParams(uri_block3.getRef(), CipherURIType.AES_256_GCM, a.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(uri_block4.getDg_id()).getDataset(uri_block4.getDt_id()).setNewAUParams(uri_block4.getRef(), CipherURIType.AES_256_GCM, a.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(uri_block5.getDg_id()).getDataset(uri_block5.getDt_id()).setNewAUParams(uri_block5.getRef(), CipherURIType.AES_256_GCM, c.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(uri_block6.getDg_id()).getDataset(uri_block6.getDt_id()).setNewAUParams(uri_block6.getRef(), CipherURIType.AES_256_GCM, a.getKey("AES", Cipher.SECRET_KEY));
        fitxer.getDatasetGroup(aupr_1P.getDg_id()).getDataset(aupr_1P.getDt_id()).setNewDTEP(CipherURIType.AES_256_CTR, a, aupr_1P, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_2P.getDg_id()).getDataset(aupr_2P.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, e, aupr_2P, WrapType.Asimetric);
        fitxer.getDatasetGroup(aupr_3P.getDg_id()).getDataset(aupr_3P.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, b, aupr_3P, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_4P.getDg_id()).getDataset(aupr_4P.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, f, aupr_4P, WrapType.Asimetric);
        fitxer.getDatasetGroup(aupr_5P.getDg_id()).getDataset(aupr_5P.getDt_id()).setNewDTEP(CipherURIType.AES_256_CTR, c, aupr_5P, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_6P.getDg_id()).getDataset(aupr_6P.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, a, aupr_6P, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_1M.getDg_id()).getDataset(aupr_1M.getDt_id()).setNewDTEP(CipherURIType.CHACHA_20_POLY_1305, a, aupr_1M, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_2M.getDg_id()).getDataset(aupr_2M.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, b, aupr_2M, WrapType.Derivation);
        fitxer.getDatasetGroup(aupr_3M.getDg_id()).getDataset(aupr_3M.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, a, aupr_3M, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_4M.getDg_id()).getDataset(aupr_4M.getDt_id()).setNewDTEP(CipherURIType.CHACHA_20_POLY_1305, e, aupr_4M, WrapType.Asimetric);
        fitxer.getDatasetGroup(aupr_5M.getDg_id()).getDataset(aupr_5M.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, c, aupr_5M, WrapType.Simetric);
        fitxer.getDatasetGroup(aupr_6M.getDg_id()).getDataset(aupr_6M.getDt_id()).setNewDTEP(CipherURIType.AES_256_GCM, f, aupr_6M, WrapType.Asimetric);
        fitxer.getDatasetGroup(dtpr_1P.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, a, dtpr_1P, WrapType.Simetric);
        fitxer.getDatasetGroup(dtpr_2P.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, f, dtpr_2P, WrapType.Asimetric);
        fitxer.getDatasetGroup(dtpr_3P.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, d, dtpr_3P, WrapType.Simetric);
        fitxer.getDatasetGroup(dtpr_1M.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, g, dtpr_1M, WrapType.EC, h);
        fitxer.getDatasetGroup(dtpr_2M.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, f, dtpr_2M, WrapType.Asimetric);
        fitxer.getDatasetGroup(dtpr_3M.getDg_id()).setNewDGEP(CipherURIType.AES_256_GCM, a, dtpr_3M, WrapType.Simetric);


        // 4- Encryptar tot.
        fitxer.encryptAll();


        // 5- Escriure el fitxer.
        fitxer.writeFile("TFG_test");


        // 6- Creacio d'una segona instancia.
        MPEG_G fitxer2 = new MPEG_G();


        // 7- Llegir fitxer de la primera instancia.
        fitxer2.readFile("TFG_test");


        // 8- Llegir block i comprovar.
        ArrayList<Integer> aaa = fitxer2.getDGIds();
        dg = fitxer2.getDatasetGroup(aaa.get(0));
        ArrayList<Integer> bbb = dg.getDTIds();
        dt = dg.getDataset(bbb.get(0));
        ArrayList<ReferenceType> ccc = dt.getAURefs();
        byte[] block = null;
        URI auxUri = new URI();
        auxUri.setRef(ccc.get(0));
        try {
            block = dt.readBlock(auxUri);
        } catch (Exception exp) {
            System.out.print("Excepcio encriptat correcte\n");
        }

        // Desencriptar tot
        fitxer.decryptAll();
        fitxer2.decryptAll();

        // Printem el bloc.
        block = dt.readBlock(auxUri);
        for (byte value : block) {
            System.out.print(value);
        }

        // Desencriptem i guardem per tenir un fitxer sense encriptar.
        fitxer2.writeFile("TFG2");
    }

    public static void main(String[] arg) throws Exception {
        ProvaTFG();
    }
}
