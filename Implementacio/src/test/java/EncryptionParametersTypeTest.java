package main.java.MPEGG.Types;

import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class EncryptionParametersTypeTest {
    EncryptionParametersType ep;
    KeyTransportType keyAES;

    @BeforeEach
    void setUp() {
        ep = new EncryptionParametersType();
        llistaClaus = new keysList();
    }

    @Test
    void setNewForEPValues() throws Exception {
        KeyTransportType wrapper = new SimpleKeyType("clau1", nouAES(128));
        URI uri1 = new URI();
        ReferenceType ref = new ReferenceType();
        uri1.apuntarBlock(-1, -1, ref);
        uri1.apuntarBlock(0, 1, ref);
        keyAES = ep.setNewForEPValues(CipherURIType.AES_128_CTR, wrapper, uri1, WrapType.Simetric, null);
        assertEquals(ep.getEncryptedLocations(), uri1);
        assertEquals(ep.getCipher(), CipherURIType.AES_128_CTR);
        llistaClaus.addKey(wrapper);
        assertNotNull(keyAES.getKey("AES", Cipher.SECRET_KEY));
    }
}