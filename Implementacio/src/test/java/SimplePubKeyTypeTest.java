package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import static main.java.MPEGG.Utilitats.KeyGens.nouParellRSA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimplePubKeyTypeTest {
    SimplePubKeyType spk;
    KeyPair kp;

    @BeforeEach
    void setUp() throws NoSuchAlgorithmException {
        kp = nouParellRSA();
    }

    @Test
    void gestioClau() throws Exception {
        spk = new SimplePubKeyType("test", kp);
        assertEquals(spk.getKey("RSA", Cipher.PUBLIC_KEY), kp.getPublic());
        spk = new SimplePubKeyType();
        boolean throwss = false;
        try {
            spk.getKey("RSA", Cipher.PUBLIC_KEY);
        } catch (Exception e) {
            throwss = true;
        }
        assertTrue(throwss);
    }
}