package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SimpleKeyTypeTest {
    SimpleKeyType spk;
    SecretKey sec;

    @BeforeEach
    void setUp() {
        sec = nouAES(256);
    }

    @Test
    void gestioClau() throws Exception {
        spk = new SimpleKeyType("test", sec);
        assertEquals(spk.getKey("AES", Cipher.SECRET_KEY), sec);
        spk = new SimpleKeyType();
        boolean throwss = false;
        try {
            spk.getKey("AES", Cipher.SECRET_KEY);
        } catch (Exception e) {
            throwss = true;
        }
        assertTrue(throwss);
    }
}