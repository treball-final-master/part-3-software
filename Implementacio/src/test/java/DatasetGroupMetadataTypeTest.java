package main.java.MPEGG.Boxes;

import main.java.MPEGG.Boxes.DatasetGroupMetadataType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DatasetGroupMetadataTypeTest {
    DatasetGroupMetadataType dgmd;
    String id;

    @BeforeEach
    void setUp() {
        dgmd = new DatasetGroupMetadataType();
        id = "2";
    }

    @Test
    void ids() {
        dgmd.setId(id);
        assertEquals(dgmd.getId(), id);
    }
}