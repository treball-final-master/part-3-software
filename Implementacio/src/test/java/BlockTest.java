package main.java.MPEGG.Data;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BlockTest {
    Block b;

    @BeforeEach
    void setUp() {
        b = new Block();
    }

    /**
     * getBytes i setBytes
     */
    @Test
    void gestioBytes() {
        Random a = new Random();
        byte[] aux = new byte[20];
        assertEquals(b.getBytes(), b.dades);
        a.nextBytes(aux);
        b.setBytes(aux);
        assertEquals(b.getBytes(), aux);
    }
}