package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.Key;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class KeySymmetricWrapTypeTest {
    KeyTransportType wrapper;
    Key aWrap;
    String nomClau;
    KeySymmetricWrapType wraped;

    @BeforeEach
    void setUp() {
        nomClau = "test";
        aWrap = nouAES(128);
        wrapper = new SimpleKeyType("Hola", nouAES(128));
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
    }

    @Test
    void getKey() throws Exception {
        wraped = new KeySymmetricWrapType(nomClau, wrapper, aWrap);
        assertArrayEquals(wraped.getKey("AES", Cipher.SECRET_KEY).getEncoded(), aWrap.getEncoded());
    }

    @Test
    void wrapKey() throws Exception {
        wraped = new KeySymmetricWrapType(nomClau, wrapper, aWrap);
        assertNotEquals(wraped.wrappedKey, aWrap);
    }

}