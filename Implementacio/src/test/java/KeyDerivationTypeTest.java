package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class KeyDerivationTypeTest {
    KeyDerivationType deriv;
    KeyDerivationType deriv2;
    KeyTransportType kt;

    @BeforeEach
    void setUp() throws InvalidKeySpecException, NoSuchAlgorithmException {
        kt = new SimpleKeyType("pass", nouAES(256));
        deriv = new KeyDerivationType("test", "pass");
        deriv2 = new KeyDerivationType("test2", "pass");
        llistaClaus = new keysList();
        llistaClaus.addKey(kt);
    }

    @Test
    void getKey() throws Exception {
        assertArrayEquals(deriv.getKey("AES", Cipher.SECRET_KEY).getEncoded(), deriv.getKey("AES", Cipher.SECRET_KEY).getEncoded());
        assertNotEquals(deriv.getKey("AES", Cipher.SECRET_KEY).getEncoded(), deriv2.getKey("AES", Cipher.SECRET_KEY).getEncoded());
    }
}