package main.java.MPEGG.Data;

import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Utilitats.URI;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class MPEG_GTest {
    MPEG_G fitxer;
    DatasetGroup dg;

    @BeforeEach
    void setUp() throws Exception {
        fitxer = new MPEG_G();
        URI uri1 = new URI();
        ReferenceType ref = new ReferenceType();
        uri1.apuntarBlock(-1, -1, ref);
        Random r = new Random();
        byte[] aux = new byte[50];
        r.nextBytes(aux);
        fitxer.newBlock(uri1, aux);
    }

    /**
     * actualitzarBytes i parse
     */
    @Test
    void parsejar() throws Exception {
        byte[] aux2 = fitxer.getBytes();
        MPEG_G fitxer2 = new MPEG_G();
        fitxer2.parse(aux2);
        assertArrayEquals(fitxer.getBytes(), fitxer2.getBytes());
    }

    /**
     * addDatasetGroup, readDatasetGroup i getDGIds
     */
    @Test
    void gestioDatasetGroup() throws Exception {
        Integer id;
        id = fitxer.addDatasetGroup();
        dg = fitxer.getDatasetGroup(id);
        DatasetGroup obj = new DatasetGroup();
        assertEquals(dg.datasets.size(), obj.datasets.size());
        ArrayList<Integer> arr = fitxer.getDGIds();
        assertTrue(arr.contains(id));
    }

    /**
     * readFileBytes i writeFileBytes
     */
    @Test
    void readiwriteFileBytes() throws Exception {
        Random r = new Random();
        byte[] aux = new byte[50];
        r.nextBytes(aux);
        fitxer.writeFile("test");
        MPEG_G fitxer2 = new MPEG_G();
        fitxer2.readFile("test");
        assertArrayEquals(fitxer.getBytes(), fitxer2.getBytes());
    }
}