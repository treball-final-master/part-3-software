package main.java.MPEGG.Utilitats;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PairTest {

    @Test
    void getValue() {
        Pair<String, String> aux = new Pair<>("Hola", "Adeu");
        String a = aux.getValue();
        assertEquals(a, "Adeu");
    }

    @Test
    void getKey() {
        Pair<String, String> aux = new Pair<>("Hola", "Adeu");
        String a = aux.getKey();
        assertEquals(a, "Hola");
    }
}