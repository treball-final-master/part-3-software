package main.java.MPEGG.Data;

import main.java.MPEGG.Boxes.AccessUnitProtectionType;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Types.AccessUnitEncryptionParametersType;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.Key;
import java.util.Random;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class AccesUnitTest {
    AccesUnit accesUnit;

    @BeforeEach
    void setUp() {
        accesUnit = new AccesUnit();
    }

    /**
     * parse i actualitzarbytes
     */
    @Test
    void gestioBytes() throws Exception {
        Random rand = new Random();
        byte[] aleatori = new byte[100];
        rand.nextBytes(aleatori);
        ReferenceType ref = new ReferenceType();
        ReferenceType.Position p = new ReferenceType.Position();
        p.setStart(1);
        p.setEnd(100);
        ref.setPosition(p);
        ref.setChromosomeName("ch1");
        accesUnit.newBlock(aleatori, ref);
        byte[] aux = accesUnit.getBytes();
        AccesUnit au2 = new AccesUnit(aux);
        assertArrayEquals(accesUnit.readBlock(), au2.readBlock());
        assertEquals(accesUnit.isEncryptedBoxP(), au2.isEncryptedBoxP());
        p.setStart(40);
        p.setEnd(300);
        ref.setPosition(p);
        assertTrue(accesUnit.intersects(ref));
        p.setStart(150);
        p.setEnd(300);
        ref.setPosition(p);
        assertFalse(accesUnit.intersects(ref));
    }

    /**
     * newMeta, getReference, isEncryptedBoxMd, setReference, getReferenceEncry, setReferenceEncrypted
     */
    @Test
    void gestioMeta() throws Exception {
        // Reference
        assertNotNull(accesUnit.getReference());
        accesUnit.reference = null;
        assertNull(accesUnit.getReference());
        accesUnit.newMeta();
        assertNotNull(accesUnit.getReference());
        ReferenceType ref = new ReferenceType();
        ref.setChromosomeName("a");
        accesUnit.setReference(ref);
        assertEquals(accesUnit.getReference().getChromosomeName(), ref.getChromosomeName());
        Random rand = new Random();
        byte[] aleatori = new byte[10];
        rand.nextBytes(aleatori);
        accesUnit.reference = null;
        assertNull(accesUnit.getReference());
        accesUnit.newMeta();
        assertNotNull(accesUnit.getReference());
    }

    /**
     * newAUProtection, getAUProtection, isEncryptedBoxP, setAUProtection, getAUProtectionEncry, setAUProtectionEncrypted
     */
    @Test
    void gestioProte() throws Exception {
        accesUnit.AUProtection = null;
        assertNull(accesUnit.getAUProtection());
        accesUnit.newProtection();
        assertNotNull(accesUnit.getAUProtection());
        assertNull(accesUnit.getAUProtectionEncry());
        assertFalse(accesUnit.isEncryptedBoxP());
        AccessUnitProtectionType aupr = new AccessUnitProtectionType();
        accesUnit.setAUProtection(aupr);
        assertEquals(accesUnit.getAUProtection(), aupr);
        Random rand = new Random();
        byte[] aleatori = new byte[10];
        rand.nextBytes(aleatori);
        accesUnit.setAUProtectionEncrypted(aleatori);
        assertTrue(accesUnit.isEncryptedBoxP());
        assertEquals(accesUnit.getAUProtectionEncry(), aleatori);
        assertNull(accesUnit.getAUProtection());
        accesUnit.setAUProtection(null);
        boolean throwss = false;
        try {
            accesUnit.isEncryptedBoxP();
        } catch (Exception e) {
            throwss = true;
        }
        assertTrue(throwss);

        accesUnit.newProtection();
        assertNotNull(accesUnit.getAUProtection());
    }

    /**
     * newBlock, encryptBlock, setBlockEP, decryptBlock, getBlock, readBlock
     */
    @Test
    void gestioBlock() throws Exception {
        Random rand = new Random();
        byte[] aleatori = new byte[10];
        rand.nextBytes(aleatori);
        ReferenceType ref = new ReferenceType();
        accesUnit.newBlock(aleatori, ref);
        assertEquals(accesUnit.readBlock(), aleatori);
        assertEquals(accesUnit.getBlock(), accesUnit.bloc);
        AccessUnitEncryptionParametersType auep = new AccessUnitEncryptionParametersType();
        Key wrapper = nouAES(128);
        auep.setNewForAUValues(CipherURIType.AES_128_CTR, wrapper);
        accesUnit.setBlockEP(auep);
        accesUnit.EncryptBlock();
        assertNotEquals(accesUnit.readBlock(), aleatori);
        accesUnit.DecryptBlock();
        assertArrayEquals(accesUnit.readBlock(), aleatori);
    }

    @Test
    void testIntersect() {
        URI uri1 = new URI();
        ReferenceType ref = new ReferenceType();
        ref.setChromosomeName("chr1");
        ReferenceType.Position p = new ReferenceType.Position();
        p.setEnd(10);
        p.setStart(0);
        ref.setPosition(p);
        accesUnit.setReference(ref);
        assertTrue(accesUnit.intersects(ref));
        p.setEnd(11);
        p.setStart(1);
        ref.setPosition(p);
        assertTrue(accesUnit.intersects(ref));
        p.setEnd(11);
        p.setStart(0);
        ref.setPosition(p);
        assertTrue(accesUnit.intersects(ref));
        p.setEnd(10);
        p.setStart(1);
        ref.setPosition(p);
        assertTrue(accesUnit.intersects(ref));
        p.setEnd(12);
        p.setStart(10);
        ref.setPosition(p);
        assertTrue(accesUnit.intersects(ref));
        p.setEnd(12);
        p.setStart(11);
        ref.setPosition(p);
        assertFalse(accesUnit.intersects(ref));
    }

}