package main.java.MPEGG.Boxes;

import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class DatasetGroupProtectionTypeTest {
    DatasetGroupProtectionType dgpr;
    DatasetProtectionType dtpr;
    DatasetMetadataType dtmd;
    byte[] dtprE;
    byte[] dtmdE;
    KeyTransportType wrapper;
    URI uri1;
    URI uri2;

    @BeforeEach
    void setUp() throws Exception {
        dgpr = new DatasetGroupProtectionType();
        wrapper = new SimpleKeyType("Test", nouAES(128));
        uri1 = new URI();
        uri2 = new URI();
        uri1.apuntarDatasetPr(1, 1);
        uri2.apuntarDatasetMd(1, 1);
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
        dgpr.setNewEP(CipherURIType.CHACHA_20_POLY_1305, wrapper, uri1, WrapType.Simetric, null);
        dgpr.setNewEP(CipherURIType.AES_128_CTR, wrapper, uri2, WrapType.Simetric, null);
    }

    @Test
    void cryptDt() throws Exception {
        dtpr = new DatasetProtectionType();
        dtmd = new DatasetMetadataType();
        dtprE = dgpr.encryptDT(uri1, dtpr);
        dtmdE = dgpr.encryptDT(uri2, dtmd);
        DatasetProtectionType auxpr = (DatasetProtectionType) dgpr.decryptDT(uri1, dtprE);
        DatasetMetadataType auxmd = (DatasetMetadataType) dgpr.decryptDT(uri2, dtmdE);
        assertEquals(dtpr.encryptionParameters, auxpr.encryptionParameters);
        assertEquals(dtpr.keyTransportAES, auxpr.keyTransportAES);
        assertEquals(dtmd.id, auxmd.id);
    }

    @Test
    void setNewEP() throws Exception {
        // Ja inicialitzat, anem a provar
        assertEquals(dgpr.getEP(uri1).getCipher(), CipherURIType.CHACHA_20_POLY_1305);
        assertEquals(dgpr.getEP(uri1).getEncryptedLocations(), uri1);
        assertNotNull(dgpr.keyTransportAES.get(0));
        assertEquals(dgpr.getEP(uri2).getEncryptedLocations(), uri2);
    }

    @Test
    void deleteEP() throws Exception {
        assertNotNull(dgpr.getEP(uri1));
        dgpr.deleteEP(uri1);
        boolean exc = false;
        try {
            dgpr.getEP(uri1);
        } catch (Exception e) {
            exc = true;
        }
        assertTrue(exc);
    }
}