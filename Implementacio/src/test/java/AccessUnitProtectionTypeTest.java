package main.java.MPEGG.Boxes;

import main.java.MPEGG.Data.Block;
import main.java.MPEGG.Types.AccessUnitEncryptionParametersType;
import main.java.MPEGG.enume.CipherURIType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.security.Key;
import java.util.Random;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class AccessUnitProtectionTypeTest {
    Block b;
    AccessUnitProtectionType aupr;
    Key master;
    byte[] aux;

    @BeforeEach
    void setUp() throws Exception {
        b = new Block();
        Random a = new Random();
        aux = new byte[20];
        a.nextBytes(aux);
        b.setBytes(aux);
        master = nouAES(128);
        AccessUnitEncryptionParametersType au = new AccessUnitEncryptionParametersType();
        au.setNewForAUValues(CipherURIType.AES_128_CTR, master);
        aupr = new AccessUnitProtectionType();
        aupr.setEP(au);
    }

    @Test
    void cryptBlock() throws Exception {
        assertEquals(b.getBytes(), aux);
        b.setBytes(aupr.encryptBlock(b.getBytes()));
        assertNotEquals(b.getBytes(), aux);
        b.setBytes(aupr.decryptBlock(b.getBytes()));
        assertArrayEquals(b.getBytes(), aux);
    }
}