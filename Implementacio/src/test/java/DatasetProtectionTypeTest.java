package main.java.MPEGG.Boxes;

import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class DatasetProtectionTypeTest {
    DatasetProtectionType dtpr;
    AccessUnitProtectionType aupr;
    ReferenceType rfmd;
    byte[] auprE;
    byte[] rfmdE;
    KeyTransportType wrapper;
    URI uri1;
    URI uri2;

    @BeforeEach
    void setUp() throws Exception {
        dtpr = new DatasetProtectionType();
        wrapper = new SimpleKeyType("Test", nouAES(128));
        uri1 = new URI();
        uri2 = new URI();
        ReferenceType ref = new ReferenceType();
        ref.setChromosomeName("a");
        ReferenceType.Position p = new ReferenceType.Position();
        p.setStart(0);
        p.setEnd(10);
        uri1.apuntarAccesUnitPr(1, 1, ref);
        uri2.apuntarAccesUnitMd(1, 1, ref);
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
        dtpr.setNewEP(CipherURIType.CHACHA_20_POLY_1305, wrapper, uri1, WrapType.Simetric, null);
        dtpr.setNewEP(CipherURIType.AES_128_CTR, wrapper, uri2, WrapType.Simetric, null);
    }

    @Test
    void cryptAU() throws Exception {
        aupr = new AccessUnitProtectionType();
        rfmd = new ReferenceType();
        auprE = dtpr.encryptAU(uri1, aupr);
        rfmdE = dtpr.encryptAU(uri2, rfmd);
        AccessUnitProtectionType aux = (AccessUnitProtectionType) dtpr.decryptAU(uri1, auprE);
        ReferenceType rau = (ReferenceType) dtpr.decryptAU(uri2, rfmdE);
        assertEquals(aupr.accessUnitEncryptionParameters, aux.accessUnitEncryptionParameters);
        assertEquals(rfmd.getChromosomeName(), rau.getChromosomeName());
    }

    @Test
    void setNewEP() throws Exception {
        // Ja inicialitzat, anem a provar
        assertEquals(dtpr.getEP(uri1).getCipher(), CipherURIType.CHACHA_20_POLY_1305);
        assertEquals(dtpr.getEP(uri1).getEncryptedLocations(), uri1);
        assertNotNull(dtpr.keyTransportAES.get(0));
        assertEquals(dtpr.getEP(uri2).getEncryptedLocations(), uri2);
    }

    @Test
    void deleteEP() throws Exception {
        assertNotNull(dtpr.getEP(uri1));
        dtpr.deleteEP(uri1);
        boolean exc = false;
        try {
            dtpr.getEP(uri1);
        } catch (Exception e) {
            exc = true;
        }
        assertTrue(exc);
    }
}