package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static main.java.MPEGG.Utilitats.KeyGens.nouParellRSA;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class KeyAsymmetricWrapTypeTest {
    KeyTransportType wrapper;
    Key aWrap;
    String nomClau;
    String hash;
    String maskhash;
    KeyAsymmetricWrapType wraped;
    PrivateKey privada;
    KeyPair kp;

    @BeforeEach
    void setUp() throws NoSuchAlgorithmException {
        nomClau = "test";
        aWrap = nouAES(256);
        kp = nouParellRSA();
        wrapper = new SimplePubKeyType("Hola", kp);
        hash = "SHA-256";
        maskhash = "SHA-256";
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
    }

    @Test
    void getKey() throws Exception {
        wraped = new KeyAsymmetricWrapType(nomClau, hash, maskhash, wrapper, aWrap);
        assertEquals(wraped.getKey("AES", Cipher.SECRET_KEY), aWrap);
    }

    @Test
    void wrapKey() throws Exception {
        wraped = new KeyAsymmetricWrapType(nomClau, hash, maskhash, wrapper, aWrap);
        assertNotEquals(wraped.wrappedKey, aWrap);
    }
}