package main.java.MPEGG.Types;

import main.java.MPEGG.Boxes.AccessUnitProtectionType;
import main.java.MPEGG.Boxes.DatasetProtectionType;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Utilitats.URI;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static main.java.MPEGG.Utilitats.BytesUtilities.deserialitzar;
import static main.java.MPEGG.Utilitats.BytesUtilities.serialitzar;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SignatureTypeTest {

    @Test
    void comprovarSign() throws IOException, ClassNotFoundException {
        AccessUnitProtectionType aupr = new AccessUnitProtectionType();
        DatasetProtectionType dtpr = new DatasetProtectionType();
        KeyTransportType kt = new SimpleKeyType("hola", nouAES(128));
        dtpr.addKeyAES(kt);
        URI uri1 = new URI();
        URI uri2 = new URI();
        ReferenceType ref = new ReferenceType();
        uri1.apuntarAccesUnitPr(0, 0, ref);
        uri2.apuntarDatasetPr(0, 0);
        SignatureType s1 = new SignatureType(aupr, uri1);
        SignatureType s2 = new SignatureType(dtpr, uri2);
        assertTrue(s1.comprovarSign(aupr));
        assertTrue(s2.comprovarSign(dtpr));
        byte[] aux1 = serialitzar(aupr);
        byte[] aux2 = serialitzar(dtpr);
        aupr = (AccessUnitProtectionType) deserialitzar(aux1);
        dtpr = (DatasetProtectionType) deserialitzar(aux2);
        assertTrue(s1.comprovarSign(aupr));
        assertTrue(s2.comprovarSign(dtpr));
    }
}