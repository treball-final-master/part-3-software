package main.java.MPEGG.Utilitats;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class KLVTest {
    KLV a;
    KLV b;
    KLV c;
    byte[] va = new byte[10];
    byte[] vb = new byte[15];
    byte[] vc = new byte[20];

    @BeforeEach
    void setUp() {
        Random gene = new Random();
        gene.nextBytes(va);
        gene.nextBytes(vb);
        gene.nextBytes(vc);
        a = new KLV("aupr", va);
        b = new KLV("dtmd", vb);
        c = new KLV("er", vc);
    }

    @Test
    void changeBytes() {
        assertEquals(a.getV(), va);
        a.changeBytes(vb);
        assertEquals(a.getV(), vb);
        b.changeBytes(va);
        assertEquals(b.getV(), va);
    }

    @Test
    void getK() {
        assertEquals(a.getK(), "aupr");
        assertEquals(b.getK(), "dtmd");
        assertEquals(c.getK(), "er");
        assertNotEquals(a.getK(), "au");
    }

    @Test
    void getL() {
        assertEquals(a.getL(), 10);
        assertEquals(b.getL(), 15);
        assertEquals(c.getL(), 20);
        assertNotEquals(a.getL(), 9);
    }

    @Test
    void getV() {
        assertEquals(a.getV(), va);
        assertEquals(b.getV(), vb);
        assertEquals(c.getV(), vc);
        assertNotEquals(a.getV(), "dgsd");
    }
}