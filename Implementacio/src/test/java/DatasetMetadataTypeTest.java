package main.java.MPEGG.Boxes;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DatasetMetadataTypeTest {
    DatasetMetadataType dtmd;
    String id;

    @BeforeEach
    void setUp() {
        dtmd = new DatasetMetadataType();
        id = "2";
    }

    @Test
    void ids() {
        dtmd.setId(id);
        assertEquals(dtmd.getId(), id);
    }
}