package main.java.MPEGG.Utilitats;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Random;

import static main.java.MPEGG.Utilitats.BytesUtilities.*;
import static org.junit.jupiter.api.Assertions.*;

class BytesUtilitiesTest {
    KLV a;
    KLV b;
    KLV c;
    byte[] va = new byte[10];
    byte[] vb = new byte[15];
    byte[] vc = new byte[20];

    @Test
    void getFirstKLVTest() throws IOException {
        Random gene = new Random();
        gene.nextBytes(va);
        gene.nextBytes(vb);
        gene.nextBytes(vc);
        a = new KLV("aupr", va);
        b = new KLV("dtmd", vb);
        c = new KLV("errr", vc);
        byte[] res = a.toBytes();
        res = addArrays(res, b.toBytes());
        res = addArrays(res, c.toBytes());
        Pair<KLV, byte[]> r = getFirstKLV(res);
        res = r.getValue();
        assertArrayEquals(r.getKey().getV(), a.getV());
        assertEquals(r.getKey().getL(), a.getL());
        assertEquals(r.getKey().getK(), a.getK());
        r = getFirstKLV(res);
        res = r.getValue();
        assertEquals(r.getKey().getK(), b.getK());
        assertEquals(r.getKey().getL(), b.getL());
        assertArrayEquals(r.getKey().getV(), b.getV());
        r = getFirstKLV(res);
        res = r.getValue();
        assertEquals(r.getKey().getL(), c.getL());
        assertEquals(r.getKey().getK(), c.getK());
        assertArrayEquals(r.getKey().getV(), c.getV());
        assertNull(r.getValue());
    }

    @Test
    void addArraysTest() {
        byte[] aa = "aaaa".getBytes();
        byte[] bb = "bbbb".getBytes();
        byte[] cc = addArrays(aa, bb);
        assertArrayEquals(cc, "aaaabbbb".getBytes());
        cc = addArrays(bb, aa);
        assertArrayEquals(cc, "bbbbaaaa".getBytes());
    }

    @Test
    void searchKeyValueTest() throws IOException {
        Random gene = new Random();
        gene.nextBytes(va);
        gene.nextBytes(vb);
        gene.nextBytes(vc);
        a = new KLV("aupr", va);
        b = new KLV("dtmd", vb);
        c = new KLV("errr", vc);
        byte[] res = a.toBytes();
        res = addArrays(res, b.toBytes());
        res = addArrays(res, c.toBytes());
        assertEquals(searchKeyValue(res, "dtmd").getK(), b.getK());
        assertEquals(searchKeyValue(res, "aupr").getK(), a.getK());
        assertEquals(searchKeyValue(res, "errr").getK(), c.getK());
    }
}