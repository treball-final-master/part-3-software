package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertEquals;

class keysListTest {
    KeyTransportType a;
    KeyTransportType b;
    KeyTransportType c;
    keysList llista;

    @BeforeEach
    void setUp() {
        a = new SimpleKeyType("A", nouAES(128));
        b = new SimpleKeyType("B", nouAES(128));
        c = new SimpleKeyType("C", nouAES(128));
        llista = new keysList();
    }

    @Test
    void addigetKey() {
        llista.addKey(a);
        llista.addKey(b);
        llista.addKey(c);
        assertEquals(llista.getKey(a.keyName), a);
        assertEquals(llista.getKey(b.keyName), b);
        assertEquals(llista.getKey(c.keyName), c);
    }
}