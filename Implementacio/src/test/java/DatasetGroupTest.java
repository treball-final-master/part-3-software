package main.java.MPEGG.Data;

import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class DatasetGroupTest {
    DatasetGroup dg;
    URI uri1;
    URI uri2;

    @BeforeEach
    void setUp() throws Exception {
        dg = new DatasetGroup();
        uri1 = new URI();
        ReferenceType r = new ReferenceType();
        uri1.apuntarBlock(-1, -1, r);
        Random rand = new Random();
        byte[] dades = new byte[10];
        rand.nextBytes(dades);
        uri1 = dg.newBlock(uri1, dades);
        KeyTransportType wrapper = new SimpleKeyType("hola", nouAES(128));
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
        uri2 = new URI();
        uri2.apuntarDatasetPr(-1, uri1.getDt_id());
        dg.DGProtection.setNewEP(CipherURIType.AES_128_CTR, wrapper, uri2, WrapType.Simetric, null);
    }

    /**
     * Parse i actualitzarBytes
     */
    @Test
    void gestioBytes() throws Exception {
        byte[] aux = dg.getBytes();
        assertNotNull(aux);
        DatasetGroup dg2 = new DatasetGroup(aux);
        assertArrayEquals(dg.DGProtection.getKLV().getV(), dg2.DGProtection.getKLV().getV());
        assertArrayEquals(dg.DGMetadata.getKLV().getV(), dg2.DGMetadata.getKLV().getV());
        assertEquals(dg.datasets.size(), dg2.datasets.size());
    }


    @Test
    void cryptDT() throws Exception {
        Dataset dt = dg.getDataset(uri1.getDt_id());
        dg.EncryptDT(uri2);
        assertNull(dg.getDataset(uri1.getDt_id()).DTProtection);
        assertNotNull(dg.getDataset(uri1.getDt_id()).encryptedBoxP);
        dg.DecryptDT(uri2);
        assertArrayEquals(dg.getDataset(uri1.getDt_id()).getAUBytes(), dt.getAUBytes());
        assertEquals(dg.getDataset(uri1.getDt_id()).DTProtection.getEncryptionParameters(), dt.DTProtection.getEncryptionParameters());
    }
}