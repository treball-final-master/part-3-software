package main.java.MPEGG.Keys;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.crypto.Cipher;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.getKeyAgreeSend;
import static main.java.MPEGG.Utilitats.KeyGens.nouParellEC;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KeyEllipticCurveTypeTest {
    KeyTransportType wrapper1;
    KeyTransportType wrapper2;
    KeyPair aWrap1;
    KeyPair aWrap2;
    String nomClau;
    KeyEllipticCurveType wraped;

    @BeforeEach
    void setUp() throws NoSuchAlgorithmException {
        nomClau = "test";
        aWrap1 = nouParellEC();
        aWrap2 = nouParellEC();
        wrapper1 = new SimplePubKeyType("Hola1", aWrap1);
        wrapper2 = new SimplePubKeyType("Hola2", aWrap2);
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper1);
        llistaClaus.addKey(wrapper2);
    }

    @Test
    void getKey() throws Exception {
        wraped = new KeyEllipticCurveType(nomClau, wrapper1.getKeyName(), wrapper2.getKeyName());
        assertEquals(wraped.getKey("AES", Cipher.SECRET_KEY), getKeyAgreeSend(aWrap1, aWrap2));
    }

}