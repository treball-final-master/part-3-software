package main.java.MPEGG.Data;

import main.java.MPEGG.Boxes.DatasetMetadataType;
import main.java.MPEGG.Boxes.DatasetProtectionType;
import main.java.MPEGG.Boxes.ReferenceType;
import main.java.MPEGG.Keys.KeyTransportType;
import main.java.MPEGG.Keys.SimpleKeyType;
import main.java.MPEGG.Keys.keysList;
import main.java.MPEGG.Utilitats.URI;
import main.java.MPEGG.enume.CipherURIType;
import main.java.MPEGG.enume.WrapType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static main.java.MPEGG.Data.MPEG_G.llistaClaus;
import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.*;

class DatasetTest {
    Dataset dt;
    URI uri1;
    URI uri2;

    @BeforeEach
    void setUp() throws Exception {
        dt = new Dataset();
        uri1 = new URI();
        ReferenceType ref = new ReferenceType();
        ref.setChromosomeName("chr1");
        ReferenceType.Position p = new ReferenceType.Position();
        p.setEnd(10);
        p.setStart(0);
        ref.setPosition(p);
        uri1.apuntarBlock(0, 0, ref);
        Random rand = new Random();
        byte[] dades = new byte[10];
        rand.nextBytes(dades);
        uri1 = dt.newBlock(uri1, dades);
        KeyTransportType wrapper = new SimpleKeyType("hola", nouAES(128));
        llistaClaus = new keysList();
        llistaClaus.addKey(wrapper);
        uri2 = new URI();
        uri2.apuntarAccesUnitPr(-1, -1, uri1.getRef());
        dt.DTProtection.setNewEP(CipherURIType.AES_128_CTR, wrapper, uri2, WrapType.Simetric, null);
    }

    /**
     * Parse i actualitzarBytes
     */
    @Test
    void gestioBytes() throws Exception {
        byte[] aux = dt.actualitzarBytes();
        assertNotNull(aux);
        Dataset dt2 = new Dataset(aux);
        assertArrayEquals(dt.encryptedBoxMd, dt2.encryptedBoxMd);
        assertArrayEquals(dt.encryptedBoxP, dt2.encryptedBoxP);
        assertArrayEquals(dt.DTProtection.getKLV().getV(), dt2.DTProtection.getKLV().getV());
        assertArrayEquals(dt.DTMetadata.getKLV().getV(), dt2.DTMetadata.getKLV().getV());
        assertEquals(dt.accesunits.size(), dt2.accesunits.size());
    }

    /**
     * setDTMetadataEncrypted i setDTMetadata i getDTMetadata i getDTMetEncry i isEncryptedBoxMd
     */
    @Test
    void gestioDTMetadata() throws Exception {
        assertNotNull(dt.DTMetadata);
        assertNull(dt.getDTMetEncry());
        DatasetMetadataType dtmd = dt.DTMetadata;
        assertEquals(dt.DTMetadata, dtmd);
        dt.DTMetadata = null;
        assertNull(dt.DTMetadata);
        dt.setDTMetadata(dtmd);
        assertEquals(dt.DTMetadata, dtmd);
        assertFalse(dt.isEncryptedBoxMd());
        Random rand = new Random();
        byte[] aleatori = new byte[10];
        rand.nextBytes(aleatori);
        dt.setDTMetadataEncrypted(aleatori);
        assertTrue(dt.isEncryptedBoxMd());
        assertNull(dt.DTMetadata);
        assertEquals(dt.getDTMetEncry(), aleatori);
        dt.encryptedBoxMd = null;
        boolean throwss = false;
        try {
            dt.isEncryptedBoxMd();
        } catch (Exception e) {
            throwss = true;
        }
        assertTrue(throwss);
    }

    /**
     * setDTProtectionEncrypted i setDTProtection i getDTProtection i getDTProtEncry i isEncryptedBoxP
     */
    @Test
    void gestioDTProtection() throws Exception {
        assertNotNull(dt.DTProtection);
        assertNull(dt.getDTProtEncry());
        DatasetProtectionType dtpr = dt.DTProtection;
        assertEquals(dt.DTProtection, dtpr);
        dt.DTProtection = null;
        assertNull(dt.DTProtection);
        dt.setDTProtection(dtpr);
        assertEquals(dt.DTProtection, dtpr);
        assertFalse(dt.isEncryptedBoxP());
        Random rand = new Random();
        byte[] aleatori = new byte[10];
        rand.nextBytes(aleatori);
        dt.setDTProtectionEncrypted(aleatori);
        assertTrue(dt.isEncryptedBoxP());
        assertNull(dt.DTProtection);
        assertEquals(dt.getDTProtEncry(), aleatori);
        dt.encryptedBoxP = null;
        boolean throwss = false;
        try {
            dt.isEncryptedBoxP();
        } catch (Exception e) {
            throwss = true;
        }
        assertTrue(throwss);
    }

    @Test
    void cryptAU() throws Exception {
        AccesUnit au = dt.getAU(uri1.getRef());
        dt.EncryptAU(uri2);
        assertNull(dt.getAU(uri1.getRef()).AUProtection);
        assertNotNull(dt.getAU(uri1.getRef()).encryptedBoxP);
        dt.DecryptAU(uri2);
        assertEquals(dt.getAU(uri1.getRef()).readBlock(), au.readBlock());
        assertEquals(dt.getAU(uri1.getRef()).AUProtection.getAccessUnitEncryptionParameters(), au.AUProtection.getAccessUnitEncryptionParameters());
    }
}