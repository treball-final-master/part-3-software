package main.java.MPEGG.Keys;

import org.junit.jupiter.api.Test;

import static main.java.MPEGG.Utilitats.KeyGens.nouAES;
import static org.junit.jupiter.api.Assertions.assertEquals;

class KeyTransportTypeTest {
    @Test
    void noms() {
        KeyTransportType a = new SimpleKeyType("A", nouAES(128));
        assertEquals(a.getKeyName(), "A");
    }
}