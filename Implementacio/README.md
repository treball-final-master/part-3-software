# MPEG-G
Implementació de l'encriptacio de MPEG-G en JAVA per TFG:
-----------

Al tractar-se d'una implementació pel TFG on la part important es la d'encriptació s'ha fet el següent diferent de l'especificació de MPEG-G part 3:
 *    <p> L'accesunit nomes t´`e dades no l'information block.
 *    <p> No s'utilitza Policy.
 *    <p> Nomes hi ha un block per cada AU. I per tant nomes un signature parametre a AU.
 *    <p> WrappedKey AccesUnit nomes un valor i sense estar wrapejat. S'ha canviat respecte l'XML i es format Key i nomes un valor enlloc de List.
 *    <p> S'envien claus privades tot i que no s'hauria de fer!.


Altres comentaris:
------
 * Finalment Blake2b no s'ha implementat com a hash i hmac.
 * Utilitza versio de Java 11 però amb dependencies de java 6/8 per JAXB.
 * Javadoc està a mitges i algunes coses no correctes.

Pel que fa a ls parametres per encriptar s'ha de tenir en compte que:
-------
  *  Note that GCM mode has a uniqueness requirement on IVs used in encryption with a given key. When IVs are repeated for GCM encryption, such usages are subject to forgery attacks. Thus, after each encryption operation using GCM mode, callers should re-initialize the cipher objects with GCM parameters which have a different IV value. In case of ChaCha20 is the same but with Nounce.
-------
-------
Falta:
- Passar parts del main a test.