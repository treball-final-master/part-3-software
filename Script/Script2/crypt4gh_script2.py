import nacl.utils
import math
import numpy as np
import time
import signal
import sys
import matplotlib.pyplot as plt
from bitarray import bitarray
from nacl.public import PrivateKey, Box
# https://pynacl.readthedocs.io/en/stable/public/

# Afegir
## modificar un bit de la clau i fer histograma de quans bits canvien i posicions que canvien

bucles = 1000000 # 1.000.000
byte_size = 8

totals = [bucles] * 256
zeros = [0] * 256

start_time = time.time()

for x in range(bucles):
    # Generate Bob's private key
    skbob = PrivateKey.generate()
    pkbob = skbob.public_key

    # Generate Alice's private key
    skalice = PrivateKey.generate()
    pkalice = skalice.public_key

    bob_box = Box(skbob, pkalice)

    shared = bob_box.shared_key() # s
    a = list(shared)

    for i in range(256):
        bits = list(format(a[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros[i] += 1

#print(totals)
#print(zeros)
v1 = np.array(totals)
v2 = np.array(zeros)

percent = (v2/v1)*100

print("Percetatge de que sigui un 0 a cada bit amb una clau d'en Bob i una de l'Alice")
print(percent)
#plt.hist(percent, bins=10)
#plt.show()
#plt.hist(percent2, bins=10)
#plt.show()
print("Nombre de zeros")
print(v2)
print("Totals")
print(v1)
print("Temps d'execució del programa")
t = time.time() - start_time
print("--- %s seconds ---" % t)
print("Temps mitja de cada iteració")
tit = t/bucles
print(tit)
