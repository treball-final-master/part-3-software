import nacl.utils
import math
import numpy as np
import time
import signal
import sys
import matplotlib.pyplot as plt
from bitarray import bitarray
from nacl.public import PrivateKey, Box
# https://pynacl.readthedocs.io/en/stable/public/

# Afegir
## modificar un bit de la clau i fer histograma de quans bits canvien i posicions que canvien

run = True

bucles = 0
byte_size = 8

zeros = [0] * 256
zeros2 = [0] * 256
diferents = [0] * 256

start_time = time.time()

def signal_handler(signal, frame):
    global run
    print("Final")
    run = False

signal.signal(signal.SIGUSR1, signal_handler)

while(run):
    bucles += 1
    # Generate Bob's private key
    skbob = PrivateKey.generate()
    pkbob = skbob.public_key

    # Generate Alice's private key
    skalice = PrivateKey.generate()
    pkalice = skalice.public_key

    # Generate Alice's second private key
    skalice2 = PrivateKey.generate()
    pkalice2 = skalice2.public_key

    bob_box = Box(skbob, pkalice)
    bob_box2 = Box(skbob, pkalice2)

    shared = bob_box.shared_key() # 32 bytes
    shared2 = bob_box2.shared_key() # 32 bytes
    a = list(shared)
    b = list(shared2)

    for i in range(256):
        bits = list(format(a[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros[i] += 1

    for i in range(256):
        bits = list(format(b[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros2[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros2[i] += 1


print("Numero d'iteracions")
print(bucles)
totals = [bucles] * 256

#print(totals)
#print(zeros)
v1 = np.array(totals)
v2 = np.array(zeros)
v3 = np.array(zeros2)

percent = (v2/v1)*100
percent2 = (v3/v1)*100

print("Percetatge de que sigui un 0 a cada bit amb una clau d'en Bob i una de l'Alice")
print(percent)
print("Percetatge de que sigui un 0 a cada bit amb la clau de'n Bob anterior Bob i una altra de l'Alice")
print(percent2)
#plt.hist(percent, bins=10)
#plt.show()
#plt.hist(percent2, bins=10)
#plt.show()
print("Temps d'execució del programa")
t = time.time() - start_time
print("--- %s seconds ---" % t)
print("Temps mitja de cada iteració")
tit = t/bucles
print(tit)
