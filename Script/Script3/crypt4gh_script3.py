import nacl.utils
import math
import numpy as np
import time
import signal
import sys
import nacl.encoding
import nacl.hash
import base64
from bitarray import bitarray
from nacl.public import PrivateKey, Box

BLAKE2B_BYTES = nacl.bindings.crypto_generichash_BYTES

# https://pynacl.readthedocs.io/en/stable/public/

# Afegir
## modificar un bit de la clau i fer histograma de quans bits canvien i posicions que canvien

bucles = 1000000 # 1.000.000
byte_size = 8

totals = [bucles] * 256
zeros = [0] * 256
zeros2 = [0] * 256
zeros3 = [0] * 256

start_time = time.time()

for x in range(bucles):
    # Generate Bob's private key
    skbob = PrivateKey.generate()
    pkbob = skbob.public_key

    # Generate Alice's private key
    skalice = PrivateKey.generate()
    pkalice = skalice.public_key

    bob_box = Box(skbob, pkalice)

    shared = bob_box.shared_key() # s
    a = list(shared)

    aux1 = shared
    aux2 = pkalice
    aux3 = pkbob

    HASHER = nacl.hash.blake2b
    data = aux1 or aux2 or aux3
    data2 = aux1 + aux2._public_key + aux3._public_key
    h = HASHER(data, digest_size=BLAKE2B_BYTES, key=b'', salt=b'', person=b'', encoder=nacl.encoding.HexEncoder)
    h2 = HASHER(data2, digest_size=BLAKE2B_BYTES, key=b'', salt=b'', person=b'', encoder=nacl.encoding.HexEncoder)
    b = list(h)
    c = list(h2)


    for i in range(256):
        bits = list(format(a[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros[i] += 1

    for i in range(256):
        bits = list(format(b[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros2[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros2[i] += 1

    for i in range(256):
        bits = list(format(c[i//8], "b"))
        pad = 8 - len(bits)
        if i%8 < pad:
            zeros3[i] += 1
        elif bits[i%8 - pad] == '0':
            zeros3[i] += 1

#print(totals)
#print(zeros)
v1 = np.array(totals)
v2 = np.array(zeros)
v3 = np.array(zeros2)
v4 = np.array(zeros3)

percent = (v2/v1)*100
percent2 = (v3/v1)*100
percent3 = (v4/v1)*100

print("Percetatge de que sigui un 0 a cada bit amb una clau d'en Bob i una de l'Alice")
print(percent)
print("Nombre de zeros")
print(zeros)
print("Percetatge2 de que sigui un 0 a cada bit amb una clau d'en Bob i una de l'Alice")
print(percent2)
print("Nombre de zeros2")
print(zeros2)
print("Percetatge3 de que sigui un 0 a cada bit amb una clau d'en Bob i una de l'Alice")
print(percent3)
print("Nombre de zeros3")
print(zeros3)
print("Temps d'execució del programa")
t = time.time() - start_time
print("--- %s seconds ---" % t)
print("Temps mitja de cada iteració")
tit = t/bucles
print(tit)
