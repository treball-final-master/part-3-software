import nacl.utils
import math
import numpy as np
import time
import signal
import sys
import nacl.encoding
import nacl.hash
import base64
import bitarray
from nacl.public import PrivateKey, Box

BLAKE2B_BYTES = 32

bucles = 1000000 # 1.000.000
byte_size = 8

totals = [bucles] * 256
zeros = [0] * 256
zeroshash = [0] * 256
matriuIguals = np.zeros((256, 256))
matriuIgualshash = np.zeros((256, 256))

start_time = time.time()

for x in range(bucles):
    zeroit = [0] * 256
    zeroithash = [0] * 256

    # Generate Bob's private key
    skbob = PrivateKey.generate()
    pkbob = skbob.public_key

    # Generate Alice's private key
    skalice = PrivateKey.generate()
    pkalice = skalice.public_key

    bob_box = Box(skbob, pkalice)

    shared = bob_box.shared_key() # s
    a = list(shared)

    for i in range(256):
        bits = list(format(a[i//8], "b")) # Al fer i//8 accedeix als numero de byte i al fer format passa de decimal a binari i list per passar de binary a vector.
        pad = 8 - len(bits) # Obte el nombre de bits de l'inici que son 0 i per tant no queden a bits.
        if i%8 < pad or bits[i%8 - pad] == '0': # Mira el la posicio dins el byte fent i%8 i comprova si es una que estaria a la posicio de 0 perque despres es 0. O si es 0.
            zeros[i] += 1
            zeroit[i] += 1

    for i in range(256):
        for j in range(256):
            if zeroit[i] == zeroit[j]:
                matriuIguals[i][j] += 1

#print(totals)
#print(zeros)
v1 = np.array(totals)
v2 = np.array(zeros)

np.savetxt("./script4-zeros1.csv", (v2,v1),fmt='%i', delimiter=",", header="zeros,totals")
np.savetxt("./script4-iguals1.csv", matriuIguals,fmt='%i', delimiter=",", header="valors iguals")

print("Temps d'execució del programa")
t = time.time() - start_time
print("--- %s seconds ---" % t)
print("Temps mitja de cada iteració")
tit = t/bucles
print(tit)
